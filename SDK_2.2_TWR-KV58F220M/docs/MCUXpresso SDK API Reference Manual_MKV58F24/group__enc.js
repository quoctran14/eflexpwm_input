var group__enc =
[
    [ "enc_config_t", "group__enc.html#structenc__config__t", [
      [ "enableReverseDirection", "group__enc.html#ga522c7ab99e1a8b897de8dd34868a0c75", null ],
      [ "decoderWorkMode", "group__enc.html#ga5e87b6b545b87079f90cbecd93e7f809", null ],
      [ "HOMETriggerMode", "group__enc.html#ga6ae2c5f9ba4a1dafbaac4b5b5eadd04d", null ],
      [ "INDEXTriggerMode", "group__enc.html#ga5e53616ea784ca3a6cd05083cfab2caa", null ],
      [ "enableTRIGGERClearPositionCounter", "group__enc.html#gaf0b92cd8d3b24f2aaf5d564a2fca4fe6", null ],
      [ "enableTRIGGERClearHoldPositionCounter", "group__enc.html#ga347cca3f04709a2372dcb0d1ff706420", null ],
      [ "enableWatchdog", "group__enc.html#ga818e09227f3a5892bda5e1128f2a03b4", null ],
      [ "watchdogTimeoutValue", "group__enc.html#ga4061699ed317acca6ad5a796cd8ff634", null ],
      [ "filterCount", "group__enc.html#ga28e5850a83a55194a165d0cac8f4add1", null ],
      [ "filterSamplePeriod", "group__enc.html#ga76b510d862901687d3afe0f9693232b7", null ],
      [ "positionMatchMode", "group__enc.html#gaaf23078fa2f9d81476596f10fa6f34c2", null ],
      [ "positionCompareValue", "group__enc.html#ga706b9258832c5da9279971b158cefe3b", null ],
      [ "revolutionCountCondition", "group__enc.html#ga5c6214d0358540fb9ca7bd31d3961fc4", null ],
      [ "enableModuloCountMode", "group__enc.html#gab41faf266e79e9fbdfbca2c4a568f286", null ],
      [ "positionModulusValue", "group__enc.html#ga9c4ca08aaa0a4680a62fae18c1f907a7", null ],
      [ "positionInitialValue", "group__enc.html#ga42ff0a11bf950e3383e2c86df34c1380", null ]
    ] ],
    [ "enc_self_test_config_t", "group__enc.html#structenc__self__test__config__t", [
      [ "signalDirection", "group__enc.html#ga43c1f46f38185c53d70945706d0f7379", null ],
      [ "signalCount", "group__enc.html#ga8aff93755f8b555b1665c5ecff59c4e2", null ],
      [ "signalPeriod", "group__enc.html#gaa7425889c344d1b9804603666b7beedc", null ]
    ] ],
    [ "FSL_ENC_DRIVER_VERSION", "group__enc.html#ga87f32f686d6336a459bfec898e524308", null ],
    [ "_enc_interrupt_enable", "group__enc.html#ga055521dd59bdb3746db74e6ae43f8509", [
      [ "kENC_HOMETransitionInterruptEnable", "group__enc.html#gga055521dd59bdb3746db74e6ae43f8509a75ab3bda44fc6a0e49a958c49a8446f9", null ],
      [ "kENC_INDEXPulseInterruptEnable", "group__enc.html#gga055521dd59bdb3746db74e6ae43f8509a2f4a6c1f0192740025e83543ff7bb4ff", null ],
      [ "kENC_WatchdogTimeoutInterruptEnable", "group__enc.html#gga055521dd59bdb3746db74e6ae43f8509ac9f8e6941282b3208d6e9d800e38e4b4", null ],
      [ "kENC_PositionCompareInerruptEnable", "group__enc.html#gga055521dd59bdb3746db74e6ae43f8509a2c8392a5dbb845809d1057342f5c096d", null ],
      [ "kENC_SimultBothPhaseChangeInterruptEnable", "group__enc.html#gga055521dd59bdb3746db74e6ae43f8509a6a2a12f6ba4b1b2546454defbce98432", null ],
      [ "kENC_PositionRollOverInterruptEnable", "group__enc.html#gga055521dd59bdb3746db74e6ae43f8509a9db4d5821d6d96decf02a2cecd25bdf5", null ],
      [ "kENC_PositionRollUnderInterruptEnable", "group__enc.html#gga055521dd59bdb3746db74e6ae43f8509aa0efb17e632b690b4453599bef9803ae", null ]
    ] ],
    [ "_enc_status_flags", "group__enc.html#ga19fd9d919dd4a1847677d7199c378bf6", [
      [ "kENC_HOMETransitionFlag", "group__enc.html#gga19fd9d919dd4a1847677d7199c378bf6ae9b2bd10da6ba16f5c8fea53447f0812", null ],
      [ "kENC_INDEXPulseFlag", "group__enc.html#gga19fd9d919dd4a1847677d7199c378bf6a55ba421b0dfb18097ac76ec21c8d0630", null ],
      [ "kENC_WatchdogTimeoutFlag", "group__enc.html#gga19fd9d919dd4a1847677d7199c378bf6a8e7a89029ba7dde8e75ae843440f6585", null ],
      [ "kENC_PositionCompareFlag", "group__enc.html#gga19fd9d919dd4a1847677d7199c378bf6ac9448fc907e5a1122271afd48e3eff45", null ],
      [ "kENC_SimultBothPhaseChangeFlag", "group__enc.html#gga19fd9d919dd4a1847677d7199c378bf6a71e6dc48bcd9c1e5b0e62ec1f356c315", null ],
      [ "kENC_PositionRollOverFlag", "group__enc.html#gga19fd9d919dd4a1847677d7199c378bf6a73afb47bdd635d7a086167e9cbfa4604", null ],
      [ "kENC_PositionRollUnderFlag", "group__enc.html#gga19fd9d919dd4a1847677d7199c378bf6a8c44bd8327bdf3ef396d32abdf2c7826", null ],
      [ "kENC_LastCountDirectionFlag", "group__enc.html#gga19fd9d919dd4a1847677d7199c378bf6aeac7a2ef9ce248e4ef4d1a824d10edf8", null ]
    ] ],
    [ "_enc_signal_status_flags", "group__enc.html#gabcad4dcf0536b020cfbea8cb4374626a", [
      [ "kENC_RawHOMEStatusFlag", "group__enc.html#ggabcad4dcf0536b020cfbea8cb4374626aa7c2d8b020d3fd84c4726e12e4eabf656", null ],
      [ "kENC_RawINDEXStatusFlag", "group__enc.html#ggabcad4dcf0536b020cfbea8cb4374626aa65070778b3f99e299d1c860705849773", null ],
      [ "kENC_RawPHBStatusFlag", "group__enc.html#ggabcad4dcf0536b020cfbea8cb4374626aa1e1e36e91eb52e966cc9701e9c8cfd77", null ],
      [ "kENC_RawPHAEXStatusFlag", "group__enc.html#ggabcad4dcf0536b020cfbea8cb4374626aaddf548234f6b197e04edef4936c5f3cf", null ],
      [ "kENC_FilteredHOMEStatusFlag", "group__enc.html#ggabcad4dcf0536b020cfbea8cb4374626aafbd74b303a2b2a1c1dc97b0d0b696552", null ],
      [ "kENC_FilteredINDEXStatusFlag", "group__enc.html#ggabcad4dcf0536b020cfbea8cb4374626aaf9b35682459d731cfbddbe4588425da7", null ],
      [ "kENC_FilteredPHBStatusFlag", "group__enc.html#ggabcad4dcf0536b020cfbea8cb4374626aa5406ade4d6ac443d8a0430dd4e2cb3b1", null ],
      [ "kENC_FilteredPHAStatusFlag", "group__enc.html#ggabcad4dcf0536b020cfbea8cb4374626aab54a4269314ff279f7bdfc84c58af5ff", null ]
    ] ],
    [ "enc_home_trigger_mode_t", "group__enc.html#ga9b19f10c2303de256aab7b1d5c0057c3", [
      [ "kENC_HOMETriggerDisabled", "group__enc.html#gga9b19f10c2303de256aab7b1d5c0057c3a14f8fc3da25b327522b1bf6b9341a10c", null ],
      [ "kENC_HOMETriggerOnRisingEdge", "group__enc.html#gga9b19f10c2303de256aab7b1d5c0057c3a2640b2c847b9d69a9b2382cb7f7a1481", null ],
      [ "kENC_HOMETriggerOnFallingEdge", "group__enc.html#gga9b19f10c2303de256aab7b1d5c0057c3a79e8a6e80fc98d6d88a84c937ef7e378", null ]
    ] ],
    [ "enc_index_trigger_mode_t", "group__enc.html#ga986882142c037e6dd8c7845410f1e287", [
      [ "kENC_INDEXTriggerDisabled", "group__enc.html#gga986882142c037e6dd8c7845410f1e287ae8376e76cb334d80c7cfc1c7f77e13ea", null ],
      [ "kENC_INDEXTriggerOnRisingEdge", "group__enc.html#gga986882142c037e6dd8c7845410f1e287ab5095f768a55185cc1bfbc089903d7b3", null ],
      [ "kENC_INDEXTriggerOnFallingEdge", "group__enc.html#gga986882142c037e6dd8c7845410f1e287a1f0da068b8eb44cfbeafb6f88426163b", null ]
    ] ],
    [ "enc_decoder_work_mode_t", "group__enc.html#ga397e650f9e1a2fef6109896f18378b4c", [
      [ "kENC_DecoderWorkAsNormalMode", "group__enc.html#gga397e650f9e1a2fef6109896f18378b4caf661773e581845df10715016acc53092", null ],
      [ "kENC_DecoderWorkAsSignalPhaseCountMode", "group__enc.html#gga397e650f9e1a2fef6109896f18378b4ca052a9bce9a3071c02b28238615b607a2", null ]
    ] ],
    [ "enc_position_match_mode_t", "group__enc.html#gaec8ec35691febedfb85e38fc4c311838", [
      [ "kENC_POSMATCHOnPositionCounterEqualToComapreValue", "group__enc.html#ggaec8ec35691febedfb85e38fc4c311838ac7207e66be9333b516ef1b5d4079a6b4", null ],
      [ "kENC_POSMATCHOnReadingAnyPositionCounter", "group__enc.html#ggaec8ec35691febedfb85e38fc4c311838ad546a2325a4a1b0d94ea19024cee663a", null ]
    ] ],
    [ "enc_revolution_count_condition_t", "group__enc.html#ga645857d43e30752a5bee6d970a1201be", [
      [ "kENC_RevolutionCountOnINDEXPulse", "group__enc.html#gga645857d43e30752a5bee6d970a1201bea6704d1629d730f19652da91096eb5a27", null ],
      [ "kENC_RevolutionCountOnRollOverModulus", "group__enc.html#gga645857d43e30752a5bee6d970a1201bea2ff1ec8052b541a253c50c2a6069dea8", null ]
    ] ],
    [ "enc_self_test_direction_t", "group__enc.html#ga0771658dcc0d7fb694a3f6c3df1f7815", [
      [ "kENC_SelfTestDirectionPositive", "group__enc.html#gga0771658dcc0d7fb694a3f6c3df1f7815a15fa41a761cf10c06dc032ad51b9d72c", null ],
      [ "kENC_SelfTestDirectionNegative", "group__enc.html#gga0771658dcc0d7fb694a3f6c3df1f7815a1536128883baf0c538ecf287c5b65a4a", null ]
    ] ],
    [ "kENC_HOMETransitionInterruptEnable", "group__enc.html#gga055521dd59bdb3746db74e6ae43f8509a75ab3bda44fc6a0e49a958c49a8446f9", null ],
    [ "kENC_INDEXPulseInterruptEnable", "group__enc.html#gga055521dd59bdb3746db74e6ae43f8509a2f4a6c1f0192740025e83543ff7bb4ff", null ],
    [ "kENC_WatchdogTimeoutInterruptEnable", "group__enc.html#gga055521dd59bdb3746db74e6ae43f8509ac9f8e6941282b3208d6e9d800e38e4b4", null ],
    [ "kENC_PositionCompareInerruptEnable", "group__enc.html#gga055521dd59bdb3746db74e6ae43f8509a2c8392a5dbb845809d1057342f5c096d", null ],
    [ "kENC_SimultBothPhaseChangeInterruptEnable", "group__enc.html#gga055521dd59bdb3746db74e6ae43f8509a6a2a12f6ba4b1b2546454defbce98432", null ],
    [ "kENC_PositionRollOverInterruptEnable", "group__enc.html#gga055521dd59bdb3746db74e6ae43f8509a9db4d5821d6d96decf02a2cecd25bdf5", null ],
    [ "kENC_PositionRollUnderInterruptEnable", "group__enc.html#gga055521dd59bdb3746db74e6ae43f8509aa0efb17e632b690b4453599bef9803ae", null ],
    [ "kENC_HOMETransitionFlag", "group__enc.html#gga19fd9d919dd4a1847677d7199c378bf6ae9b2bd10da6ba16f5c8fea53447f0812", null ],
    [ "kENC_INDEXPulseFlag", "group__enc.html#gga19fd9d919dd4a1847677d7199c378bf6a55ba421b0dfb18097ac76ec21c8d0630", null ],
    [ "kENC_WatchdogTimeoutFlag", "group__enc.html#gga19fd9d919dd4a1847677d7199c378bf6a8e7a89029ba7dde8e75ae843440f6585", null ],
    [ "kENC_PositionCompareFlag", "group__enc.html#gga19fd9d919dd4a1847677d7199c378bf6ac9448fc907e5a1122271afd48e3eff45", null ],
    [ "kENC_SimultBothPhaseChangeFlag", "group__enc.html#gga19fd9d919dd4a1847677d7199c378bf6a71e6dc48bcd9c1e5b0e62ec1f356c315", null ],
    [ "kENC_PositionRollOverFlag", "group__enc.html#gga19fd9d919dd4a1847677d7199c378bf6a73afb47bdd635d7a086167e9cbfa4604", null ],
    [ "kENC_PositionRollUnderFlag", "group__enc.html#gga19fd9d919dd4a1847677d7199c378bf6a8c44bd8327bdf3ef396d32abdf2c7826", null ],
    [ "kENC_LastCountDirectionFlag", "group__enc.html#gga19fd9d919dd4a1847677d7199c378bf6aeac7a2ef9ce248e4ef4d1a824d10edf8", null ],
    [ "kENC_RawHOMEStatusFlag", "group__enc.html#ggabcad4dcf0536b020cfbea8cb4374626aa7c2d8b020d3fd84c4726e12e4eabf656", null ],
    [ "kENC_RawINDEXStatusFlag", "group__enc.html#ggabcad4dcf0536b020cfbea8cb4374626aa65070778b3f99e299d1c860705849773", null ],
    [ "kENC_RawPHBStatusFlag", "group__enc.html#ggabcad4dcf0536b020cfbea8cb4374626aa1e1e36e91eb52e966cc9701e9c8cfd77", null ],
    [ "kENC_RawPHAEXStatusFlag", "group__enc.html#ggabcad4dcf0536b020cfbea8cb4374626aaddf548234f6b197e04edef4936c5f3cf", null ],
    [ "kENC_FilteredHOMEStatusFlag", "group__enc.html#ggabcad4dcf0536b020cfbea8cb4374626aafbd74b303a2b2a1c1dc97b0d0b696552", null ],
    [ "kENC_FilteredINDEXStatusFlag", "group__enc.html#ggabcad4dcf0536b020cfbea8cb4374626aaf9b35682459d731cfbddbe4588425da7", null ],
    [ "kENC_FilteredPHBStatusFlag", "group__enc.html#ggabcad4dcf0536b020cfbea8cb4374626aa5406ade4d6ac443d8a0430dd4e2cb3b1", null ],
    [ "kENC_FilteredPHAStatusFlag", "group__enc.html#ggabcad4dcf0536b020cfbea8cb4374626aab54a4269314ff279f7bdfc84c58af5ff", null ],
    [ "kENC_HOMETriggerDisabled", "group__enc.html#gga9b19f10c2303de256aab7b1d5c0057c3a14f8fc3da25b327522b1bf6b9341a10c", null ],
    [ "kENC_HOMETriggerOnRisingEdge", "group__enc.html#gga9b19f10c2303de256aab7b1d5c0057c3a2640b2c847b9d69a9b2382cb7f7a1481", null ],
    [ "kENC_HOMETriggerOnFallingEdge", "group__enc.html#gga9b19f10c2303de256aab7b1d5c0057c3a79e8a6e80fc98d6d88a84c937ef7e378", null ],
    [ "kENC_INDEXTriggerDisabled", "group__enc.html#gga986882142c037e6dd8c7845410f1e287ae8376e76cb334d80c7cfc1c7f77e13ea", null ],
    [ "kENC_INDEXTriggerOnRisingEdge", "group__enc.html#gga986882142c037e6dd8c7845410f1e287ab5095f768a55185cc1bfbc089903d7b3", null ],
    [ "kENC_INDEXTriggerOnFallingEdge", "group__enc.html#gga986882142c037e6dd8c7845410f1e287a1f0da068b8eb44cfbeafb6f88426163b", null ],
    [ "kENC_DecoderWorkAsNormalMode", "group__enc.html#gga397e650f9e1a2fef6109896f18378b4caf661773e581845df10715016acc53092", null ],
    [ "kENC_DecoderWorkAsSignalPhaseCountMode", "group__enc.html#gga397e650f9e1a2fef6109896f18378b4ca052a9bce9a3071c02b28238615b607a2", null ],
    [ "kENC_POSMATCHOnPositionCounterEqualToComapreValue", "group__enc.html#ggaec8ec35691febedfb85e38fc4c311838ac7207e66be9333b516ef1b5d4079a6b4", null ],
    [ "kENC_POSMATCHOnReadingAnyPositionCounter", "group__enc.html#ggaec8ec35691febedfb85e38fc4c311838ad546a2325a4a1b0d94ea19024cee663a", null ],
    [ "kENC_RevolutionCountOnINDEXPulse", "group__enc.html#gga645857d43e30752a5bee6d970a1201bea6704d1629d730f19652da91096eb5a27", null ],
    [ "kENC_RevolutionCountOnRollOverModulus", "group__enc.html#gga645857d43e30752a5bee6d970a1201bea2ff1ec8052b541a253c50c2a6069dea8", null ],
    [ "kENC_SelfTestDirectionPositive", "group__enc.html#gga0771658dcc0d7fb694a3f6c3df1f7815a15fa41a761cf10c06dc032ad51b9d72c", null ],
    [ "kENC_SelfTestDirectionNegative", "group__enc.html#gga0771658dcc0d7fb694a3f6c3df1f7815a1536128883baf0c538ecf287c5b65a4a", null ],
    [ "ENC_Init", "group__enc.html#ga3137a28cb8af9082c19244923faa6c7a", null ],
    [ "ENC_Deinit", "group__enc.html#gae5541a81fcbffecd6184d3c88eb7ae4b", null ],
    [ "ENC_GetDefaultConfig", "group__enc.html#ga812eaa30d800dd8901fada028ef14562", null ],
    [ "ENC_DoSoftwareLoadInitialPositionValue", "group__enc.html#ga813809b8cc5b8e3ecde96fa048854a78", null ],
    [ "ENC_SetSelfTestConfig", "group__enc.html#gaba180b2066b162e2e89107622102583f", null ],
    [ "ENC_GetStatusFlags", "group__enc.html#gac6d4b61f9be15c7f02aea388b4a227b5", null ],
    [ "ENC_ClearStatusFlags", "group__enc.html#ga339ef1f5da9195843d761dc9cc2a4096", null ],
    [ "ENC_GetSignalStatusFlags", "group__enc.html#ga5680b84c11d6abd61e0a8bb1e855a0c0", null ],
    [ "ENC_EnableInterrupts", "group__enc.html#ga381d452d1280b3c96438d23daa53feb7", null ],
    [ "ENC_DisableInterrupts", "group__enc.html#gafc42fec40149f8dddcd32a5425e6cabd", null ],
    [ "ENC_GetEnabledInterrupts", "group__enc.html#ga30209697ff01356ddb1c2c481046114f", null ],
    [ "ENC_GetPositionValue", "group__enc.html#ga20710a64c9be86f39f1e3e07611418d0", null ],
    [ "ENC_GetHoldPositionValue", "group__enc.html#gabb415676c9ed37e70302447ba74c079a", null ],
    [ "ENC_GetPositionDifferenceValue", "group__enc.html#gac6174b059531f941c244f4ba4bb0334f", null ],
    [ "ENC_GetHoldPositionDifferenceValue", "group__enc.html#ga0c1d85fb1ae43b29b5956c94ed73364f", null ],
    [ "ENC_GetRevolutionValue", "group__enc.html#ga61378ace000c47c8020aa956fe153f16", null ],
    [ "ENC_GetHoldRevolutionValue", "group__enc.html#gad7c69f91e436ff10454c22fb7ae5b7e4", null ],
    [ "enableReverseDirection", "group__enc.html#ga522c7ab99e1a8b897de8dd34868a0c75", null ],
    [ "decoderWorkMode", "group__enc.html#ga5e87b6b545b87079f90cbecd93e7f809", null ],
    [ "HOMETriggerMode", "group__enc.html#ga6ae2c5f9ba4a1dafbaac4b5b5eadd04d", null ],
    [ "INDEXTriggerMode", "group__enc.html#ga5e53616ea784ca3a6cd05083cfab2caa", null ],
    [ "enableTRIGGERClearPositionCounter", "group__enc.html#gaf0b92cd8d3b24f2aaf5d564a2fca4fe6", null ],
    [ "enableTRIGGERClearHoldPositionCounter", "group__enc.html#ga347cca3f04709a2372dcb0d1ff706420", null ],
    [ "enableWatchdog", "group__enc.html#ga818e09227f3a5892bda5e1128f2a03b4", null ],
    [ "watchdogTimeoutValue", "group__enc.html#ga4061699ed317acca6ad5a796cd8ff634", null ],
    [ "filterCount", "group__enc.html#ga28e5850a83a55194a165d0cac8f4add1", null ],
    [ "filterSamplePeriod", "group__enc.html#ga76b510d862901687d3afe0f9693232b7", null ],
    [ "positionMatchMode", "group__enc.html#gaaf23078fa2f9d81476596f10fa6f34c2", null ],
    [ "positionCompareValue", "group__enc.html#ga706b9258832c5da9279971b158cefe3b", null ],
    [ "revolutionCountCondition", "group__enc.html#ga5c6214d0358540fb9ca7bd31d3961fc4", null ],
    [ "enableModuloCountMode", "group__enc.html#gab41faf266e79e9fbdfbca2c4a568f286", null ],
    [ "positionModulusValue", "group__enc.html#ga9c4ca08aaa0a4680a62fae18c1f907a7", null ],
    [ "positionInitialValue", "group__enc.html#ga42ff0a11bf950e3383e2c86df34c1380", null ],
    [ "signalDirection", "group__enc.html#ga43c1f46f38185c53d70945706d0f7379", null ],
    [ "signalCount", "group__enc.html#ga8aff93755f8b555b1665c5ecff59c4e2", null ],
    [ "signalPeriod", "group__enc.html#gaa7425889c344d1b9804603666b7beedc", null ]
];