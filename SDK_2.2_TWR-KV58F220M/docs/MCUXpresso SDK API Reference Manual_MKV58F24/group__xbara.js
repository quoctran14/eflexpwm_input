var group__xbara =
[
    [ "xbara_control_config_t", "group__xbara.html#structxbara__control__config__t", [
      [ "activeEdge", "group__xbara.html#a115969c58f3d6d8642a1095a7c685167", null ],
      [ "requestType", "group__xbara.html#ae5ec7c11cc4f3164a562917fbc8f9183", null ]
    ] ],
    [ "FSL_XBARA_DRIVER_VERSION", "group__xbara.html#ga653ee2175ec5d96ce3093446c75849a7", null ],
    [ "xbara_active_edge_t", "group__xbara.html#gacf40a09fdbb3dd3f141697fb08ce224c", [
      [ "kXBARA_EdgeNone", "group__xbara.html#ggacf40a09fdbb3dd3f141697fb08ce224ca0b1f05aab008cc333fc9d221dc471ad2", null ],
      [ "kXBARA_EdgeRising", "group__xbara.html#ggacf40a09fdbb3dd3f141697fb08ce224caa9f6a89ba6b33bb42492a01a32e125d2", null ],
      [ "kXBARA_EdgeFalling", "group__xbara.html#ggacf40a09fdbb3dd3f141697fb08ce224cadd352c2692d10c47e0565b5db9c0aaad", null ],
      [ "kXBARA_EdgeRisingAndFalling", "group__xbara.html#ggacf40a09fdbb3dd3f141697fb08ce224cab5a8f839c97e6895864cf9c9acdf3acc", null ]
    ] ],
    [ "xbara_request_t", "group__xbara.html#ga8e4281860299afa4b037f46a43d194bc", [
      [ "kXBARA_RequestDisable", "group__xbara.html#gga8e4281860299afa4b037f46a43d194bca67befcfe617c522f75f9c58aca037aef", null ],
      [ "kXBARA_RequestDMAEnable", "group__xbara.html#gga8e4281860299afa4b037f46a43d194bca074420520b90004cbbc9fe1cf6d95b18", null ],
      [ "kXBARA_RequestInterruptEnalbe", "group__xbara.html#gga8e4281860299afa4b037f46a43d194bca12e1a53356dbfc00a5df9310eca29f91", null ]
    ] ],
    [ "xbara_status_flag_t", "group__xbara.html#ga0c8c0b6bbacda08f8d646f2ba109158e", [
      [ "kXBARA_EdgeDetectionOut0", "group__xbara.html#gga0c8c0b6bbacda08f8d646f2ba109158ea7fd07691d8d5cd8b7cbfa21c080464af", null ],
      [ "kXBARA_EdgeDetectionOut1", "group__xbara.html#gga0c8c0b6bbacda08f8d646f2ba109158eaa9104d5388f116228e21a65116dabd6d", null ],
      [ "kXBARA_EdgeDetectionOut2", "group__xbara.html#gga0c8c0b6bbacda08f8d646f2ba109158ea53460b197642d46a4f04eadda3cb4bd2", null ],
      [ "kXBARA_EdgeDetectionOut3", "group__xbara.html#gga0c8c0b6bbacda08f8d646f2ba109158eaa26a7c1cbca51e3ffa9362c03ac5a0d1", null ]
    ] ],
    [ "XBARA_Init", "group__xbara.html#ga5c026c940af4df8e13962eb78d4e0f13", null ],
    [ "XBARA_Deinit", "group__xbara.html#ga951d45c9a0884c2ea35d98b71a7ea889", null ],
    [ "XBARA_SetSignalsConnection", "group__xbara.html#gac03e8f73ad95b6df165650308b809cad", null ],
    [ "XBARA_GetStatusFlags", "group__xbara.html#ga47a5efeb5b13f65c272c01daadbf739c", null ],
    [ "XBARA_ClearStatusFlags", "group__xbara.html#gaee2d4bb11ed4c302dc6d66904f4eb7a8", null ],
    [ "XBARA_SetOutputSignalConfig", "group__xbara.html#gaa010e6aecc627b857b04d9c2e8f5178b", null ]
];