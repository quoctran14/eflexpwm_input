var group__pwm__driver =
[
    [ "pwm_signal_param_t", "group__pwm__driver.html#structpwm__signal__param__t", [
      [ "pwmChannel", "group__pwm__driver.html#afe284fd225ea192d3456b41dd2703365", null ],
      [ "dutyCyclePercent", "group__pwm__driver.html#acf5a17cbd7dafb2cca8afee404f12b17", null ],
      [ "level", "group__pwm__driver.html#a1cc3927fcf1fd1adaeac49139919ed4f", null ],
      [ "deadtimeValue", "group__pwm__driver.html#aad95dc08519edfd81485bad0640512f2", null ]
    ] ],
    [ "pwm_config_t", "group__pwm__driver.html#structpwm__config__t", [
      [ "enableDebugMode", "group__pwm__driver.html#a6269c0b9d8089986d5ca6be2d59e0728", null ],
      [ "enableWait", "group__pwm__driver.html#a8606dc1ca1a8645f9f45b7ed55694ed6", null ],
      [ "faultFilterCount", "group__pwm__driver.html#a00f0dbe857fe1f05794138c0acec4559", null ],
      [ "faultFilterPeriod", "group__pwm__driver.html#a949a67b134705cc3eea4c6843b07540c", null ],
      [ "initializationControl", "group__pwm__driver.html#a063619b08565ed073b4aa22065b6514b", null ],
      [ "clockSource", "group__pwm__driver.html#ae1129784787c30f1eafb856ef742316f", null ],
      [ "prescale", "group__pwm__driver.html#ae3583449715cded4b5d8f751243d1849", null ],
      [ "pairOperation", "group__pwm__driver.html#a7b35c872bee7e85d8aeda609b681a41f", null ],
      [ "reloadLogic", "group__pwm__driver.html#aa6c467701070fd42c3081e516a1a4faf", null ],
      [ "reloadSelect", "group__pwm__driver.html#adb40a7b5723fc3d4a0185275e8fda62b", null ],
      [ "reloadFrequency", "group__pwm__driver.html#a576607f47ae97b0c3eb47f9cfbab66d7", null ],
      [ "forceTrigger", "group__pwm__driver.html#a878aeddf9a10f2cb7ae5917cfe2c9c84", null ]
    ] ],
    [ "pwm_fault_param_t", "group__pwm__driver.html#structpwm__fault__param__t", [
      [ "faultClearingMode", "group__pwm__driver.html#abd6959c9123d004bd98d1f000404eb7e", null ],
      [ "faultLevel", "group__pwm__driver.html#a623c03ffe07a6bb87f5f678cf965fc61", null ],
      [ "enableCombinationalPath", "group__pwm__driver.html#aadb707d1b75ac87f0cf1fab143a8b51d", null ],
      [ "recoverMode", "group__pwm__driver.html#ac771868777f59c5876e89d69c7a6552f", null ]
    ] ],
    [ "pwm_input_capture_param_t", "group__pwm__driver.html#structpwm__input__capture__param__t", [
      [ "captureInputSel", "group__pwm__driver.html#a010807beeb7b92f66bae9eb2c0107a70", null ],
      [ "edgeCompareValue", "group__pwm__driver.html#ab7f5713d68bcce0b9e12db75630dc73c", null ],
      [ "edge0", "group__pwm__driver.html#aa4255f37fdebd83a57003336d1118a2f", null ],
      [ "edge1", "group__pwm__driver.html#a2eb7622e237d0472a7cabc92a6327d90", null ],
      [ "enableOneShotCapture", "group__pwm__driver.html#a05d085bcc3aa391d8e2b2119d6f4f140", null ],
      [ "fifoWatermark", "group__pwm__driver.html#a3dd2b7c6d1b55c7d324d0e2a9f77b119", null ]
    ] ],
    [ "FSL_PWM_DRIVER_VERSION", "group__pwm__driver.html#ga8bda5d1da1f6b7150e002c109a62d083", null ],
    [ "PWM_SUBMODULE_SWCONTROL_WIDTH", "group__pwm__driver.html#ga6e56de2326b1f4bf708bd7991010e63f", null ],
    [ "pwm_submodule_t", "group__pwm__driver.html#ga467e18574027c8e3bde8f61a3fe61010", [
      [ "kPWM_Module_0", "group__pwm__driver.html#gga467e18574027c8e3bde8f61a3fe61010add38c1b5a7f784f71f3cebf9a1bf3ff6", null ],
      [ "kPWM_Module_1", "group__pwm__driver.html#gga467e18574027c8e3bde8f61a3fe61010a47a6ce50cc1613c9988bf21d9bb44c4c", null ],
      [ "kPWM_Module_2", "group__pwm__driver.html#gga467e18574027c8e3bde8f61a3fe61010a0340ee44d37ea0644debbc392abe0218", null ],
      [ "kPWM_Module_3", "group__pwm__driver.html#gga467e18574027c8e3bde8f61a3fe61010a262d2987720c085ebda6b24b4e646ab9", null ]
    ] ],
    [ "pwm_channels_t", "group__pwm__driver.html#ga595affbbaec64a811545de35ae1914ce", null ],
    [ "pwm_value_register_t", "group__pwm__driver.html#ga7772029819872f4f3ee61d3f715827d4", [
      [ "kPWM_ValueRegister_0", "group__pwm__driver.html#gga7772029819872f4f3ee61d3f715827d4a2629df804c88f83f3934ff2fbb68aacc", null ],
      [ "kPWM_ValueRegister_1", "group__pwm__driver.html#gga7772029819872f4f3ee61d3f715827d4a42790226368938e490216e5f2c63c2fd", null ],
      [ "kPWM_ValueRegister_2", "group__pwm__driver.html#gga7772029819872f4f3ee61d3f715827d4ae42d8eb24146ed351e0e42dc349885e2", null ],
      [ "kPWM_ValueRegister_3", "group__pwm__driver.html#gga7772029819872f4f3ee61d3f715827d4a7539de4e419733b706a122acfe8b1b8a", null ],
      [ "kPWM_ValueRegister_4", "group__pwm__driver.html#gga7772029819872f4f3ee61d3f715827d4a8bfe3c876423ba03198b0086bf95c9a3", null ],
      [ "kPWM_ValueRegister_5", "group__pwm__driver.html#gga7772029819872f4f3ee61d3f715827d4ae046f51b0c1edd5217aa3f4ed2cadfa0", null ]
    ] ],
    [ "pwm_clock_source_t", "group__pwm__driver.html#ga54dcbb5e3e5c8ceebef5f46a9ec73a2f", [
      [ "kPWM_BusClock", "group__pwm__driver.html#gga54dcbb5e3e5c8ceebef5f46a9ec73a2faadd6fce563ffd4fcfa1b8fd32a6c8836", null ],
      [ "kPWM_ExternalClock", "group__pwm__driver.html#gga54dcbb5e3e5c8ceebef5f46a9ec73a2fa5491a1f9512916febc4d1e5a241f4aa1", null ],
      [ "kPWM_Submodule0Clock", "group__pwm__driver.html#gga54dcbb5e3e5c8ceebef5f46a9ec73a2facb3515bd116d5f3603ddedcfe12f0ec8", null ]
    ] ],
    [ "pwm_clock_prescale_t", "group__pwm__driver.html#ga1072624f12c3af3bba1591c9985961d0", [
      [ "kPWM_Prescale_Divide_1", "group__pwm__driver.html#gga1072624f12c3af3bba1591c9985961d0af9a5a8f75935115c248270592ebc1958", null ],
      [ "kPWM_Prescale_Divide_2", "group__pwm__driver.html#gga1072624f12c3af3bba1591c9985961d0a23a1e736f7176595b5c8f6928aae92af", null ],
      [ "kPWM_Prescale_Divide_4", "group__pwm__driver.html#gga1072624f12c3af3bba1591c9985961d0a22a28a9f43a62602bd76c6891def7df3", null ],
      [ "kPWM_Prescale_Divide_8", "group__pwm__driver.html#gga1072624f12c3af3bba1591c9985961d0a815f3c6fe8ebbf7cbc8321370b748c01", null ],
      [ "kPWM_Prescale_Divide_16", "group__pwm__driver.html#gga1072624f12c3af3bba1591c9985961d0ab480291453a4ed9f12bb649c3999cd7a", null ],
      [ "kPWM_Prescale_Divide_32", "group__pwm__driver.html#gga1072624f12c3af3bba1591c9985961d0a3609a2960ea5f6af9395bd712c835cd7", null ],
      [ "kPWM_Prescale_Divide_64", "group__pwm__driver.html#gga1072624f12c3af3bba1591c9985961d0a9329060508da1a966a938982f0437092", null ],
      [ "kPWM_Prescale_Divide_128", "group__pwm__driver.html#gga1072624f12c3af3bba1591c9985961d0a19e584bea4f6afaaf968fd6452763154", null ]
    ] ],
    [ "pwm_force_output_trigger_t", "group__pwm__driver.html#ga199c9464fb869977d72535e508628966", [
      [ "kPWM_Force_Local", "group__pwm__driver.html#gga199c9464fb869977d72535e508628966a9e11b14bc1860c492200b1370c672c6a", null ],
      [ "kPWM_Force_Master", "group__pwm__driver.html#gga199c9464fb869977d72535e508628966ac4b4c049b4469672fa062ca8e4afe5b6", null ],
      [ "kPWM_Force_LocalReload", "group__pwm__driver.html#gga199c9464fb869977d72535e508628966a9bb2f0d12171255d729380b2aacb92bb", null ],
      [ "kPWM_Force_MasterReload", "group__pwm__driver.html#gga199c9464fb869977d72535e508628966accf9880be990d673848acdcb9c6d58db", null ],
      [ "kPWM_Force_LocalSync", "group__pwm__driver.html#gga199c9464fb869977d72535e508628966ab4286c65207f98e1c2fe3c018a4a183e", null ],
      [ "kPWM_Force_MasterSync", "group__pwm__driver.html#gga199c9464fb869977d72535e508628966afea13339a2eeb6a11e8700cfd90dc8c3", null ],
      [ "kPWM_Force_External", "group__pwm__driver.html#gga199c9464fb869977d72535e508628966a764ad229154a4eaeee4ae8c407d060f2", null ],
      [ "kPWM_Force_ExternalSync", "group__pwm__driver.html#gga199c9464fb869977d72535e508628966a941c421adc51446d05dd65a257e081b9", null ]
    ] ],
    [ "pwm_init_source_t", "group__pwm__driver.html#ga56a54ba9383680868db1e1292aceedd1", [
      [ "kPWM_Initialize_LocalSync", "group__pwm__driver.html#gga56a54ba9383680868db1e1292aceedd1abc2411b795be6f4f26efdc1d7abeadfa", null ],
      [ "kPWM_Initialize_MasterReload", "group__pwm__driver.html#gga56a54ba9383680868db1e1292aceedd1a46121e203b79d6f4bfa0e65f3151ea97", null ],
      [ "kPWM_Initialize_MasterSync", "group__pwm__driver.html#gga56a54ba9383680868db1e1292aceedd1a46d08f3cb3f213bfc543a5f31140dd89", null ],
      [ "kPWM_Initialize_ExtSync", "group__pwm__driver.html#gga56a54ba9383680868db1e1292aceedd1a81e313c61b9f2ca344156d26c46b88b1", null ]
    ] ],
    [ "pwm_load_frequency_t", "group__pwm__driver.html#ga636ca143685fed2cb715c8e74b8e7d91", [
      [ "kPWM_LoadEveryOportunity", "group__pwm__driver.html#gga636ca143685fed2cb715c8e74b8e7d91a502575e9bd4268226093b0a60cb496f7", null ],
      [ "kPWM_LoadEvery2Oportunity", "group__pwm__driver.html#gga636ca143685fed2cb715c8e74b8e7d91ae25e14c14030995b7bcafea1ce1cd0bb", null ],
      [ "kPWM_LoadEvery3Oportunity", "group__pwm__driver.html#gga636ca143685fed2cb715c8e74b8e7d91a8bef356364d1b93faecf843dd26071bb", null ],
      [ "kPWM_LoadEvery4Oportunity", "group__pwm__driver.html#gga636ca143685fed2cb715c8e74b8e7d91a994a426c30942ee491d83dcf14315f10", null ],
      [ "kPWM_LoadEvery5Oportunity", "group__pwm__driver.html#gga636ca143685fed2cb715c8e74b8e7d91a466a761a7e6855ac0f6ab43b4a5a4900", null ],
      [ "kPWM_LoadEvery6Oportunity", "group__pwm__driver.html#gga636ca143685fed2cb715c8e74b8e7d91aa4b38e484380977443e365543194a83d", null ],
      [ "kPWM_LoadEvery7Oportunity", "group__pwm__driver.html#gga636ca143685fed2cb715c8e74b8e7d91a1eac6ad423d02507928c955a154c46e7", null ],
      [ "kPWM_LoadEvery8Oportunity", "group__pwm__driver.html#gga636ca143685fed2cb715c8e74b8e7d91a1ece67ccad5cbfe798eb8b2884d3d058", null ],
      [ "kPWM_LoadEvery9Oportunity", "group__pwm__driver.html#gga636ca143685fed2cb715c8e74b8e7d91a2afb25bbf82ea891d626ac5e7d5672a6", null ],
      [ "kPWM_LoadEvery10Oportunity", "group__pwm__driver.html#gga636ca143685fed2cb715c8e74b8e7d91a7a4c48816896b8ff0b2b574911329354", null ],
      [ "kPWM_LoadEvery11Oportunity", "group__pwm__driver.html#gga636ca143685fed2cb715c8e74b8e7d91a11bca58390b8d76d09df74743d40f90d", null ],
      [ "kPWM_LoadEvery12Oportunity", "group__pwm__driver.html#gga636ca143685fed2cb715c8e74b8e7d91a0ad8ab598380fb326ecaced36caae3b3", null ],
      [ "kPWM_LoadEvery13Oportunity", "group__pwm__driver.html#gga636ca143685fed2cb715c8e74b8e7d91af01c1a29ee56d9971bf39066926c9b41", null ],
      [ "kPWM_LoadEvery14Oportunity", "group__pwm__driver.html#gga636ca143685fed2cb715c8e74b8e7d91a8d394dde4e8bee1e79602903744d1108", null ],
      [ "kPWM_LoadEvery15Oportunity", "group__pwm__driver.html#gga636ca143685fed2cb715c8e74b8e7d91a9978f86435c6102452f6253d9b8e9f16", null ],
      [ "kPWM_LoadEvery16Oportunity", "group__pwm__driver.html#gga636ca143685fed2cb715c8e74b8e7d91ae5e40f251ac27cb47862be061094a04c", null ]
    ] ],
    [ "pwm_fault_input_t", "group__pwm__driver.html#ga5b339bfb308c5c3b4a1f3170a8653926", [
      [ "kPWM_Fault_0", "group__pwm__driver.html#gga5b339bfb308c5c3b4a1f3170a8653926aa50cf34eeaa8076d5ea0df6a38e2aee3", null ],
      [ "kPWM_Fault_1", "group__pwm__driver.html#gga5b339bfb308c5c3b4a1f3170a8653926a3bd0bf391f22f4998a3086c6d711ec56", null ],
      [ "kPWM_Fault_2", "group__pwm__driver.html#gga5b339bfb308c5c3b4a1f3170a8653926a7a3b2460e58d5ba0256d80b3be07b5bb", null ],
      [ "kPWM_Fault_3", "group__pwm__driver.html#gga5b339bfb308c5c3b4a1f3170a8653926a4a75c3be4618667d27733b946d5a96c6", null ]
    ] ],
    [ "pwm_input_capture_edge_t", "group__pwm__driver.html#gad5bbf1775f0f1d22d37523c5062f044b", [
      [ "kPWM_Disable", "group__pwm__driver.html#ggad5bbf1775f0f1d22d37523c5062f044ba3a9b563f19903f48e0dc698b1bd9e7cb", null ],
      [ "kPWM_FallingEdge", "group__pwm__driver.html#ggad5bbf1775f0f1d22d37523c5062f044ba789adc57426b9cab5e701edcfef6dd98", null ],
      [ "kPWM_RisingEdge", "group__pwm__driver.html#ggad5bbf1775f0f1d22d37523c5062f044ba3b3dfb3faa75faef27dca1156af8f930", null ],
      [ "kPWM_RiseAndFallEdge", "group__pwm__driver.html#ggad5bbf1775f0f1d22d37523c5062f044ba2287e78066381060836aa41f70f26395", null ]
    ] ],
    [ "pwm_force_signal_t", "group__pwm__driver.html#ga6429e832c8b26915f2448463ab396cd1", [
      [ "kPWM_UsePwm", "group__pwm__driver.html#gga6429e832c8b26915f2448463ab396cd1a166c8c92dffb307770373f011f58d8b6", null ],
      [ "kPWM_InvertedPwm", "group__pwm__driver.html#gga6429e832c8b26915f2448463ab396cd1a1c5e51e8f0bf695ca6d554719042f886", null ],
      [ "kPWM_SoftwareControl", "group__pwm__driver.html#gga6429e832c8b26915f2448463ab396cd1acf2037b15d767df630652ecdcc0b6b5a", null ],
      [ "kPWM_UseExternal", "group__pwm__driver.html#gga6429e832c8b26915f2448463ab396cd1aeaf780fe39f7852159d59b95361f5799", null ]
    ] ],
    [ "pwm_chnl_pair_operation_t", "group__pwm__driver.html#ga74af21116f00e824d05e776ddccb0802", [
      [ "kPWM_Independent", "group__pwm__driver.html#gga74af21116f00e824d05e776ddccb0802a4e4e74c3efd797dcc5bd430df090c548", null ],
      [ "kPWM_ComplementaryPwmA", "group__pwm__driver.html#gga74af21116f00e824d05e776ddccb0802a001cd311a33d4f1bf3bcfb0882ed30c2", null ],
      [ "kPWM_ComplementaryPwmB", "group__pwm__driver.html#gga74af21116f00e824d05e776ddccb0802a1c1204d77dfefb55460b359c9911d3df", null ]
    ] ],
    [ "pwm_register_reload_t", "group__pwm__driver.html#ga1376825a790fbe133bab869191bf5764", [
      [ "kPWM_ReloadImmediate", "group__pwm__driver.html#gga1376825a790fbe133bab869191bf5764a0302e609960d4cdd2d9017d76dabb12f", null ],
      [ "kPWM_ReloadPwmHalfCycle", "group__pwm__driver.html#gga1376825a790fbe133bab869191bf5764a1f4ca50703a5f530ca5436647bee5d84", null ],
      [ "kPWM_ReloadPwmFullCycle", "group__pwm__driver.html#gga1376825a790fbe133bab869191bf5764a143f812e649672592ac6f2966239a80f", null ],
      [ "kPWM_ReloadPwmHalfAndFullCycle", "group__pwm__driver.html#gga1376825a790fbe133bab869191bf5764a87c32f53e2c480284ebd863c866d76c0", null ]
    ] ],
    [ "pwm_fault_recovery_mode_t", "group__pwm__driver.html#ga10bb013dcfa77ce708fe90ac290894e7", [
      [ "kPWM_NoRecovery", "group__pwm__driver.html#gga10bb013dcfa77ce708fe90ac290894e7ad683c9a0ca12b4932bb3e753a1b71c44", null ],
      [ "kPWM_RecoverHalfCycle", "group__pwm__driver.html#gga10bb013dcfa77ce708fe90ac290894e7a9438b2356162912bca02e9bc87a6218c", null ],
      [ "kPWM_RecoverFullCycle", "group__pwm__driver.html#gga10bb013dcfa77ce708fe90ac290894e7a79b1439966e60152ede2e96789640b8a", null ],
      [ "kPWM_RecoverHalfAndFullCycle", "group__pwm__driver.html#gga10bb013dcfa77ce708fe90ac290894e7a0759098353e5f56bb2aff98f46b57c7e", null ]
    ] ],
    [ "pwm_interrupt_enable_t", "group__pwm__driver.html#ga4f56d1f07b6947af55eccc5375ed520d", [
      [ "kPWM_CompareVal0InterruptEnable", "group__pwm__driver.html#gga4f56d1f07b6947af55eccc5375ed520daafc673bd39a17c660c2b7ab0f8e3dc82", null ],
      [ "kPWM_CompareVal1InterruptEnable", "group__pwm__driver.html#gga4f56d1f07b6947af55eccc5375ed520da2e38a714989c515bce7cf90f8526fe69", null ],
      [ "kPWM_CompareVal2InterruptEnable", "group__pwm__driver.html#gga4f56d1f07b6947af55eccc5375ed520daf595e6cd0654614be3b013a40071e43b", null ],
      [ "kPWM_CompareVal3InterruptEnable", "group__pwm__driver.html#gga4f56d1f07b6947af55eccc5375ed520da11b2ef7316b810c58d6da5374c15249c", null ],
      [ "kPWM_CompareVal4InterruptEnable", "group__pwm__driver.html#gga4f56d1f07b6947af55eccc5375ed520da69a6bfc5caa28f56280900793223843b", null ],
      [ "kPWM_CompareVal5InterruptEnable", "group__pwm__driver.html#gga4f56d1f07b6947af55eccc5375ed520daa162e16f672b526d73a20c7bd9d596b8", null ],
      [ "kPWM_CaptureX0InterruptEnable", "group__pwm__driver.html#gga4f56d1f07b6947af55eccc5375ed520dab7fd23d9cbe1485ddfea034329bf21a4", null ],
      [ "kPWM_CaptureX1InterruptEnable", "group__pwm__driver.html#gga4f56d1f07b6947af55eccc5375ed520da75e7a7c02cac25144674919fae5c988b", null ],
      [ "kPWM_CaptureB0InterruptEnable", "group__pwm__driver.html#gga4f56d1f07b6947af55eccc5375ed520da60950ae650bdac58cc9d7525ee0d26f0", null ],
      [ "kPWM_CaptureB1InterruptEnable", "group__pwm__driver.html#gga4f56d1f07b6947af55eccc5375ed520dabeb97760b062764028ce3548bf4bd4f0", null ],
      [ "kPWM_CaptureA0InterruptEnable", "group__pwm__driver.html#gga4f56d1f07b6947af55eccc5375ed520da265b164e3f9f77535218f57a4be1b53d", null ],
      [ "kPWM_CaptureA1InterruptEnable", "group__pwm__driver.html#gga4f56d1f07b6947af55eccc5375ed520daea67600460b65ab0fdcc702cfe8dce41", null ],
      [ "kPWM_ReloadInterruptEnable", "group__pwm__driver.html#gga4f56d1f07b6947af55eccc5375ed520dacea7ba0ccfc25c688ccfbae5e9082949", null ],
      [ "kPWM_ReloadErrorInterruptEnable", "group__pwm__driver.html#gga4f56d1f07b6947af55eccc5375ed520da5c181e7dfc61241177e0a2e761c12bc6", null ],
      [ "kPWM_Fault0InterruptEnable", "group__pwm__driver.html#gga4f56d1f07b6947af55eccc5375ed520dad02badace3737fa894aa30c3d4cb78a2", null ],
      [ "kPWM_Fault1InterruptEnable", "group__pwm__driver.html#gga4f56d1f07b6947af55eccc5375ed520dae7860301d0be780fd19d84009205ee6d", null ],
      [ "kPWM_Fault2InterruptEnable", "group__pwm__driver.html#gga4f56d1f07b6947af55eccc5375ed520daf84aa7e6052c1761a770927a22fa4061", null ],
      [ "kPWM_Fault3InterruptEnable", "group__pwm__driver.html#gga4f56d1f07b6947af55eccc5375ed520da5da7daaf3baaf533de940613f720ef45", null ]
    ] ],
    [ "pwm_status_flags_t", "group__pwm__driver.html#gabc3ee1e46578039b2dbe795d60a69b2f", [
      [ "kPWM_CompareVal0Flag", "group__pwm__driver.html#ggabc3ee1e46578039b2dbe795d60a69b2fa4085859503a306f4838b8708270aebd3", null ],
      [ "kPWM_CompareVal1Flag", "group__pwm__driver.html#ggabc3ee1e46578039b2dbe795d60a69b2fa76c8be56ca875e509cf7249e16051221", null ],
      [ "kPWM_CompareVal2Flag", "group__pwm__driver.html#ggabc3ee1e46578039b2dbe795d60a69b2fabda314afa7b77b8fb1280ab8ad8c92e4", null ],
      [ "kPWM_CompareVal3Flag", "group__pwm__driver.html#ggabc3ee1e46578039b2dbe795d60a69b2fa9eb2effb85360b09a5823aa8821a1d85", null ],
      [ "kPWM_CompareVal4Flag", "group__pwm__driver.html#ggabc3ee1e46578039b2dbe795d60a69b2fa5574e42fdfbb30c9a22e0da0c703a351", null ],
      [ "kPWM_CompareVal5Flag", "group__pwm__driver.html#ggabc3ee1e46578039b2dbe795d60a69b2fa6c732459b6d238d783c9049d11d7541c", null ],
      [ "kPWM_CaptureX0Flag", "group__pwm__driver.html#ggabc3ee1e46578039b2dbe795d60a69b2faf0334248cfeec0e102b747c6112ddc52", null ],
      [ "kPWM_CaptureX1Flag", "group__pwm__driver.html#ggabc3ee1e46578039b2dbe795d60a69b2fac40ed759adecc7104fe66d77e226f5bd", null ],
      [ "kPWM_CaptureB0Flag", "group__pwm__driver.html#ggabc3ee1e46578039b2dbe795d60a69b2fa01e003611b957a607bc6f00df965af92", null ],
      [ "kPWM_CaptureB1Flag", "group__pwm__driver.html#ggabc3ee1e46578039b2dbe795d60a69b2fa8883f6fb88a1eafe3696a4e5287155cd", null ],
      [ "kPWM_CaptureA0Flag", "group__pwm__driver.html#ggabc3ee1e46578039b2dbe795d60a69b2fa8777414835e99f9d0f9376dbfe981a16", null ],
      [ "kPWM_CaptureA1Flag", "group__pwm__driver.html#ggabc3ee1e46578039b2dbe795d60a69b2fab80c16b4b478e220ccda0b6427fbbb1a", null ],
      [ "kPWM_ReloadFlag", "group__pwm__driver.html#ggabc3ee1e46578039b2dbe795d60a69b2fa335926bdcc79dfa6e3df0127d2d426c2", null ],
      [ "kPWM_ReloadErrorFlag", "group__pwm__driver.html#ggabc3ee1e46578039b2dbe795d60a69b2fa675549b86323ccf753f9189942f1d208", null ],
      [ "kPWM_RegUpdatedFlag", "group__pwm__driver.html#ggabc3ee1e46578039b2dbe795d60a69b2fab03a24483985e2a865ae61402a80a08e", null ],
      [ "kPWM_Fault0Flag", "group__pwm__driver.html#ggabc3ee1e46578039b2dbe795d60a69b2faa475a054c52a6f6d405221835bcec032", null ],
      [ "kPWM_Fault1Flag", "group__pwm__driver.html#ggabc3ee1e46578039b2dbe795d60a69b2faf2ac36eb7c23cd116999efb473ba5120", null ],
      [ "kPWM_Fault2Flag", "group__pwm__driver.html#ggabc3ee1e46578039b2dbe795d60a69b2faa4040bb373c31ab8f79a2960bc0a04fc", null ],
      [ "kPWM_Fault3Flag", "group__pwm__driver.html#ggabc3ee1e46578039b2dbe795d60a69b2fadaca248c0c3948533401a8dad2c1f6d2", null ]
    ] ],
    [ "pwm_mode_t", "group__pwm__driver.html#ga562b5946a0edd6f5eebb63db7d154d56", [
      [ "kPWM_SignedCenterAligned", "group__pwm__driver.html#gga562b5946a0edd6f5eebb63db7d154d56a62a713094bb4c628d40e801e07acab0a", null ],
      [ "kPWM_CenterAligned", "group__pwm__driver.html#gga562b5946a0edd6f5eebb63db7d154d56a74be077739ace445b92fbfb515a228ab", null ],
      [ "kPWM_SignedEdgeAligned", "group__pwm__driver.html#gga562b5946a0edd6f5eebb63db7d154d56a1ce6a8e531f0d012f2904435772adb59", null ],
      [ "kPWM_EdgeAligned", "group__pwm__driver.html#gga562b5946a0edd6f5eebb63db7d154d56a9e529b50dfb0c394a287c6f9e0ccf03a", null ]
    ] ],
    [ "pwm_level_select_t", "group__pwm__driver.html#gaad9ca9d9d013e066ba14b2f93d512a4f", [
      [ "kPWM_HighTrue", "group__pwm__driver.html#ggaad9ca9d9d013e066ba14b2f93d512a4fa23232af7d53d18d6ae51cdef22bf89bd", null ],
      [ "kPWM_LowTrue", "group__pwm__driver.html#ggaad9ca9d9d013e066ba14b2f93d512a4fa7db3fb960191fb50008d3c8e17c640eb", null ]
    ] ],
    [ "pwm_reload_source_select_t", "group__pwm__driver.html#ga07f269543e848d4e520300d938a60ec5", [
      [ "kPWM_LocalReload", "group__pwm__driver.html#gga07f269543e848d4e520300d938a60ec5aed91b2aede324d8f2242e5d1baf795d4", null ],
      [ "kPWM_MasterReload", "group__pwm__driver.html#gga07f269543e848d4e520300d938a60ec5a5570b1618c1b54d235fb14df8e2de4d5", null ]
    ] ],
    [ "pwm_fault_clear_t", "group__pwm__driver.html#gaa2b5b38c3958201ca8b446091f0fe71d", [
      [ "kPWM_Automatic", "group__pwm__driver.html#ggaa2b5b38c3958201ca8b446091f0fe71dabf68c2da0cf3a166bfbcb3936261e61d", null ],
      [ "kPWM_ManualNormal", "group__pwm__driver.html#ggaa2b5b38c3958201ca8b446091f0fe71da9afba77080d761b13b132caf69d7b55a", null ],
      [ "kPWM_ManualSafety", "group__pwm__driver.html#ggaa2b5b38c3958201ca8b446091f0fe71da2ab19200e6e01024b4ea5afb1c00091f", null ]
    ] ],
    [ "pwm_module_control_t", "group__pwm__driver.html#ga19711b05aa2441dd54d8e5213362b864", [
      [ "kPWM_Control_Module_0", "group__pwm__driver.html#gga19711b05aa2441dd54d8e5213362b864a1f44c9b0cfe295515db31f8e9d7b166e", null ],
      [ "kPWM_Control_Module_1", "group__pwm__driver.html#gga19711b05aa2441dd54d8e5213362b864adfcd5cb91128996487412233496d9d67", null ],
      [ "kPWM_Control_Module_2", "group__pwm__driver.html#gga19711b05aa2441dd54d8e5213362b864adae0a8676b2afcba6a12b17ea2f68ba6", null ],
      [ "kPWM_Control_Module_3", "group__pwm__driver.html#gga19711b05aa2441dd54d8e5213362b864afb56a21760a3e3b74d4721c0c1028ae2", null ]
    ] ],
    [ "PWM_Init", "group__pwm__driver.html#gab4d245c939c7cc9106065020244d84e2", null ],
    [ "PWM_Deinit", "group__pwm__driver.html#ga23a26fead175fa19dfe4617dde090bb5", null ],
    [ "PWM_GetDefaultConfig", "group__pwm__driver.html#gaead09677ab28bfd57dce26c6c161b18d", null ],
    [ "PWM_SetupPwm", "group__pwm__driver.html#ga50ecefa180c7ab83cb25c8ed58505b43", null ],
    [ "PWM_UpdatePwmDutycycle", "group__pwm__driver.html#ga8324acc824d28acc111006c67746d6ae", null ],
    [ "PWM_SetupInputCapture", "group__pwm__driver.html#ga714adfb30a4bac917530e4f7ac9f8497", null ],
    [ "PWM_SetupFaults", "group__pwm__driver.html#ga6dba321676f0d87b3bd294afd859fd6c", null ],
    [ "PWM_SetupForceSignal", "group__pwm__driver.html#gab149e523b4f42b2ef8380bcffb0ae899", null ],
    [ "PWM_EnableInterrupts", "group__pwm__driver.html#ga16b4ff14585b9b4898c2b0e296f45779", null ],
    [ "PWM_DisableInterrupts", "group__pwm__driver.html#gae4766d9e26c674a3ed9634b11888ce2c", null ],
    [ "PWM_GetEnabledInterrupts", "group__pwm__driver.html#ga448340a1df47236ffbf02c1da40d942a", null ],
    [ "PWM_GetStatusFlags", "group__pwm__driver.html#ga827ba4608c9021cb29b981c716dfe83f", null ],
    [ "PWM_ClearStatusFlags", "group__pwm__driver.html#ga66f5e7f2d2f671b35031e17f17ae5b0d", null ],
    [ "PWM_StartTimer", "group__pwm__driver.html#ga7da5f4910ad7945fee50bd642f871fcc", null ],
    [ "PWM_StopTimer", "group__pwm__driver.html#ga542db369e1459b8b7847d005937a5be9", null ],
    [ "PWM_OutputTriggerEnable", "group__pwm__driver.html#ga40256df62c18de1ab8fef8609d6b4060", null ],
    [ "PWM_SetupSwCtrlOut", "group__pwm__driver.html#ga8236aa4d0e9beba91cdaabdadc841b32", null ],
    [ "PWM_SetPwmLdok", "group__pwm__driver.html#ga7ff017951793deb3e01659ab0f440133", null ]
];