var group__hsadc =
[
    [ "HSADC Peripheral driver", "group__hsadc__driver.html", null ],
    [ "hsadc_config_t", "group__hsadc.html#structhsadc__config__t", [
      [ "dualConverterScanMode", "group__hsadc.html#a397b6e01f4a5596d5d0a167ae3ccc944", null ],
      [ "enableSimultaneousMode", "group__hsadc.html#ae05b5c0e00e41f730360768e17fb40c0", null ],
      [ "resolution", "group__hsadc.html#aa5a563606f8633f80a46d627afcbaaf6", null ],
      [ "DMATriggerSoruce", "group__hsadc.html#a370674fe6f5b3d75a9dc9d47a0feb875", null ],
      [ "idleWorkMode", "group__hsadc.html#a7d3cca50a006b88d38e7d9083d5f1c2c", null ],
      [ "powerUpDelayCount", "group__hsadc.html#ad638218e301637c683b36cb9bb0e82bc", null ]
    ] ],
    [ "hsadc_converter_config_t", "group__hsadc.html#structhsadc__converter__config__t", [
      [ "clockDivisor", "group__hsadc.html#afb91487e520e43d12c77f7ffeab481b5", null ],
      [ "samplingTimeCount", "group__hsadc.html#a0d823ec410df77c81b9365bb4c220f29", null ],
      [ "powerUpCalibrationModeMask", "group__hsadc.html#a076554c5f028220b748d4f4c0c1d4d2d", null ]
    ] ],
    [ "hsadc_sample_config_t", "group__hsadc.html#structhsadc__sample__config__t", [
      [ "channelNumber", "group__hsadc.html#ae9c509f991b9445f5d6ce37510b1bc51", null ],
      [ "channel67MuxNumber", "group__hsadc.html#ad1a0112bf0805dd59a6972a4f486f38e", null ],
      [ "enableDifferentialPair", "group__hsadc.html#a3bfbd0ce58af9b0bc966beb5b29909ce", null ],
      [ "zeroCrossingMode", "group__hsadc.html#ae70dc5cdb8a909533f962688e8284489", null ],
      [ "highLimitValue", "group__hsadc.html#a47e63b464be859de25c1f8411d7ca9f0", null ],
      [ "lowLimitValue", "group__hsadc.html#a80583dbd445185b943924a4f3abed43e", null ],
      [ "offsetValue", "group__hsadc.html#ad0fbceeabfed2ab66180d9952dfcefda", null ],
      [ "enableWaitSync", "group__hsadc.html#abfae91abdbf5f5b1b80858e0defb2a79", null ]
    ] ],
    [ "FSL_HSADC_DRIVER_VERSION", "group__hsadc.html#ga467133a5baf838822aa44e35014587f8", null ],
    [ "HSADC_SAMPLE_MASK", "group__hsadc.html#ga4f5f61111ee6274db96f59ad8545354c", null ],
    [ "HSADC_CALIBRATION_VALUE_A_SINGLE_ENDED_MASK", "group__hsadc.html#ga19c5ef851329d293877b964695eecfe4", null ],
    [ "HSADC_CALIBRATION_VALUE_A_SINGLE_ENDED_SHIFT", "group__hsadc.html#gaa3eedb60ddb6963186b159ba1b1c1cbc", null ],
    [ "HSADC_CALIBRATION_VALUE_A_DIFFERENTIAL_MASK", "group__hsadc.html#gac434f889b9a6537bba8430e7e1c8b97d", null ],
    [ "HSADC_CALIBRATION_VALUE_A_DIFFERENTIAL_SHIFT", "group__hsadc.html#gae1c34029643efcdc9ba858bfde49bee6", null ],
    [ "HSADC_CALIBRATION_VALUE_B_SINGLE_ENDED_MASK", "group__hsadc.html#ga9b8f47935787b3ee4f9f400135c9bf32", null ],
    [ "HSADC_CALIBRATION_VALUE_B_SINGLE_ENDED_SHIFT", "group__hsadc.html#gabfe23fc616a876bd83449526b55dc4c4", null ],
    [ "HSADC_CALIBRATION_VALUE_B_DIFFERENTIAL_MASK", "group__hsadc.html#ga21f6c1953dc93a6e3a75db60ee89040f", null ],
    [ "HSADC_CALIBRATION_VALUE_B_DIFFERENTIAL_SHIFT", "group__hsadc.html#gaa8934e4fa4374a4ea53e53531977918b", null ],
    [ "_hsadc_status_flags", "group__hsadc.html#gab47209632ffe4a03b70d0250978733ea", [
      [ "kHSADC_ZeroCrossingFlag", "group__hsadc.html#ggab47209632ffe4a03b70d0250978733eaa18785fb4955dd066f8a2861284fa29ea", null ],
      [ "kHSADC_HighLimitFlag", "group__hsadc.html#ggab47209632ffe4a03b70d0250978733eaa0afac7b0a997242c8337e2d367d8eb05", null ],
      [ "kHSADC_LowLimitFlag", "group__hsadc.html#ggab47209632ffe4a03b70d0250978733eaa1dce3cb027a27be46b906df1849c2890", null ],
      [ "kHSADC_ConverterAEndOfScanFlag", "group__hsadc.html#ggab47209632ffe4a03b70d0250978733eaacc33460258a557f5478e6a37117cc304", null ],
      [ "kHSADC_ConverterBEndOfScanFlag", "group__hsadc.html#ggab47209632ffe4a03b70d0250978733eaa9ee87c8eeb2cf5a7ae9f096389399156", null ],
      [ "kHSADC_ConverterAEndOfCalibrationFlag", "group__hsadc.html#ggab47209632ffe4a03b70d0250978733eaa7237746e4f485a41978b8bd1a616c850", null ],
      [ "kHSADC_ConverterBEndOfCalibrationFlag", "group__hsadc.html#ggab47209632ffe4a03b70d0250978733eaa9c036e8f40cde6af5110840af794b972", null ],
      [ "kHSADC_ConverterAConvertingFlag", "group__hsadc.html#ggab47209632ffe4a03b70d0250978733eaa07462282b795e033c1de86b6572293e5", null ],
      [ "kHSADC_ConverterBConvertingFlag", "group__hsadc.html#ggab47209632ffe4a03b70d0250978733eaa399a1fd4f9d1744be12a140c41cd619c", null ],
      [ "kHSADC_ConverterADummyConvertingFlag", "group__hsadc.html#ggab47209632ffe4a03b70d0250978733eaa2244136d87866aa760926fce292009f5", null ],
      [ "kHSADC_ConverterBDummyConvertingFlag", "group__hsadc.html#ggab47209632ffe4a03b70d0250978733eaa85379b1075335f8c0610683d5548d6d7", null ],
      [ "kHSADC_ConverterACalibratingFlag", "group__hsadc.html#ggab47209632ffe4a03b70d0250978733eaaa683a62ad15444213b195d26951f1e82", null ],
      [ "kHSADC_ConverterBCalibratingFlag", "group__hsadc.html#ggab47209632ffe4a03b70d0250978733eaa9fc9ae225aa39d4d88822a54de42a911", null ],
      [ "kHSADC_ConverterAPowerDownFlag", "group__hsadc.html#ggab47209632ffe4a03b70d0250978733eaae732550b8f67d6ee7287306864c3e2d8", null ],
      [ "kHSADC_ConverterBPowerDownFlag", "group__hsadc.html#ggab47209632ffe4a03b70d0250978733eaa4bdca2bf096e1f4b35092d8c748e3351", null ]
    ] ],
    [ "_hsadc_interrupt_enable", "group__hsadc.html#ga4a98ef7bff280541751a69421d3a417a", [
      [ "kHSADC_ZeroCrossingInterruptEnable", "group__hsadc.html#gga4a98ef7bff280541751a69421d3a417aacb9ed6dc3e37b3668c11013883710045", null ],
      [ "kHSADC_HighLimitInterruptEnable", "group__hsadc.html#gga4a98ef7bff280541751a69421d3a417aaf6396f43119e1a4180cbc32976511888", null ],
      [ "kHSADC_LowLimitInterruptEnable", "group__hsadc.html#gga4a98ef7bff280541751a69421d3a417aa24c1230e6e27dbc54082523474be9195", null ],
      [ "kHSADC_ConverterAEndOfScanInterruptEnable", "group__hsadc.html#gga4a98ef7bff280541751a69421d3a417aa87af95c028f08efc3cfee5238c0788fe", null ],
      [ "kHSADC_ConverterBEndOfScanInterruptEnable", "group__hsadc.html#gga4a98ef7bff280541751a69421d3a417aa52bc9a6630baf35b927b27c7d86112dc", null ],
      [ "kHSADC_ConverterAEndOfCalibrationInterruptEnable", "group__hsadc.html#gga4a98ef7bff280541751a69421d3a417aaf0eb1d8cbb8ee2fe4102d828b31a5f83", null ],
      [ "kHSADC_ConverterBEndOfCalibrationInterruptEnable", "group__hsadc.html#gga4a98ef7bff280541751a69421d3a417aa76497dc59b1f8e8ecf15049c6db58c72", null ]
    ] ],
    [ "_hsadc_converter_id", "group__hsadc.html#gaf034adf23c1591ccf06625f53654714c", [
      [ "kHSADC_ConverterA", "group__hsadc.html#ggaf034adf23c1591ccf06625f53654714ca56034f202359bcce31c87221d680c2f5", null ],
      [ "kHSADC_ConverterB", "group__hsadc.html#ggaf034adf23c1591ccf06625f53654714ca878cf1df2479a539158df0991f3b45f7", null ]
    ] ],
    [ "hsadc_dual_converter_scan_mode_t", "group__hsadc.html#ga8796286cff5fa08b57a7ea38384ffaed", [
      [ "kHSADC_DualConverterWorkAsOnceSequential", "group__hsadc.html#gga8796286cff5fa08b57a7ea38384ffaedaaa182417ddc98c131325a8170e8ea589", null ],
      [ "kHSADC_DualConverterWorkAsOnceParallel", "group__hsadc.html#gga8796286cff5fa08b57a7ea38384ffaeda42246ff9b44a837323c5970be3881de6", null ],
      [ "kHSADC_DualConverterWorkAsLoopSequential", "group__hsadc.html#gga8796286cff5fa08b57a7ea38384ffaedab6730bb3b5807d9c7adb137dd7bca010", null ],
      [ "kHSADC_DualConverterWorkAsLoopParallel", "group__hsadc.html#gga8796286cff5fa08b57a7ea38384ffaedad85aff4867097cba5df65670422eb8f0", null ],
      [ "kHSADC_DualConverterWorkAsTriggeredSequential", "group__hsadc.html#gga8796286cff5fa08b57a7ea38384ffaeda9f396f9e203eabbe6b91a9d3c67c7a91", null ],
      [ "kHSADC_DualConverterWorkAsTriggeredParallel", "group__hsadc.html#gga8796286cff5fa08b57a7ea38384ffaeda377ae8a68463fbf0961d994e5e4c9b08", null ]
    ] ],
    [ "hsadc_resolution_t", "group__hsadc.html#ga0be304150c94b53dce1b78480b8ab7d3", [
      [ "kHSADC_Resolution6Bit", "group__hsadc.html#gga0be304150c94b53dce1b78480b8ab7d3adf6d0f68952cfdebbffbbe24945a808b", null ],
      [ "kHSADC_Resolution8Bit", "group__hsadc.html#gga0be304150c94b53dce1b78480b8ab7d3ac2440a47d8396ef369ec2f43ee62d3f2", null ],
      [ "kHSADC_Resolution10Bit", "group__hsadc.html#gga0be304150c94b53dce1b78480b8ab7d3afa5af3d28046004c8d6e14aef0caa974", null ],
      [ "kHSADC_Resolution12Bit", "group__hsadc.html#gga0be304150c94b53dce1b78480b8ab7d3a34c8ff7b869e79ec829a511b48ad1f0e", null ]
    ] ],
    [ "hsadc_dma_trigger_source_t", "group__hsadc.html#ga48704e27087fb4579995c2ae9d898062", [
      [ "kHSADC_DMATriggerSourceAsEndOfScan", "group__hsadc.html#gga48704e27087fb4579995c2ae9d898062a47c58f9d2af15f8bdd5edc481fc6e050", null ],
      [ "kHSADC_DMATriggerSourceAsSampleReady", "group__hsadc.html#gga48704e27087fb4579995c2ae9d898062a39680bec6e8f54aa39e29cca10eacc85", null ]
    ] ],
    [ "hsadc_zero_crossing_mode_t", "group__hsadc.html#ga139056b2cdd97173b5ea799494fa2380", [
      [ "kHSADC_ZeroCorssingDisabled", "group__hsadc.html#gga139056b2cdd97173b5ea799494fa2380a27de8c569b83e53e5bc7c875119e8103", null ],
      [ "kHSADC_ZeroCorssingForPtoNSign", "group__hsadc.html#gga139056b2cdd97173b5ea799494fa2380ae33e14ae257e463727e94b30997d5cc0", null ],
      [ "kHSADC_ZeroCorssingForNtoPSign", "group__hsadc.html#gga139056b2cdd97173b5ea799494fa2380a6311a772ebe3698aa6581e1db147c1a7", null ],
      [ "kHSADC_ZeroCorssingForAnySignChanged", "group__hsadc.html#gga139056b2cdd97173b5ea799494fa2380a7224e4312a7b43843092d8b75407ffed", null ]
    ] ],
    [ "hsadc_idle_work_mode_t", "group__hsadc.html#ga83164cd0b18dc4682941162c72f2403b", [
      [ "kHSADC_IdleKeepNormal", "group__hsadc.html#gga83164cd0b18dc4682941162c72f2403ba96fd2d777ae79bd9130518c3e0891037", null ],
      [ "kHSADC_IdleAutoStandby", "group__hsadc.html#gga83164cd0b18dc4682941162c72f2403ba05ebf2c5e09a3275947934ee933d8265", null ],
      [ "kHSADC_IdleAutoPowerDown", "group__hsadc.html#gga83164cd0b18dc4682941162c72f2403ba9c502d83dc593666c9942736583e7f38", null ]
    ] ],
    [ "_hsadc_calibration_mode", "group__hsadc.html#ga67296d83680514113c964cec19c09b61", [
      [ "kHSADC_CalibrationModeDifferential", "group__hsadc.html#gga67296d83680514113c964cec19c09b61a8bc8b573217a031b6e80ced0a2a1156a", null ],
      [ "kHSADC_CalibrationModeSingleEnded", "group__hsadc.html#gga67296d83680514113c964cec19c09b61abfe2173337bbfd5b023b0dc9fb0c1748", null ]
    ] ],
    [ "HSADC_Init", "group__hsadc.html#gafd37291e29285ec245f7dabf118b155c", null ],
    [ "HSADC_GetDefaultConfig", "group__hsadc.html#ga55dec81f34082128e234b4fa3129f33a", null ],
    [ "HSADC_Deinit", "group__hsadc.html#gab654d5d2b1d6a182275167bdb40c489c", null ],
    [ "HSADC_SetConverterConfig", "group__hsadc.html#ga08da9b1db044d4e8d7438df56bebd49a", null ],
    [ "HSADC_GetDefaultConverterConfig", "group__hsadc.html#ga0e5f7d124406952edab22c59e59adb89", null ],
    [ "HSADC_EnableConverter", "group__hsadc.html#ga75fe510eef5ea01f5e6a83cc95469a33", null ],
    [ "HSADC_EnableConverterSyncInput", "group__hsadc.html#ga102a9f03b26bea567067261d90c44832", null ],
    [ "HSADC_EnableConverterPower", "group__hsadc.html#ga893624270e7ec1c8b9884278f2cce4bd", null ],
    [ "HSADC_DoSoftwareTriggerConverter", "group__hsadc.html#gab8f27fbdbf12dda9126a49ae271ea714", null ],
    [ "HSADC_EnableConverterDMA", "group__hsadc.html#ga6cf8af435729336c40b92b06ae26ada0", null ],
    [ "HSADC_EnableInterrupts", "group__hsadc.html#ga995963df2ae9f02a1f4ae58286e66ff0", null ],
    [ "HSADC_DisableInterrupts", "group__hsadc.html#ga562e9c95d5d3876b9514a3450aaf3135", null ],
    [ "HSADC_GetStatusFlags", "group__hsadc.html#ga479527ea9b1baee74a74b8369711d0e4", null ],
    [ "HSADC_ClearStatusFlags", "group__hsadc.html#gac8f04fdbba5c8fc175ead3b0ac4237c4", null ],
    [ "HSADC_SetSampleConfig", "group__hsadc.html#gad819aef5fd1563cc4a6054ea595694d1", null ],
    [ "HSADC_GetDefaultSampleConfig", "group__hsadc.html#ga6109c4fbff08c0b5c7cbdb5301b6eb63", null ],
    [ "HSADC_EnableSample", "group__hsadc.html#ga50f7ad8378944fa2af163cd15a9a5f5b", null ],
    [ "HSADC_EnableSampleResultReadyInterrupts", "group__hsadc.html#ga9a5f551764f3590b1ae242f894bdfec2", null ],
    [ "HSADC_GetSampleReadyStatusFlags", "group__hsadc.html#ga58f74769d7ceb4821e5382bb8cadbeaa", null ],
    [ "HSADC_GetSampleLowLimitStatusFlags", "group__hsadc.html#gac8a5d2c5d763fd7f0ec07f24553b521c", null ],
    [ "HSADC_ClearSampleLowLimitStatusFlags", "group__hsadc.html#gae3fcbf8e7fc1d9b88ab03b20f2584377", null ],
    [ "HSADC_GetSampleHighLimitStatusFlags", "group__hsadc.html#gaf578bbaf53283b7a7941222023b51d7a", null ],
    [ "HSADC_ClearSampleHighLimitStatusFlags", "group__hsadc.html#gaa88a575e7fb56811ed2e06e99f490a40", null ],
    [ "HSADC_GetSampleZeroCrossingStatusFlags", "group__hsadc.html#ga3d156452a79af35387dee7d134d84440", null ],
    [ "HSADC_ClearSampleZeroCrossingStatusFlags", "group__hsadc.html#ga74a456ed46dbbd46a53eb536dd6bc7d7", null ],
    [ "HSADC_GetSampleResultValue", "group__hsadc.html#gaa7c1e6e3a32006d4f80ca2a1c4306d54", null ],
    [ "HSADC_DoAutoCalibration", "group__hsadc.html#gac029eed9dfb4894705c91bba511f7ee2", null ],
    [ "HSADC_GetCalibrationResultValue", "group__hsadc.html#ga0d76b2d790addfddac3b2cc5ad738558", null ],
    [ "HSADC_EnableCalibrationResultValue", "group__hsadc.html#gade96cbccecd85905e29127f1555d4959", null ]
];