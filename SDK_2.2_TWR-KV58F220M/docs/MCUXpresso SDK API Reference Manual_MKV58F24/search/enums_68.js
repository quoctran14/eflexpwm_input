var searchData=
[
  ['hsadc_5fdma_5ftrigger_5fsource_5ft',['hsadc_dma_trigger_source_t',['../group__hsadc.html#ga48704e27087fb4579995c2ae9d898062',1,'fsl_hsadc.h']]],
  ['hsadc_5fdual_5fconverter_5fscan_5fmode_5ft',['hsadc_dual_converter_scan_mode_t',['../group__hsadc.html#ga8796286cff5fa08b57a7ea38384ffaed',1,'fsl_hsadc.h']]],
  ['hsadc_5fidle_5fwork_5fmode_5ft',['hsadc_idle_work_mode_t',['../group__hsadc.html#ga83164cd0b18dc4682941162c72f2403b',1,'fsl_hsadc.h']]],
  ['hsadc_5fresolution_5ft',['hsadc_resolution_t',['../group__hsadc.html#ga0be304150c94b53dce1b78480b8ab7d3',1,'fsl_hsadc.h']]],
  ['hsadc_5fzero_5fcrossing_5fmode_5ft',['hsadc_zero_crossing_mode_t',['../group__hsadc.html#ga139056b2cdd97173b5ea799494fa2380',1,'fsl_hsadc.h']]]
];
