/*
 * Copyright (c) 2016, Freescale Semiconductor, Inc.
 * Copyright 2016-2017 NXP
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "m1_sm_ref_sol.h"
#include "mcdrv.h"
#include "fsl_gpio.h"
#include "fsl_dac.h"
#include "fsl_adc16.h"
/*******************************************************************************
 * Definitions
 ******************************************************************************/

#define M1_MAX_CMT_ERRORS (3) /* maximum number of commutation errors */
#define M1_CURRENT_CONTROLLER_ALIGN_LIM_LOW \
    FRAC16(2.0 / 100.0) /* align current PI controller's low output limit - in percentage / 100 */
#define M1_CURRENT_CONTROLLER_ALIGN_LIM_HIGH \
    FRAC16(90.0 / 100.0) /* align current PI controller's high output limit - in percentage / 100 */

//#define TEST_DEBUG
//#define BACK_EMF
#define ZERO_X
#define HISTORY
/*******************************************************************************
 * Prototypes
 ******************************************************************************/

static void M1_StateFaultFast(int motorNum);
static void M1_StateInitFast(int motorNum);
static void M1_StateStopFast(int motorNum);
static void M1_StateRunFast(int motorNum);

static void M1_StateFaultSlow(int motorNum);
static void M1_StateInitSlow(int motorNum);
static void M1_StateStopSlow(int motorNum);
static void M1_StateRunSlow(int motorNum);

static void M1_TransFaultStop(int motorNum);
static void M1_TransInitFault(int motorNum);
static void M1_TransInitStop(int motorNum);
static void M1_TransStopFault(int motorNum);
static void M1_TransStopRun(int motorNum);
static void M1_TransRunFault(int motorNum);
static void M1_TransRunStop(int motorNum);

static void M1_StateRunCalibFast(int motorNum);
static void M1_StateRunReadyFast(int motorNum);
static void M1_StateRunAlignFast(int motorNum);
static void M1_StateRunStartupFast(int motorNum);
static void M1_StateRunSpinFast(int motorNum);
static void M1_StateRunFreewheelFast(int motorNum);

static void M1_StateRunCalibSlow(int motorNum);
static void M1_StateRunReadySlow(int motorNum);
static void M1_StateRunAlignSlow(int motorNum);
static void M1_StateRunStartupSlow(int motorNum);
static void M1_StateRunSpinSlow(int motorNum);
static void M1_StateRunFreewheelSlow(int motorNum);

static void M1_TransRunCalibReady(int motorNum);
static void M1_TransRunReadyAlign(int motorNum);
static void M1_TransRunAlignStartup(int motorNum);
static void M1_TransRunAlignReady(int motorNum);
static void M1_TransRunStartupSpin(int motorNum);
static void M1_TransRunStartupFreewheel(int motorNum);
static void M1_TransRunSpinFreewheel(int motorNum);
static void M1_TransRunFreewheelReady(int motorNum);

static void M1_FaultDetection(int motorNum);

/*******************************************************************************
 * Variables
 ******************************************************************************/

#define NUM_MOTORS 8
/*! @brief Main control structure */
mcdef_bldc_t g_sM1Drive[NUM_MOTORS];

/*! @brief Main application switch */
bool_t g_bM1SwitchAppOnOff;

/*! @brief M1 structure */
static m1_run_substate_t s_eM1StateRun[NUM_MOTORS];

/*! @brief FreeMASTER scales */
/*! DO NOT USE THEM in the code to avoid float library include */
static volatile float s_fltM1DCBvoltageScale;
static volatile float s_fltM1currentScale;
static volatile float s_fltM1speedScale;

/*! @brief Application state machine table - fast */
const sm_app_state_fcn_t s_STATE_FAST = {M1_StateFaultFast, M1_StateInitFast, M1_StateStopFast, M1_StateRunFast};

/*! @brief Application state machine table - slow */
const sm_app_state_fcn_t s_STATE_SLOW = {M1_StateFaultSlow, M1_StateInitSlow, M1_StateStopSlow, M1_StateRunSlow};

/*! @brief Application sub-state function field - fast */
static const pfcn_void_uint8 s_M1_STATE_RUN_TABLE_FAST[6] = {M1_StateRunCalibFast, M1_StateRunReadyFast,
                                                            M1_StateRunAlignFast, M1_StateRunStartupFast,
                                                            M1_StateRunSpinFast,  M1_StateRunFreewheelFast};

/*! @brief Application sub-state function field - slow */
static const pfcn_void_uint8 s_M1_STATE_RUN_TABLE_SLOW[6] = {M1_StateRunCalibSlow, M1_StateRunReadySlow,
                                                            M1_StateRunAlignSlow, M1_StateRunStartupSlow,
                                                            M1_StateRunSpinSlow,  M1_StateRunFreewheelSlow};

/*! @brief Application state-transition functions field  */
static const sm_app_trans_fcn_t s_TRANS = {M1_TransFaultStop, M1_TransInitFault, M1_TransInitStop, M1_TransStopFault,
                                           M1_TransStopRun,   M1_TransRunFault,  M1_TransRunStop};

/*! @brief  State machine structure declaration and initialization */
/* g_sM1Ctrl.psState, User state functions  */
/* g_sM1Ctrl.psState, User state functions  */
/* g_sM1Ctrl..psTrans, User state-transition functions */
/* g_sM1Ctrl.eState, Default state after reset */
sm_app_ctrl_t g_sM1Ctrl[NUM_MOTORS] = {
		{ &s_STATE_FAST, &s_STATE_SLOW, &s_TRANS, SM_CTRL_NONE, kSM_AppInit},
		{ &s_STATE_FAST, &s_STATE_SLOW, &s_TRANS, SM_CTRL_NONE, kSM_AppInit},
		{ &s_STATE_FAST, &s_STATE_SLOW, &s_TRANS, SM_CTRL_NONE, kSM_AppInit},
		{ &s_STATE_FAST, &s_STATE_SLOW, &s_TRANS, SM_CTRL_NONE, kSM_AppInit},
		{ &s_STATE_FAST, &s_STATE_SLOW, &s_TRANS, SM_CTRL_NONE, kSM_AppInit},
		{ &s_STATE_FAST, &s_STATE_SLOW, &s_TRANS, SM_CTRL_NONE, kSM_AppInit},
		{ &s_STATE_FAST, &s_STATE_SLOW, &s_TRANS, SM_CTRL_NONE, kSM_AppInit},
		{ &s_STATE_FAST, &s_STATE_SLOW, &s_TRANS, SM_CTRL_NONE, kSM_AppInit}
};



/*******************************************************************************
 * Code
 ******************************************************************************/

/*!
 * @brief Fault state called in fast state machine
 *
 * @param void  No input parameter
 *
 * @return None
 */
static void M1_StateFaultFast(int motorNum)
{
    /* type the code to do when in the FAULT state */

    /* determine which ADC has desired phase voltage value */
    g_sM1AdcSensor[motorNum].ui16Sector = g_sM1Drive[motorNum].sCtrlBLDC.i16SectorCmt;

    /* read ADC results (ADC triggered by HW trigger from PDB) */
    /* get all adc samples - DC-bus voltage, current, bemf and aux sample */
    M1_MCDRV_ADC_GET(&g_sM1AdcSensor[motorNum]);

    /* DC-Bus current filter */
    g_sM1Drive[motorNum].sCtrlBLDC.f16IDcBus =
        GDFLIB_FilterIIR1_F16(g_sM1Drive[motorNum].sCtrlBLDC.f16IDcBusNoFilt, &g_sM1Drive[motorNum].sCtrlBLDC.sIDcBusFilter);

    /* DC-Bus voltage filter */
    g_sM1Drive[motorNum].sCtrlBLDC.f16UDcBus =
        GDFLIB_FilterIIR1_F16(g_sM1Drive[motorNum].sCtrlBLDC.f16UDcBusNoFilt, &g_sM1Drive[motorNum].sCtrlBLDC.sUDcBusFilter);

    /* Disable user application switch */
    g_bM1SwitchAppOnOff = FALSE;

    /* clear fault state manually from FreeMASTER */
    if (g_sM1Drive[motorNum].bFaultClearMan)
    {
        /* Clear fault state */
        g_sM1Ctrl[motorNum].uiCtrl |= SM_CTRL_FAULT_CLEAR;
        g_sM1Drive[motorNum].bFaultClearMan = FALSE;
    }

    /* Detects faults */
    M1_FaultDetection(motorNum);
}

/*!
 * @brief State initialization routine called in fast state machine
 *
 * @param void  No input parameter
 *
 * @return None
 */
static void M1_StateInitFast(int motorNum)
{
    /* type the code to do when in the INIT state */

    /* initialize free wheel period */
    g_sM1Drive[motorNum].ui16PeriodFreewheelLong = M1_FREEWHEEL_T_LONG;
    g_sM1Drive[motorNum].ui16PeriodFreewheelShort = M1_FREEWHEEL_T_SHORT;

    /* initialize alignment period */
    g_sM1Drive[motorNum].ui16TimeAlignment = M1_ALIGN_DURATION;
    g_sM1Drive[motorNum].sCtrlBLDC.f16IDcBusAlign = M1_ALIGN_CURRENT;

    /* DC-bus current measurement */
    g_sM1Drive[motorNum].sCtrlBLDC.f16IDcBusLim = M1_I_DCB_LIMIT;
    g_sM1Drive[motorNum].sCtrlBLDC.sIDcBusFilter.sFltCoeff.f32B0 = M1_IDCB_IIR_B0;
    g_sM1Drive[motorNum].sCtrlBLDC.sIDcBusFilter.sFltCoeff.f32B1 = M1_IDCB_IIR_B1;
    g_sM1Drive[motorNum].sCtrlBLDC.sIDcBusFilter.sFltCoeff.f32A1 = M1_IDCB_IIR_A1;
    g_sM1Drive[motorNum].sCtrlBLDC.sUDcBusFilter.sFltCoeff.f32B0 = M1_UDCB_IIR_B0;
    g_sM1Drive[motorNum].sCtrlBLDC.sUDcBusFilter.sFltCoeff.f32B1 = M1_UDCB_IIR_B1;
    g_sM1Drive[motorNum].sCtrlBLDC.sUDcBusFilter.sFltCoeff.f32A1 = M1_UDCB_IIR_A1;

    /* current controller parameters */
    g_sM1Drive[motorNum].sCtrlBLDC.sIDcBusPiParams.a32PGain = M1_TORQUE_LOOP_KP_GAIN;
    g_sM1Drive[motorNum].sCtrlBLDC.sIDcBusPiParams.a32IGain = M1_TORQUE_LOOP_KI_GAIN;
    g_sM1Drive[motorNum].sCtrlBLDC.sIDcBusPiParams.f16UpperLim = M1_CTRL_LOOP_LIM_HIGH;
    g_sM1Drive[motorNum].sCtrlBLDC.sIDcBusPiParams.f16LowerLim = M1_CTRL_LOOP_LIM_LOW;
    g_sM1Drive[motorNum].sCtrlBLDC.bIDcBusPiStopIntFlag = FALSE;

    /* reset required speed */
    g_sM1Drive[motorNum].sCtrlBLDC.f16SpeedCmd = FRAC16(0.0);
    g_sM1Drive[motorNum].sCtrlBLDC.f16SpeedMinimal = M1_N_MIN;

    /* initialize speed ramp */
    g_sM1Drive[motorNum].sCtrlBLDC.sSpeedRampParams.f32RampDown = M1_SPEED_LOOP_RAMP_DOWN;
    g_sM1Drive[motorNum].sCtrlBLDC.sSpeedRampParams.f32RampUp = M1_SPEED_LOOP_RAMP_UP;

    /* speed controller parameters */
    g_sM1Drive[motorNum].sCtrlBLDC.sSpeedPiParams.a32PGain = M1_SPEED_LOOP_KP_GAIN;
    g_sM1Drive[motorNum].sCtrlBLDC.sSpeedPiParams.a32IGain = M1_SPEED_LOOP_KI_GAIN;
    g_sM1Drive[motorNum].sCtrlBLDC.sSpeedPiParams.f16UpperLim = M1_CTRL_LOOP_LIM_HIGH;
    g_sM1Drive[motorNum].sCtrlBLDC.sSpeedPiParams.f16LowerLim = M1_CTRL_LOOP_LIM_LOW;
    g_sM1Drive[motorNum].sCtrlBLDC.bSpeedPiStopIntFlag = FALSE;

    g_sM1Drive[motorNum].sCtrlBLDC.i16SpeedScaleConst = M1_SPEED_SCALE_CONST;
    g_sM1Drive[motorNum].sCtrlBLDC.f16SpeedNominal = M1_N_NOM;

    /* initialize sensorless algorithm */
    g_sM1Drive[motorNum].sCtrlBLDC.i16SectorCmt = 0;
    g_sM1Drive[motorNum].f16StartCmtAcceleration = M1_START_CMT_ACCELER;
    g_sM1Drive[motorNum].ui16StartCmtNumber = M1_STARTUP_CMT_CNT;
    g_sM1Drive[motorNum].ui16PeriodCmtNextInit = M1_STARTUP_CMT_PER;
    g_sM1Drive[motorNum].ui16PeriodToffInit = M1_CMT_T_OFF;
    g_sM1Drive[motorNum].f32UBemfIntegThreshold = M1_INTEG_TRH;

    /* initialize PWM duty cycle input to 0 */
    g_sM1Drive[motorNum].sCtrlBLDC.f16DutyCycleInput = -0.5;

    /* fault thresholds */
    g_sM1Drive[motorNum].sFaultThresholds.f16IDcBusOver = M1_I_DCB_OVERCURRENT;
    g_sM1Drive[motorNum].sFaultThresholds.f16UDcBusOver = M1_U_DCB_OVERVOLTAGE;
    g_sM1Drive[motorNum].sFaultThresholds.f16UDcBusUnder = M1_U_DCB_UNDERVOLTAGE;

    g_sM1Drive[motorNum].ui16TimeFaultRelease = M1_FAULT_DURATION;
    g_sM1Drive[motorNum].ui16TimeCalibration = M1_CALIB_DURATION;

    /* frequencies of control loop and commutation timer for MCAT constant calculation */
    g_sM1Drive[motorNum].ui32FreqCmtTimer = (uint32_t)g_sClockSetup.ui32CmtTimerFreq;
    g_sM1Drive[motorNum].ui16FreqCtrlLoop = CTRL_LOOP_FREQ;
    g_sM1Drive[motorNum].ui16FreqPwm = PWM_FREQ;

    /* Defined scaling for FreeMASTER */
    s_fltM1currentScale = M1_I_MAX;
    s_fltM1DCBvoltageScale = M1_U_DCB_MAX;
    s_fltM1speedScale = M1_N_MAX;

    /* Filter init not to enter to fault */
    g_sM1Drive[motorNum].sCtrlBLDC.sUDcBusFilter.f16FltBfrX[0] =
        (frac16_t)((M1_U_DCB_UNDERVOLTAGE / 2.0) + (M1_U_DCB_OVERVOLTAGE / 2.0));
    g_sM1Drive[motorNum].sCtrlBLDC.sUDcBusFilter.f32FltBfrY[0] =
        (frac32_t)((M1_U_DCB_UNDERVOLTAGE / 2.0) + (M1_U_DCB_OVERVOLTAGE / 2.0)) << 16;

    /* Init sensors/actuators pointers */
    M1_SET_PTR_U_DC_BUS(g_sM1Drive[motorNum].sCtrlBLDC.f16UDcBusNoFilt, motorNum);
    M1_SET_PTR_I_DC_BUS(g_sM1Drive[motorNum].sCtrlBLDC.f16IDcBusNoFilt, motorNum);
    M1_SET_PTR_BEMF_VOLT(g_sM1Drive[motorNum].sCtrlBLDC.f16UPhase, motorNum);
    M1_SET_PTR_AUX_CHAN(g_sM1Drive[motorNum].ui16Aux, motorNum);
    M1_SET_PTR_CNT_ACT(g_sM1Drive[motorNum].ui16TimeCurrent, motorNum);
    M1_SET_PTR_VALUE_ACT(g_sM1Drive[motorNum].ui16TimeCurrentEvent, motorNum);

    /* INIT_DONE command */
    g_sM1Ctrl[motorNum].uiCtrl |= SM_CTRL_INIT_DONE;
}

/*!
 * @brief Stop state routine routine called in fast state machine
 *
 * @param void  No input parameter
 *
 * @return None
 */
static void M1_StateStopFast(int motorNum)
{
    /* type the code to do when in the STOP state */

    /* determine which ADC has desired phase voltage value */
    g_sM1AdcSensor[motorNum].ui16Sector = g_sM1Drive[motorNum].sCtrlBLDC.i16SectorCmt;

    /* read ADC results (ADC triggered by HW trigger from PDB) */
    /* get all adc samples - DC-bus voltage, current, bemf and aux sample */
    M1_MCDRV_ADC_GET(&g_sM1AdcSensor[motorNum]);

    /* DC-Bus current filter */
    g_sM1Drive[motorNum].sCtrlBLDC.f16IDcBus =
        GDFLIB_FilterIIR1_F16(g_sM1Drive[motorNum].sCtrlBLDC.f16IDcBusNoFilt, &g_sM1Drive[motorNum].sCtrlBLDC.sIDcBusFilter);

    /* DC-Bus voltage filter */
    g_sM1Drive[motorNum].sCtrlBLDC.f16UDcBus =
        GDFLIB_FilterIIR1_F16(g_sM1Drive[motorNum].sCtrlBLDC.f16UDcBusNoFilt, &g_sM1Drive[motorNum].sCtrlBLDC.sUDcBusFilter);

    /* calculate BEMF voltage from phase voltage */
    g_sM1Drive[motorNum].sCtrlBLDC.f16UPhaseBemf =
        MLIB_Sub_F16(g_sM1Drive[motorNum].sCtrlBLDC.f16UPhase, (g_sM1Drive[motorNum].sCtrlBLDC.f16UDcBus >> 1));

    g_bM1SwitchAppOnOff = 1;

    /* if the user switches on or set non-zero speed */
    if ((g_bM1SwitchAppOnOff != 0) || (g_sM1Drive[motorNum].sCtrlBLDC.f16SpeedCmd != 0))
    {
        /* Set the switch on */
        g_bM1SwitchAppOnOff = 1;

        /* Start command */
        g_sM1Ctrl[motorNum].uiCtrl |= SM_CTRL_START;
    }

    M1_FaultDetection(motorNum);

    /* If a fault occurred */
    if (g_sM1Drive[motorNum].sFaultIdPending)
    {
        /* Switches to the FAULT state */
        g_sM1Ctrl[motorNum].uiCtrl |= SM_CTRL_FAULT;
    }
}

/*!
 * @brief Run state routine routine called in fast state machine
 *
 * @param void  No input parameter
 *
 * @return None
 */
static void M1_StateRunFast(int motorNum)
{
    /* read timer counter and value registers */
    M1_MCDRV_TMR_CMT_GET(&g_sM1CmtTmr[motorNum]);

    /* type the code to do when in the RUN state */
    /* check application main switch */
    if (!g_bM1SwitchAppOnOff)
    {
        /* Stop command */
        g_sM1Ctrl[motorNum].uiCtrl |= SM_CTRL_STOP;
    }

    /* detect fault */
//    M1_FaultDetection(motorNum);

    /* If a fault occurred */
    if (g_sM1Drive[motorNum].sFaultIdPending != 0)
    {
        /* Switches to the FAULT state */
        g_sM1Ctrl[motorNum].uiCtrl |= SM_CTRL_FAULT;
    }

    /* all ADC result registers must be read before next measurement */

    /* determine which ADC has desired phase voltage value */
    g_sM1AdcSensor[motorNum].ui16Sector = g_sM1Drive[motorNum].sCtrlBLDC.i16SectorCmt;

    /* read ADC results (ADC triggered by HW trigger from PDB) */
    /* get all adc samples - DC-bus voltage, current, bemf and aux sample */
    M1_MCDRV_ADC_GET(&g_sM1AdcSensor[motorNum]);

    /* DC-Bus current filter */
    g_sM1Drive[motorNum].sCtrlBLDC.f16IDcBus =
        GDFLIB_FilterIIR1_F16(g_sM1Drive[motorNum].sCtrlBLDC.f16IDcBusNoFilt, &g_sM1Drive[motorNum].sCtrlBLDC.sIDcBusFilter);

    /* DC-Bus voltage filter */
    g_sM1Drive[motorNum].sCtrlBLDC.f16UDcBus =
        GDFLIB_FilterIIR1_F16(g_sM1Drive[motorNum].sCtrlBLDC.f16UDcBusNoFilt, &g_sM1Drive[motorNum].sCtrlBLDC.sUDcBusFilter);

    /* calculate BEMF voltage from phase voltage */
    g_sM1Drive[motorNum].sCtrlBLDC.f16UPhaseBemf =
        MLIB_Sub_F16(g_sM1Drive[motorNum].sCtrlBLDC.f16UPhase, (g_sM1Drive[motorNum].sCtrlBLDC.f16UDcBus >> 1));

//#define DAC_TEST
#ifdef DAC_TEST
    if(s_eM1StateRun == kRunState_Spin)
    {
    	uint32_t dacValue = 4096*MLIB_AbsSat_F16(g_sM1Drive[motorNum].sCtrlBLDC.f16SpeedMeasured);
    	/* Saturate DAC value to 0xFFF */
        DAC_SetBufferValue(DAC0, 0U, 4000);
    }
#endif
#ifdef HISTORY
	/* Save all ADC histories */
	if((history_index < (MAX_HISTORY_NUM-1)) /*&& (motorNum==0) && (ui16_NumCmt[motorNum] < 5000)*/ && ((s_eM1StateRun[motorNum] == kRunState_Spin) || (s_eM1StateRun[motorNum] == kRunState_Startup)))
	{
		history_index++;
		ui16_BEMFHistory0[history_index] = g_sM1Drive[motorNum].sCtrlBLDC.f16UPhase;
		ui16_UDCBHistory0[history_index] = g_sM1Drive[motorNum].sCtrlBLDC.f16UDcBus;
		ui16_TimeHistory[history_index] = g_sM1Drive[motorNum].ui16TimeCurrent;
		ui16_SectorHistory[history_index] = g_sM1AdcSensor[motorNum].ui16Sector;
		ui16_CurrentHistory[history_index] = g_sM1Drive[motorNum].sCtrlBLDC.f16IDcBus;
	}
#endif

    /* Run sub-state function */
    s_M1_STATE_RUN_TABLE_FAST[s_eM1StateRun[motorNum]](motorNum);

    /* configure ADC for next measurement */
    M1_MCDRV_ADC_ASSIGN_BEMF(&g_sM1AdcSensor[motorNum]);
    M1_MCDRV_ADC_ASSIGN_CURR(&g_sM1AdcSensor[motorNum]);
}

/*!
 * @brief Fault state routine called in slow state machine
 *
 * @param void  No input parameter
 *
 * @return None
 */
static void M1_StateFaultSlow(int motorNum)
{
    /* after fault condition ends wait defined time to clear fault state */
    if (!MC_FAULT_ANY(g_sM1Drive[motorNum].sFaultIdPending))
    {
        if (--g_sM1Drive[motorNum].ui16CounterState == 0)
        {
            /* Clear fault state */
            g_sM1Ctrl[motorNum].uiCtrl |= SM_CTRL_FAULT_CLEAR;
        }
    }
    else
    {
        g_sM1Drive[motorNum].ui16CounterState = g_sM1Drive[motorNum].ui16TimeFaultRelease;
    }
}

/*!
 * @brief Fault state routine called in slow state machine
 *
 * @param void  No input parameter
 *
 * @return None
 */
static void M1_StateInitSlow(int motorNum)
{
}

/*!
 * @brief Stop state routine called in slow state machine
 *
 * @param void  No input parameter
 *
 * @return None
 */
static void M1_StateStopSlow(int motorNum)
{
}

/*!
 * @brief Run state routine called in slow state machine
 *
 * @param void  No input parameter
 *
 * @return None
 */
static void M1_StateRunSlow(int motorNum)
{
    /* Run sub-state function */
    s_M1_STATE_RUN_TABLE_SLOW[s_eM1StateRun[motorNum]](motorNum);
}

/*!
 * @brief Transition from Fault to Stop state
 *
 * @param void  No input parameter
 *
 * @return None
 */
static void M1_TransFaultStop(int motorNum)
{
#ifdef TEST_DEBUG
	PRINTF("M1_TransFaultStop\r\n");
	g_bM1SwitchAppOnOff = 1;
#endif
}

/*!
 * @brief Transition from Init to Fault state
 *
 * @param void  No input parameter
 *
 * @return None
 */
static void M1_TransInitFault(int motorNum)
{
#ifdef TEST_DEBUG
	PRINTF("M1_TransInitFault\r\n");
	g_bSwitchedStates = 1;
#endif
    /* type the code to do when going from the INIT to the FAULT state */
    /* disable PWM outputs */
    M1_MCDRV_PWM3PH_SET_PWM_OUTPUT(&g_sM1Pwm3ph[motorNum], 7);

    /* clear required speed */
    g_sM1Drive[motorNum].sCtrlBLDC.f16SpeedCmd = 0;
}

/*!
 * @brief Transition from Init to Stop state
 *
 * @param void  No input parameter
 *
 * @return None
 */
static void M1_TransInitStop(int motorNum)
{
#ifdef TEST_DEBUG
	PRINTF("M1_TransInitStop\r\n");
	g_bSwitchedStates = 1;
#endif
    /* type the code to do when going from the INIT to the FAULT state */
    /* disable PWM outputs */
    M1_MCDRV_PWM3PH_SET_PWM_OUTPUT(&g_sM1Pwm3ph[motorNum], 7);
}

/*!
 * @brief Transition from Stop to Fault state
 *
 * @param void  No input parameter
 *
 * @return None
 */
static void M1_TransStopFault(int motorNum)
{
#ifdef TEST_DEBUG
	PRINTF("M1_TransStopFault\r\n");
	g_bSwitchedStates = 1;
#endif
    /* Type the code to do when going from the STOP to the FAULT state */
    /* Disable PWM output */
    g_sM1Drive[motorNum].sFaultId = g_sM1Drive[motorNum].sFaultIdPending;
    g_sM1Drive[motorNum].ui16CounterState = g_sM1Drive[motorNum].ui16TimeFaultRelease;
}

/*!
 * @brief Transition from Stop to Run state
 *
 * @param void  No input parameter
 *
 * @return None
 */
static void M1_TransStopRun(int motorNum)
{
#ifdef TEST_DEBUG
	PRINTF("M1_TransStopRun\r\n");
	g_bSwitchedStates = 1;
#endif
    /* type the code to do when going from the STOP to the RUN state */
    /* pass calibration routine duration to state counter*/
    g_sM1Drive[motorNum].ui16CounterState = g_sM1Drive[motorNum].ui16TimeCalibration;

    /* initialize calibration state */
    M1_MCDRV_CURR_CALIB_INIT(&g_sM1AdcSensor[motorNum]);

    /* got to CALIB sub-state */
    s_eM1StateRun[motorNum] = kRunState_Calib;

    /* acknowledge that the system can proceed into the RUN state */
    g_sM1Ctrl[motorNum].uiCtrl |= SM_CTRL_RUN_ACK;
}

/*!
 * @brief Transition from Run to Fault state
 *
 * @param void  No input parameter
 *
 * @return None
 */
static void M1_TransRunFault(int motorNum)
{
#ifdef TEST_DEBUG
	PRINTF("M1_TransRunFault\r\n");
	g_bSwitchedStates = 1;
#endif
    /* type the code to do when going from the RUN to the FAULT state */
    /* turn off application */
    g_bM1SwitchAppOnOff = 0;

    /* clear required speed */
    g_sM1Drive[motorNum].sCtrlBLDC.f16SpeedCmd = FRAC16(0.0);

    /* clear current limitation flag */
    g_sM1Drive[motorNum].sCtrlBLDC.bSpeedPiStopIntFlag = FALSE;

    /* set long free-wheel period */
    g_sM1Drive[motorNum].ui16CounterState = g_sM1Drive[motorNum].ui16PeriodFreewheelLong;

    /* go to FREEWHEEL sub-state */
    s_eM1StateRun[motorNum] = kRunState_Freewheel;
}

/*!
 * @brief Transition from Run to Stop state
 *
 * @param void  No input parameter
 *
 * @return None
 */
static void M1_TransRunStop(int motorNum)
{
#ifdef TEST_DEBUG
	PRINTF("M1_TransRunStop\r\n");
	g_bSwitchedStates = 1;
#endif
    /* type the code to do when going from the RUN to the STOP state */
    /* disable PWM outputs */
    M1_MCDRV_PWM3PH_SET_PWM_OUTPUT(&g_sM1Pwm3ph[motorNum], 7);

    g_sM1Drive[motorNum].sCtrlBLDC.bSpeedPiStopIntFlag = FALSE;

    /* reinitialize PI controllers and duty cycle */
    GFLIB_CtrlPIpAWInit_F16(FRAC16(0.0), &g_sM1Drive[motorNum].sCtrlBLDC.sIDcBusPiParams);
    g_sM1Drive[motorNum].sCtrlBLDC.f16IDcBusPiOutput = FRAC16(0.0);
    GFLIB_CtrlPIpAWInit_F16(FRAC16(0.0), &g_sM1Drive[motorNum].sCtrlBLDC.sSpeedPiParams);
    g_sM1Drive[motorNum].sCtrlBLDC.f16SpeedPiOutput = FRAC16(0.0);
    g_sM1Drive[motorNum].sCtrlBLDC.f16DutyCycle = FRAC16(0.0);

    /* clear speed command */
    g_sM1Drive[motorNum].sCtrlBLDC.f16SpeedCmd = 0;

    /* enter READY sub-state */
    s_eM1StateRun[motorNum] = kRunState_Ready;

    /* acknowledge that the system can proceed into the STOP state */
    g_sM1Ctrl[motorNum].uiCtrl |= SM_CTRL_STOP_ACK;
}

/*!
 * @brief Calibration process called in fast state machine as Run sub state
 *
 * @param void  No input parameter
 *
 * @return None
 */
static void M1_StateRunCalibFast(int motorNum)
{
#ifdef TEST_DEBUG
	if(g_bSwitchedStates)
	{
		PRINTF("M1_StateRunCalibFast\r\n");
		g_bSwitchedStates = 0;
	}
#endif
#ifdef DEBUG_MULTI
	printf("\r\n Motor Number: %i CALIB FAST \r\n", motorNum);
#endif
	if (motorNum==1)
	{
//		printf("\r\nMOTOR 1 STUCK IN CALIB\r\n");
		return;
	}
}

/*!
 * @brief Ready state called in fast state machine as Run sub state
 *
 * @param void  No input parameter
 *
 * @return None
 */
static void M1_StateRunReadyFast(int motorNum)
{
#ifdef TEST_DEBUG
	if(g_bSwitchedStates)
	{
//		PRINTF("M1_StateRunReadyFast\r\n");
		g_bSwitchedStates = 0;
	}
#endif
#ifdef DEBUG_MULTI
	printf("\r\n Motor Number: %i READY FAST \r\n", motorNum);
#endif
}

/*!
 * @brief Alignment process called in fast state machine as Run sub state
 *
 * @param void  No input parameter
 *
 * @return None
 */
static void M1_StateRunAlignFast(int motorNum)
{
#ifdef TEST_DEBUG
	if(g_bSwitchedStates)
	{
		PRINTF("M1_StateRunAlignFast\r\n");
		g_bSwitchedStates = 0;
	}
#endif
//	if (motorNum == 1)
//	{
//		return;
//	}
#ifdef DEBUG_MULTI
	printf("\r\n Motor Number: %i READY FAST \r\n", motorNum);
#endif
    /* type the code to do when in the RUN ALIGN sub-state */
    MCS_BLDCAlignment(&g_sM1Drive[motorNum].sCtrlBLDC);

    /* update PWM duty cycle and set sector */
//    if (motorNum == 0)
    {
    g_sM1Drive[motorNum].sCtrlBLDC.f16DutyCycle = M1_ALIGN_DUTY;
    M1_MCDRV_PWM3PH_SET_DUTY(&g_sM1Pwm3ph[motorNum], g_sM1Drive[motorNum].sCtrlBLDC.f16DutyCycle);

    }
}

/*!
 * @brief Start-up process called in fast state machine as Run sub state
 *
 * @param void  No input parameter
 *
 * @return None
 */
static void M1_StateRunStartupFast(int motorNum)
{
#ifdef TEST_DEBUG
	if(g_bSwitchedStates)
	{
		PRINTF("M1_StateRunStartupFast\r\n");
		g_bSwitchedStates = 0;
	}
#endif

//	if (motorNum == 1)
//	{
//		return;
//	}
#ifdef DEBUG_MULTI
	printf("\r\n Motor Number: %i STARTUP FAST \r\n", motorNum);
#endif


#ifdef ZERO_X
    /* blanking period used to ignore inductive spike at the beginning of each commutation */
	g_sM1Drive[motorNum].ui16PeriodToff = MLIB_Mul_F16(g_sM1Drive[motorNum].ui16PeriodCmtNext, FRAC16(0.3));

    /* check Toff period has passed after commutation event */
    if ((uint16_t)MLIB_SubSat_F16(g_sM1Drive[motorNum].ui16TimeCurrent, g_sM1Drive[motorNum].ui16TimeOfCmt) >= g_sM1Drive[motorNum].ui16PeriodToff)
    {
//#ifdef HISTORY
//				if(motorNum==0)
//					ui16_SectorHistory[history_index] = 20;
//#endif
//    		if(sBEMFCurr[motorNum] == kZXState_Init)
//    		{
//    			sBEMFCurr[motorNum] = kZXState_High;
//    		}

			if (potentialZeroXPN[motorNum])
			{
				if (g_sM1Drive[motorNum].sCtrlBLDC.f16UPhaseBemf <= FRAC16(0.0))
				{
					g_bZeroCross[motorNum] = true;
					sBEMFCurr[motorNum] = kZXState_Low;
					potentialZeroXPN[motorNum] = false;
				}
				else
				{
					sBEMFCurr[motorNum] = kZXState_High;
					potentialZeroXPN[motorNum] = false;
				}
			}
			else if (potentialZeroXNP[motorNum])
			{
				if (g_sM1Drive[motorNum].sCtrlBLDC.f16UPhaseBemf >= FRAC16(0.0))
				{
					g_bZeroCross[motorNum] = true;
					sBEMFCurr[motorNum] = kZXState_High;
					potentialZeroXNP[motorNum] = false;
				}
				else
				{
					// Indicate its a false crossing
					sBEMFCurr[motorNum] = kZXState_Low;
					potentialZeroXNP[motorNum] = false;

				}

			}
			/* This condition is mainly for high speeds */
			else if (g_sM1Drive[motorNum].sCtrlBLDC.f16UPhaseBemf == FRAC16(0.0))
			{
				if(sBEMFPrev[motorNum] == kZXState_Low )//&& ((uint16_t)MLIB_SubSat_F16(g_sM1Drive[motorNum].ui16TimeCurrent, ui16_ZeroXPrev) >= (M1_CMT_PER_MIN >> 1)))
				{
					/* zero-cross detected */
					potentialZeroXNP[motorNum] = TRUE;
					ui16PotentialZeroXTime[motorNum] = g_sM1Drive[motorNum].ui16TimeCurrent;
					/* update BEMF state */
//					sBEMFCurr[motorNum] = kZXState_High;
				}
				else if (sBEMFPrev[motorNum]  == kZXState_High)
				{
					/* zero-cross detected */
					potentialZeroXPN[motorNum] = TRUE;
					ui16PotentialZeroXTime[motorNum] = g_sM1Drive[motorNum].ui16TimeCurrent;
					/* update BEMF state */
					sBEMFCurr[motorNum] = kZXState_Low;
				}
			}
			else if (g_sM1Drive[motorNum].sCtrlBLDC.f16UPhaseBemf > FRAC16(0.0))
			{
				/* if back EMF is positive */
				/* update BEMF state */

//				sBEMFCurr[motorNum] = kZXState_High;
				if(sBEMFCurr[motorNum] == kZXState_Init)
				{
					sBEMFCurr[motorNum] = kZXState_High;
				}

				if(sBEMFPrev[motorNum] == kZXState_Low )//&& ((uint16_t)MLIB_SubSat_F16(g_sM1Drive[motorNum].ui16TimeCurrent, ui16_ZeroXPrev) >= (M1_CMT_PER_MIN >> 1)))
				{
					/* zero-cross detected */
					potentialZeroXNP[motorNum] = TRUE;
					ui16PotentialZeroXTime[motorNum] = g_sM1Drive[motorNum].ui16TimeCurrent;
				}
			}
			else //if (g_sM1Drive[motorNum].sCtrlBLDC.f16UPhaseBemf < 0)
			{
				/* if back EMF is negative */
				/* update BEMF state */
//				sBEMFCurr[motorNum] = kZXState_Low;
				if(sBEMFCurr[motorNum] == kZXState_Init)
				{
					sBEMFCurr[motorNum] = kZXState_Low;
				}

				if(sBEMFPrev[motorNum] == kZXState_High) //&& ((uint16_t)MLIB_SubSat_F16(g_sM1Drive[motorNum].ui16TimeCurrent, ui16_ZeroXPrev) >= (M1_CMT_PER_MIN >> 1)))
				{
					/* zero-cross detected */
					potentialZeroXPN[motorNum] = TRUE;
					ui16PotentialZeroXTime[motorNum] = g_sM1Drive[motorNum].ui16TimeCurrent;
				}
			}

		if(g_bZeroCross[motorNum])
		{
			/* reset zero crossing flag */
			g_bZeroCross[motorNum] = FALSE;
			/* get current zero crossing time */
			ui16_ZeroXCurr[motorNum] = ui16PotentialZeroXTime[motorNum];


			if((sBEMFPrev[motorNum] != kZXState_Init) && (g_sM1Drive[motorNum].ui16CounterStartCmt == 0))
			{
				/* if no time counter overflow */
				ui16ZeroXTime[motorNum] = (uint16_t)MLIB_Sub_F16(ui16_ZeroXCurr[motorNum], ui16_ZeroXPrev[motorNum]);

				/* next commutation is time between zero crossing divided by 2 */
				g_sM1Drive[motorNum].ui16PeriodCmtNext = MLIB_Sh1R_F16(ui16ZeroXTime[motorNum]);
				g_sM1Drive[motorNum].ui16PeriodCmt[g_sM1Drive[motorNum].sCtrlBLDC.i16SectorCmt] = g_sM1Drive[motorNum].ui16PeriodCmtNext;
				/* request next time event */
				g_sM1Drive[motorNum].ui16TimeNextEvent = MLIB_Add_F16(ui16_ZeroXCurr[motorNum], g_sM1Drive[motorNum].ui16PeriodCmtNext);
				M1_MCDRV_TMR_CMT_SET(&g_sM1CmtTmr[motorNum], g_sM1Drive[motorNum].ui16TimeNextEvent);

				/* sensorless commutation is running */
				g_sM1Drive[motorNum].bCommutatedSnsless = TRUE;

#ifdef HISTORY
				if(motorNum==0)
					ui16_SectorHistory[history_index] = 15;
#endif
				ui16_ZeroXPrev[motorNum] = ui16_ZeroXCurr[motorNum];
		        M1_TransRunStartupSpin(motorNum);
			}


			/* save current zero crossing time */
			ui16_ZeroXPrev[motorNum] = ui16_ZeroXCurr[motorNum];


		}

		sBEMFPrev[motorNum] = sBEMFCurr[motorNum];

    }
#endif


}

/*!
 * @brief Spin state called in fast state machine as Run sub state
 *
 * @param void  No input parameter
 *
 * @return None
 */
static void M1_StateRunSpinFast(int motorNum)
{

#ifdef TEST_DEBUG
	if(g_bSwitchedStates)
	{
		PRINTF("M1_StateRunSpinFast\r\n");
		g_bSwitchedStates = 0;
	}
#endif
#ifdef DEBUG_MULTI
	printf("\r\n Motor Number: %i FASt SPin \r\n", motorNum);
#endif
//	if (motorNum == 1)
//	{
//		return;
//	}
//	if (motorNum == 0)
//	{
//		return;
//	}

#ifdef ZERO_X
    /* check Toff period has passed after commutation event */
    if ((uint16_t)MLIB_SubSat_F16(g_sM1Drive[motorNum].ui16TimeCurrent, g_sM1Drive[motorNum].ui16TimeOfCmt) >= g_sM1Drive[motorNum].ui16PeriodToff)
    {

		if (potentialZeroXPN[motorNum])
		{
			if (g_sM1Drive[motorNum].sCtrlBLDC.f16UPhaseBemf <= FRAC16(0.0))
			{
				g_bZeroCross[motorNum] = true;
				sBEMFCurr[motorNum] = kZXState_Low;
				potentialZeroXPN[motorNum] = false;
			}
			else
			{
				sBEMFCurr[motorNum] = kZXState_High;
				potentialZeroXPN[motorNum] = false;
			}
		}
		else if (potentialZeroXNP[motorNum])
		{
			if (g_sM1Drive[motorNum].sCtrlBLDC.f16UPhaseBemf >= FRAC16(0.0))
			{
				g_bZeroCross[motorNum] = true;
				sBEMFCurr[motorNum] = kZXState_High;
				potentialZeroXNP[motorNum] = false;
			}
			else
			{
				// Indicate its a false crossing
				sBEMFCurr[motorNum] = kZXState_Low;
				potentialZeroXNP[motorNum] = false;

			}

		}
		/* This condition is mainly for high speeds */
		else if (g_sM1Drive[motorNum].sCtrlBLDC.f16UPhaseBemf == FRAC16(0.0))
		{
			if(sBEMFPrev[motorNum] == kZXState_Low )//&& ((uint16_t)MLIB_SubSat_F16(g_sM1Drive[motorNum].ui16TimeCurrent, ui16_ZeroXPrev) >= (M1_CMT_PER_MIN >> 1)))
			{
				/* zero-cross detected */
				potentialZeroXNP[motorNum] = TRUE;
				ui16PotentialZeroXTime[motorNum] = g_sM1Drive[motorNum].ui16TimeCurrent;
				/* update BEMF state */
				sBEMFCurr[motorNum] = kZXState_High;
			}
			else if (sBEMFPrev[motorNum]  == kZXState_High)
			{
				/* zero-cross detected */
				potentialZeroXPN[motorNum] = TRUE;
				ui16PotentialZeroXTime[motorNum] = g_sM1Drive[motorNum].ui16TimeCurrent;
				/* update BEMF state */
				sBEMFCurr[motorNum] = kZXState_Low;
			}
		}
		else if (g_sM1Drive[motorNum].sCtrlBLDC.f16UPhaseBemf > FRAC16(0.0))
		{
			/* if back EMF is positive */
			/* update BEMF state */
			sBEMFCurr[motorNum] = kZXState_High;

			if((sBEMFPrev[motorNum] == kZXState_Low ))// && ((uint16_t)MLIB_SubSat_F16(g_sM1Drive[motorNum].ui16TimeCurrent, ui16_ZeroXPrev[motorNum]) >= (M1_CMT_PER_MIN >> 1)))
			{
				/* zero-cross detected */
				potentialZeroXNP[motorNum] = TRUE;
				ui16PotentialZeroXTime[motorNum] = g_sM1Drive[motorNum].ui16TimeCurrent;
			}
		}
		else //if (g_sM1Drive[motorNum].sCtrlBLDC.f16UPhaseBemf < 0)
		{
			/* if back EMF is negative */
			/* update BEMF state */
			sBEMFCurr[motorNum] = kZXState_Low;

			if((sBEMFPrev[motorNum] == kZXState_High))// && ((uint16_t)MLIB_SubSat_F16(g_sM1Drive[motorNum].ui16TimeCurrent, ui16_ZeroXPrev[motorNum]) >= (M1_CMT_PER_MIN >> 1)))
			{
				/* zero-cross detected */
				potentialZeroXPN[motorNum] = TRUE;
				ui16PotentialZeroXTime[motorNum] = g_sM1Drive[motorNum].ui16TimeCurrent;
			}
		}

		if(g_bZeroCross[motorNum])
		{
#ifdef HISTORY
			if(motorNum==0)
				ui16_SectorHistory[history_index] = 10;
#endif
			/* reset zero crossing flag */
			g_bZeroCross[motorNum] = FALSE;
			/* get current zero crossing time */
			ui16_ZeroXCurr[motorNum] = ui16PotentialZeroXTime[motorNum];

			if(sBEMFPrev != kZXState_Init)
			{
				/* if no time counter overflow */
				ui16ZeroXTime[motorNum] = (uint16_t)MLIB_Sub_F16(ui16_ZeroXCurr[motorNum], ui16_ZeroXPrev[motorNum]);

				/* next commutation is time between zero crossing divided by 2 */
				g_sM1Drive[motorNum].ui16PeriodCmtNext = MLIB_Sh1R_F16(ui16ZeroXTime[motorNum]);

				/* request next time event */
				g_sM1Drive[motorNum].ui16TimeNextEvent = MLIB_Add_F16(ui16_ZeroXCurr[motorNum], g_sM1Drive[motorNum].ui16PeriodCmtNext);
//				g_sM1Drive[motorNum].ui16PeriodCmt[g_sM1Drive[motorNum].sCtrlBLDC.i16SectorCmt] = g_sM1Drive[motorNum].ui16PeriodCmtNext;
				M1_MCDRV_TMR_CMT_SET(&g_sM1CmtTmr[motorNum], g_sM1Drive[motorNum].ui16TimeNextEvent);

				/* sensorless commutation is running */
				g_sM1Drive[motorNum].bCommutatedSnsless = TRUE;

			}

		}
		/* save current zero crossing time */
		ui16_ZeroXPrev[motorNum] = ui16_ZeroXCurr[motorNum];
		sBEMFPrev[motorNum] = sBEMFCurr[motorNum];

    }
#endif

#ifdef BACK_EMF
    /* check Toff period after commutation event */
    if ((uint16_t)MLIB_SubSat_F16(g_sM1Drive[motorNum].ui16TimeCurrent, g_sM1Drive[motorNum].ui16TimeOfCmt) >= g_sM1Drive[motorNum].ui16PeriodToff)
    {

        /* mirror BEMF voltage according to the sector (change falling BEMF to rising BEMF) */
        if (!((g_sM1Drive[motorNum].sCtrlBLDC.i16SectorCmt ^ g_sM1Drive[motorNum].sCtrlBLDC.ui16MotorDir) & 1))
            g_sM1Drive[motorNum].sCtrlBLDC.f16UPhaseBemf = MLIB_Neg_F16(g_sM1Drive[motorNum].sCtrlBLDC.f16UPhaseBemf);

        /* integrate if positive BEMF voltage */
        if (g_sM1Drive[motorNum].sCtrlBLDC.f16UPhaseBemf > 0)
        {
            if (g_sM1Drive[motorNum].f32UBemfIntegSum == 0)
            {
                g_sM1Drive[motorNum].sCtrlBLDC.f16IDcBusZC = g_sM1Drive[motorNum].sCtrlBLDC.f16IDcBus;
            }
            g_sM1Drive[motorNum].f32UBemfIntegSum += (frac32_t)g_sM1Drive[motorNum].sCtrlBLDC.f16UPhaseBemf;
        }

        /* check whether the integral of BEMF voltage threshold has been reached */
        if (g_sM1Drive[motorNum].f32UBemfIntegSum >= g_sM1Drive[motorNum].f32UBemfIntegThreshold)
        {
            /* check whether commutation period was longer than minimum */
            if ((uint16_t)MLIB_SubSat_F16(g_sM1Drive[motorNum].ui16TimeCurrent, g_sM1Drive[motorNum].ui16TimeOfCmt) < (M1_CMT_PER_MIN >> 1))
            {
                /* PWM output disable request - actual commutation period was shorter than minimum */
                M1_MCDRV_PWM3PH_SET_PWM_OUTPUT(&g_sM1Pwm3ph, 7);

                /* set long free-wheel period */
                g_sM1Drive[motorNum].ui16CounterState = g_sM1Drive[motorNum].ui16PeriodFreewheelLong;

                /* go to FREEWHEEL state */
                M1_TransRunSpinFreewheel();
                return;
            }


            /* save commutation time and periods to calculate speed */
            g_sM1Drive[motorNum].ui16TimeOfCmtOld = g_sM1Drive[motorNum].ui16TimeOfCmt;
            g_sM1Drive[motorNum].ui16TimeOfCmt = g_sM1Drive[motorNum].ui16TimeCurrent;
            g_sM1Drive[motorNum].ui16PeriodCmtNext =
                (uint16_t)MLIB_Sub_F16(g_sM1Drive[motorNum].ui16TimeOfCmt, g_sM1Drive[motorNum].ui16TimeOfCmtOld);
            g_sM1Drive[motorNum].ui16PeriodCmt[g_sM1Drive[motorNum].sCtrlBLDC.i16SectorCmt] = g_sM1Drive[motorNum].ui16PeriodCmtNext;

            /* perform commutation */
            MCS_BLDCCommutation(&g_sM1Drive[motorNum].sCtrlBLDC);

            /* request sector change */
            M1_MCDRV_PWM3PH_SET_PWM_OUTPUT(&g_sM1Pwm3ph, g_sM1Drive[motorNum].sCtrlBLDC.i16SectorCmt);

            /* calculate Toff time */
            g_sM1Drive[motorNum].ui16PeriodToff = MLIB_Mul_F16(g_sM1Drive[motorNum].ui16PeriodCmtNext, g_sM1Drive[motorNum].ui16PeriodToffInit);

            /* calculate next time of safety commutation  */
            g_sM1Drive[motorNum].ui16PeriodCmtNext = MLIB_Sh1LSat_F16(g_sM1Drive[motorNum].ui16PeriodCmtNext);

            /* request next time event */
            g_sM1Drive[motorNum].ui16TimeNextEvent = MLIB_Add_F16(g_sM1Drive[motorNum].ui16TimeCurrent, g_sM1Drive[motorNum].ui16PeriodCmtNext);
            M1_MCDRV_TMR_CMT_SET(&g_sM1CmtTmr, g_sM1Drive[motorNum].ui16TimeNextEvent);

            /* reset BEMF integral value */
            g_sM1Drive[motorNum].f32UBemfIntegSum = 0;

            /* decrement commutation error counter */
            if (g_sM1Drive[motorNum].ui16CounterCmtError > 0)
                g_sM1Drive[motorNum].ui16CounterCmtError--;

            /* raise sensorless commutation flag */
            g_sM1Drive[motorNum].bCommutatedSnsless = TRUE;

        }
    }
#endif
}

/*!
 * @brief Free-wheel process called in fast state machine as Run sub state
 *
 * @param void  No input parameter
 *
 * @return None
 */
static void M1_StateRunFreewheelFast(int motorNum)
{
#ifdef TEST_DEBUG
	if(g_bSwitchedStates)
	{
		PRINTF("M1_StateRunFreewheelFast\r\n");
		g_bSwitchedStates = 0;
	}
#endif
}

/*!
 * @brief Calibration process called in slow state machine as Run sub state
 *
 * @param void  No input parameter
 *
 * @return None
 */
static void M1_StateRunCalibSlow(int motorNum)
{
#ifdef TEST_DEBUG
	if(g_bSwitchedStates)
	{
		PRINTF("M1_StateRunCalibSlow\r\n");
		g_bSwitchedStates = 0;
	}
#endif
    /* type the code to do when in the RUN CALIB sub-state */
//	if (motorNum == 1)
//	{
//		return;
//	}
    /* integrate DC-bus current offset */
    M1_MCDRV_CURR_CALIB(&g_sM1AdcSensor[motorNum]);

    /* wait until the calibration duration passes */
    if (--g_sM1Drive[motorNum].ui16CounterState == 0)
    {
        /* set the DC-bus measurement offset value */
        M1_MCDRV_CURR_CALIB_SET(&g_sM1AdcSensor[motorNum]);

        /* Transition to the RUN READY sub-state */
        M1_TransRunCalibReady(motorNum);
    }
}

/*!
 * @brief Ready state called in slow state machine as Run sub state
 *
 * @param void  No input parameter
 *
 * @return None
 */
static void M1_StateRunReadySlow(int motorNum)
{
#ifdef TEST_DEBUG
	if(g_bSwitchedStates)
	{
		PRINTF("M1_StateRunReadySlow\r\n");
		g_bSwitchedStates = 0;
	}
#endif
//	if (motorNum == 1)
//	{
//		return;
//	}
	g_sM1Drive[motorNum].sCtrlBLDC.f16SpeedCmd = MOTOR_COMMAND_SPEED;
    /* if the required speed is higher than minimal, switch to Ready state */
    if (MLIB_AbsSat_F16(g_sM1Drive[motorNum].sCtrlBLDC.f16SpeedCmd) > g_sM1Drive[motorNum].sCtrlBLDC.f16SpeedMinimal)
    {
        /* check required spin direction */
        if (g_sM1Drive[motorNum].sCtrlBLDC.f16SpeedCmd > 0)
            g_sM1Drive[motorNum].sCtrlBLDC.ui16MotorDir = 0;
        else
            g_sM1Drive[motorNum].sCtrlBLDC.ui16MotorDir = 1;

        /* Transition to the RUN ALIGN sub-state */
        M1_TransRunReadyAlign(motorNum);
    }
}

/*!
 * @brief Alignment process called in slow state machine as Run sub state
 *
 * @param void  No input parameter
 *
 * @return None
 */
static void M1_StateRunAlignSlow(int motorNum)
{
#ifdef TEST_DEBUG
	if(g_bSwitchedStates)
	{
		PRINTF("M1_StateRunAlignSlow\r\n");
		g_bSwitchedStates = 0;
	}
#endif
    /* check if the required speed is lower than minimal */
//    if (MLIB_AbsSat_F16(g_sM1Drive[motorNum].sCtrlBLDC.f16SpeedCmd) < g_sM1Drive[motorNum].sCtrlBLDC.f16SpeedMinimal)
//    {
//        s_eM1StateRun[motorNum] = kRunState_Freewheel;
//        return;
//    }

//	if (motorNum==1)
//	{
//		return;
//	}
    if (--g_sM1Drive[motorNum].ui16CounterState == 0)
    {
        /* Transition to the RUN STARTUP sub-state */
        M1_TransRunAlignStartup(motorNum);
    }

    /* If zero speed command go back to Ready */
//    if (g_sM1Drive[motorNum].sCtrlBLDC.f16SpeedCmd == 0)
//    {
//        M1_TransRunAlignReady();
//    }

}

/*!
 * @brief Start-up process called in slow state machine as Run sub state
 *
 * @param void  No input parameter
 *
 * @return None
 */
static void M1_StateRunStartupSlow(int motorNum)
{
#ifdef TEST_DEBUG
	if(g_bSwitchedStates)
	{
		PRINTF("M1_StateRunStartupSlow\r\n");
		g_bSwitchedStates = 0;
	}
#endif
    /* check if the required speed is lower than minimal */
//    if (MLIB_AbsSat_F16(g_sM1Drive[motorNum].sCtrlBLDC.f16SpeedCmd) < g_sM1Drive[motorNum].sCtrlBLDC.f16SpeedMinimal)
//    {
//        /* Transition to the RUN FREEWHEEL sub-state */
//        M1_TransRunStartupFreewheel();
//    }
}

/*!
 * @brief Spin state called in slow state machine as Run sub state
 *
 * @param void  No input parameter
 *
 * @return None
 */
static void M1_StateRunSpinSlow(int motorNum)
{
#ifdef TEST_DEBUG
	if(g_bSwitchedStates)
	{
		PRINTF("M1_StateRunSpinSlow\r\n");
		g_bSwitchedStates = 0;
	}
#endif
//	if (motorNum==1)
//	{
//		return;
//	}
    /* variables are added due to issue with optimization */
    frac16_t f16SpeedMeasTemp = 0;
    frac16_t f16SpeedRampTemp = 0;

    /* calculate absolute value of average speed */
    g_sM1Drive[motorNum].sCtrlBLDC.ui32PeriodSixCmtSum =
        ((uint32_t)g_sM1Drive[motorNum].ui16PeriodCmt[0]) + ((uint32_t)g_sM1Drive[motorNum].ui16PeriodCmt[1]) +
        ((uint32_t)g_sM1Drive[motorNum].ui16PeriodCmt[2]) + ((uint32_t)g_sM1Drive[motorNum].ui16PeriodCmt[3]) +
        ((uint32_t)g_sM1Drive[motorNum].ui16PeriodCmt[4]) + ((uint32_t)g_sM1Drive[motorNum].ui16PeriodCmt[5]);

    /* call BLDC control loop - speed and current PI controller */
	if (ui16_NumCmt[motorNum] < 9950)
	{
		START_PI = true;
	}
    if (START_PI)
    {
    	/* Read PWM Input duty cycle */
    	capture1Val = FTM3->CONTROLS[0].CnV;
    	capture2Val = FTM3->CONTROLS[1].CnV;
    	if (capture1Val > capture2Val)
    			{
    				capture2Val += 0xFFFF;
    			}
    	floatPulseWidth = (float_t)((capture2Val - capture1Val) + 1) / (FTM_SOURCE_CLOCK / 1000000);
    	floatPulseWidth *= 128;
//    	PRINTF("\r\nInput signals pulse width=%f percent\r\n", floatPulseWidth);
    //	PRINTF("\r\nClock Source=%i percent\r\n", FTM_SOURCE_CLOCK);

    	floatPulseWidth = (floatPulseWidth-2000);
    	floatPulseWidth *= (1.66666);
    	floatPulseWidth *= -0.0001;
    	if (floatPulseWidth > LOWER_LIMIT)
    	{
    		floatPulseWidth = LOWER_LIMIT;
    	}
    	if (floatPulseWidth < UPPER_LIMIT)
    	{
    		floatPulseWidth = UPPER_LIMIT;
    	}
    	g_sM1Drive[motorNum].sCtrlBLDC.f16DutyCycleInput = FRAC16(floatPulseWidth);
    	MCS_BLDCControl(&g_sM1Drive[motorNum].sCtrlBLDC);
    	M1_MCDRV_PWM3PH_SET_DUTY(&g_sM1Pwm3ph[motorNum], g_sM1Drive[motorNum].sCtrlBLDC.f16DutyCycle);
    }
    /* update duty cycle */
//
//    /* check minimal speed boundary */
//    f16SpeedMeasTemp = MLIB_AbsSat_F16(g_sM1Drive[motorNum].sCtrlBLDC.f16SpeedMeasured);
//    f16SpeedRampTemp = MLIB_AbsSat_F16(g_sM1Drive[motorNum].sCtrlBLDC.f16SpeedRamp);
//    if ((f16SpeedMeasTemp < g_sM1Drive[motorNum].sCtrlBLDC.f16SpeedMinimal) ||
//        (f16SpeedRampTemp < g_sM1Drive[motorNum].sCtrlBLDC.f16SpeedMinimal))
//    {
//        /* set long free-wheel period */
//        g_sM1Drive[motorNum].ui16CounterState = g_sM1Drive[motorNum].ui16PeriodFreewheelLong;
//
//        /* clear measured speed values */
//        g_sM1Drive[motorNum].sCtrlBLDC.f16SpeedMeasured = 0;
//
//        /* go to FREEWHEEL state */
//        M1_TransRunSpinFreewheel();
//    }
}

/*!
 * @brief Free-wheel process called in slow state machine as Run sub state
 *
 * @param void  No input parameter
 *
 * @return None
 */
static void M1_StateRunFreewheelSlow(int motorNum)
{
#ifdef TEST_DEBUG
	if(g_bSwitchedStates)
	{
		PRINTF("M1_StateRunFreewheelSlow\r\n");
		g_bSwitchedStates = 0;
	}
#endif
    /* wait until free-wheel time passes */
    if (--g_sM1Drive[motorNum].ui16CounterState == 0)
    {
        /* switch to sub state READY */
        M1_TransRunFreewheelReady(motorNum);

    }
}

/*!
 * @brief Transition from Calib to Ready state
 *
 * @param void  No input parameter
 *
 * @return None
 */
static void M1_TransRunCalibReady(int motorNum)
{
#ifdef TEST_DEBUG
	g_bSwitchedStates = 1;
	PRINTF("M1_TransRunCalibReady\r\n");
#endif
    /* switch to sub state READY */
    s_eM1StateRun[motorNum] = kRunState_Ready;
}

/*!
 * @brief Transition from Ready to Align state
 *
 * @param void  No input parameter
 *
 * @return None
 */
static void M1_TransRunReadyAlign(int motorNum)
{
#ifdef TEST_DEBUG
	g_bSwitchedStates = 1;
	PRINTF("M1_TransRunReadyAlign\r\n");
#endif
    /* request alignment vector and zero duty cycle */
    g_sM1Drive[motorNum].sCtrlBLDC.f16DutyCycle = FRAC16(0.0);

    /* update PWM duty cycle and set sector */
    M1_MCDRV_PWM3PH_SET_DUTY(&g_sM1Pwm3ph[motorNum], g_sM1Drive[motorNum].sCtrlBLDC.f16DutyCycle);

    M1_MCDRV_PWM3PH_SET_PWM_OUTPUT(&g_sM1Pwm3ph[motorNum], 6);

    /* init alignment period */
    g_sM1Drive[motorNum].ui16CounterState = g_sM1Drive[motorNum].ui16TimeAlignment;

    /* initialize align current controller */
    g_sM1Drive[motorNum].sCtrlBLDC.sIDcBusPiParams.f16InErrK_1 = FRAC16(0.0);
    g_sM1Drive[motorNum].sCtrlBLDC.sIDcBusPiParams.f32IAccK_1 = MLIB_Conv_F32s(g_sM1Drive[motorNum].sCtrlBLDC.f16DutyCycle);
    g_sM1Drive[motorNum].sCtrlBLDC.sIDcBusPiParams.f16UpperLim = M1_CURRENT_CONTROLLER_ALIGN_LIM_HIGH;
    g_sM1Drive[motorNum].sCtrlBLDC.sIDcBusPiParams.f16LowerLim = M1_CURRENT_CONTROLLER_ALIGN_LIM_LOW;

    /* Sub-state RUN ALIGN */
    s_eM1StateRun[motorNum] = kRunState_Align;
}

/*!
 * @brief Transition from Align to Startup state
 *
 * @param void  No input parameter
 *
 * @return None
 */
static void M1_TransRunAlignStartup(int motorNum)
{
#ifdef TEST_DEBUG
	g_bSwitchedStates = 1;
	PRINTF("M1_TransRunAlignStartup\r\n");
#endif
    /* alignment is done, proceed to startup */
    /* initialize startup commutation period counter */
    g_sM1Drive[motorNum].ui16CounterStartCmt = g_sM1Drive[motorNum].ui16StartCmtNumber;

    /* initialize next commutation period time */
    g_sM1Drive[motorNum].ui16PeriodCmtNext = g_sM1Drive[motorNum].ui16PeriodCmtNextInit;
    g_sM1Drive[motorNum].ui16PeriodToff = MLIB_Mul_F16(g_sM1Drive[motorNum].ui16PeriodCmtNext, FRAC16(0.3));

    /* request next time event */
    g_sM1Drive[motorNum].ui16TimeNextEvent = MLIB_Add_F16(g_sM1Drive[motorNum].ui16TimeCurrent, g_sM1Drive[motorNum].ui16PeriodCmtNext);

    M1_MCDRV_TMR_CMT_SET(&g_sM1CmtTmr[motorNum], g_sM1Drive[motorNum].ui16TimeNextEvent);

    /* select next sector based on required spin direction */
    if (g_sM1Drive[motorNum].sCtrlBLDC.ui16MotorDir == 0)
        g_sM1Drive[motorNum].sCtrlBLDC.i16SectorCmt = 2;
    else
        g_sM1Drive[motorNum].sCtrlBLDC.i16SectorCmt = 5;

    /* initialize zero cross current and history states */
    sBEMFPrev[motorNum] = kZXState_Init;
    sBEMFCurr[motorNum] = kZXState_Init;


    /* request sector change */
    /* update PWM duty cycle and set sector */
    g_sM1Drive[motorNum].sCtrlBLDC.f16DutyCycle = M1_START_DUTY;
    M1_MCDRV_PWM3PH_SET_DUTY(&g_sM1Pwm3ph[motorNum], g_sM1Drive[motorNum].sCtrlBLDC.f16DutyCycle);
    M1_MCDRV_PWM3PH_SET_PWM_OUTPUT(&g_sM1Pwm3ph[motorNum], g_sM1Drive[motorNum].sCtrlBLDC.i16SectorCmt);

    /* Go to sub-state RUN STARTUP */
    s_eM1StateRun[motorNum] = kRunState_Startup;
}

/*!
 * @brief Transition from Align to Ready state
 *
 * @param void  No input parameter
 *
 * @return None
 */
static void M1_TransRunAlignReady(int motorNum)
{
#ifdef TEST_DEBUG
	g_bSwitchedStates = 1;
	PRINTF("M1_TransRunAlignReady\r\n");
#endif

    /* Go to sub-state RUN READY */
    s_eM1StateRun[motorNum] = kRunState_Ready;
}

/*!
 * @brief Transition from Startup to Spin state
 *
 * @param void  No input parameter
 *
 * @return None
 */
static void M1_TransRunStartupSpin(int motorNum)
{
#ifdef TEST_DEBUG
	g_bSwitchedStates = 1;
	PRINTF("M1_TransRunStartupSpin\r\n");
#endif
    /* startup is done, initialize startup */
    /* calculate Toff period */
//    g_sM1Drive[motorNum].ui16PeriodToff = MLIB_Mul_F16(g_sM1Drive[motorNum].ui16PeriodCmtNext, g_sM1Drive[motorNum].ui16PeriodToffInit);

    /* initialize commutation period buffer */
    g_sM1Drive[motorNum].ui16PeriodCmt[0] = g_sM1Drive[motorNum].ui16PeriodCmtNext;
    g_sM1Drive[motorNum].ui16PeriodCmt[1] = g_sM1Drive[motorNum].ui16PeriodCmtNext;
    g_sM1Drive[motorNum].ui16PeriodCmt[2] = g_sM1Drive[motorNum].ui16PeriodCmtNext;
    g_sM1Drive[motorNum].ui16PeriodCmt[3] = g_sM1Drive[motorNum].ui16PeriodCmtNext;
    g_sM1Drive[motorNum].ui16PeriodCmt[4] = g_sM1Drive[motorNum].ui16PeriodCmtNext;
    g_sM1Drive[motorNum].ui16PeriodCmt[5] = g_sM1Drive[motorNum].ui16PeriodCmtNext;

    /* clear BEMF integrator */
//    g_sM1Drive[motorNum].f32UBemfIntegSum = 0;

    /* DC-bus current PI controller initialization */
    g_sM1Drive[motorNum].sCtrlBLDC.sIDcBusPiParams.f16UpperLim = g_sM1Drive[motorNum].sCtrlBLDC.sSpeedPiParams.f16UpperLim;
    g_sM1Drive[motorNum].sCtrlBLDC.sIDcBusPiParams.f16LowerLim = g_sM1Drive[motorNum].sCtrlBLDC.sSpeedPiParams.f16LowerLim;
    g_sM1Drive[motorNum].sCtrlBLDC.sIDcBusPiParams.f16InErrK_1 = FRAC16(0.0);

    /* speed PI controller initialization */
    g_sM1Drive[motorNum].sCtrlBLDC.sSpeedPiParams.f16InErrK_1 = FRAC16(0.0);
    g_sM1Drive[motorNum].sCtrlBLDC.sSpeedPiParams.f32IAccK_1 = MLIB_Conv_F32s(g_sM1Drive[motorNum].sCtrlBLDC.f16DutyCycle);

    /* reset commutation error counter */
    g_sM1Drive[motorNum].ui16CounterCmtError = 0;

    /* initialize speed ramp based on spin direction */
    if (g_sM1Drive[motorNum].sCtrlBLDC.ui16MotorDir == 0)
    {
        /* forward */
        g_sM1Drive[motorNum].sCtrlBLDC.sSpeedRampParams.f32State = MLIB_Conv_F32s(M1_N_START_TRH);
    }
    else
    {
        /* backward */
        g_sM1Drive[motorNum].sCtrlBLDC.sSpeedRampParams.f32State = MLIB_Conv_F32s(-M1_N_START_TRH);
    }

    /* this is last startup commutation, safety commutation will be
       performed in close-loop (if not commutated in sensorless mode) */
    g_sM1Drive[motorNum].ui16TimeNextEvent = MLIB_Add_F16(g_sM1Drive[motorNum].ui16TimeCurrentEvent, (g_sM1Drive[motorNum].ui16PeriodCmtNext << 1));
    g_sM1Drive[motorNum].ui16TimeOfCmt = g_sM1Drive[motorNum].ui16TimeCurrent;
	M1_MCDRV_TMR_CMT_SET(&g_sM1CmtTmr[motorNum], g_sM1Drive[motorNum].ui16TimeNextEvent);

//    g_sM1Drive[motorNum].f32UBemfIntegSum = 0;

    /* Go to sub-state RUN SPIN */

    s_eM1StateRun[motorNum] = kRunState_Spin;
}

/*!
 * @brief Transition from Spin to Free-wheel state
 *
 * @param void  No input parameter
 *
 * @return None
 */
static void M1_TransRunSpinFreewheel(int motorNum)
{
#ifdef TEST_DEBUG
	g_bSwitchedStates = 1;
	PRINTF("M1_TransRunSpinFreewheel\r\n");
#endif
    /* set long free-wheel period - expected motor spinning */
    g_sM1Drive[motorNum].ui16CounterState = g_sM1Drive[motorNum].ui16PeriodFreewheelLong;

    /* enter FREEWHEEL sub-state */
    s_eM1StateRun[motorNum] = kRunState_Freewheel;
}

/*!
 * @brief Transition from Startup to Free-wheel state
 *
 * @param void  No input parameter
 *
 * @return None
 */
static void M1_TransRunStartupFreewheel(int motorNum)
{
#ifdef TEST_DEBUG
	g_bSwitchedStates = 1;
	PRINTF("M1_TransRunStartupFreewheel\r\n");
#endif
    /* required speed is below minimum - go to free-wheel */
    /* set short free-wheel period */
    g_sM1Drive[motorNum].ui16CounterState = g_sM1Drive[motorNum].ui16PeriodFreewheelShort;

    /* unstable or very low speed - PWM output disable request */
    M1_MCDRV_PWM3PH_SET_PWM_OUTPUT(&g_sM1Pwm3ph[motorNum], 7);

    /* enter FREEWHEEL sub-state */
    s_eM1StateRun[motorNum] = kRunState_Freewheel;
}

/*!
 * @brief Transition from Free-wheel to Ready state
 *
 * @param void  No input parameter
 *
 * @return None
 */
static void M1_TransRunFreewheelReady(int motorNum)
{
#ifdef TEST_DEBUG
	g_bSwitchedStates = 1;
	PRINTF("M1_TransRunFreewheelReady\r\n");
#endif
    /* Type the code to do when going from the RUN FREEWHEEL to the RUN READY sub-state */
    if (MLIB_AbsSat_F16(g_sM1Drive[motorNum].sCtrlBLDC.f16SpeedCmd) < g_sM1Drive[motorNum].sCtrlBLDC.f16SpeedMinimal)
        g_sM1Drive[motorNum].sCtrlBLDC.f16SpeedCmd = 0;

    /* PWM output disable request */
    M1_MCDRV_PWM3PH_SET_PWM_OUTPUT(&g_sM1Pwm3ph[motorNum], 7);

    /* reinitialize PI controllers and duty cycle */
    GFLIB_CtrlPIpAWInit_F16(FRAC16(0.0), &g_sM1Drive[motorNum].sCtrlBLDC.sIDcBusPiParams);
    g_sM1Drive[motorNum].sCtrlBLDC.f16IDcBusPiOutput = FRAC16(0.0);
    GFLIB_CtrlPIpAWInit_F16(FRAC16(0.0), &g_sM1Drive[motorNum].sCtrlBLDC.sSpeedPiParams);
    g_sM1Drive[motorNum].sCtrlBLDC.f16SpeedPiOutput = FRAC16(0.0);
    g_sM1Drive[motorNum].sCtrlBLDC.f16DutyCycle = FRAC16(0.0);

    /* Sub-state RUN READY */
    s_eM1StateRun[motorNum] = kRunState_Ready;
}

/*!
 * @brief Forced commutation if regular commutation not detected using BEMF method
 *
 * @param void  No input parameter
 *
 * @return None
 */

void M1_TimeEvent(int motorNum)
{
    /* read timer counter and value registers */
    M1_MCDRV_TMR_CMT_GET(&g_sM1CmtTmr[motorNum]);

    /* take action based on current RUN sub-state */
    switch (s_eM1StateRun[motorNum])
    {
        /* RUN sub-state STARTUP is active  */
        case kRunState_Startup:
        	// If zero-crossing failed to detect, restart open-loop
			if(g_sM1Drive[motorNum].ui16CounterStartCmt == 0)
			{
				M1_MCDRV_PWM3PH_SET_PWM_OUTPUT(&g_sM1Pwm3ph[motorNum], 7);
                /* set long free-wheel time period */
                g_sM1Drive[motorNum].ui16CounterState = g_sM1Drive[motorNum].ui16PeriodFreewheelLong;

                /* clear required speed */
                g_sM1Drive[motorNum].sCtrlBLDC.f16SpeedCmd = 0;

                /* go to FREEWHEEL state */
                M1_TransRunSpinFreewheel(motorNum);
//				done = true;
				return;
			}

			/* perform commutation */
			MCS_BLDCCommutation(&g_sM1Drive[motorNum].sCtrlBLDC);
			M1_MCDRV_PWM3PH_SET_PWM_OUTPUT(&g_sM1Pwm3ph[motorNum], g_sM1Drive[motorNum].sCtrlBLDC.i16SectorCmt);

            /* save commutation time and periods */
            g_sM1Drive[motorNum].ui16TimeOfCmtOld = g_sM1Drive[motorNum].ui16TimeOfCmt;
            g_sM1Drive[motorNum].ui16TimeOfCmt = g_sM1Drive[motorNum].ui16TimeCurrent;

            if(!g_sM1Drive[motorNum].bCommutatedSnsless)
            {
				/* set next startup commutation period */
				g_sM1Drive[motorNum].ui16PeriodCmtNext =
					MLIB_Mul_F16(g_sM1Drive[motorNum].ui16PeriodCmtNext, g_sM1Drive[motorNum].f16StartCmtAcceleration);

				g_sM1Drive[motorNum].ui16TimeNextEvent = MLIB_Add_F16(g_sM1Drive[motorNum].ui16TimeCurrentEvent, g_sM1Drive[motorNum].ui16PeriodCmtNext);

				/* request next time event (commutation ISR) */
				M1_MCDRV_TMR_CMT_SET(&g_sM1CmtTmr[motorNum], g_sM1Drive[motorNum].ui16TimeNextEvent);

            }
            else
            {
        		ui16_NumCmt[motorNum]--;
            }
			/* decrement startup commutation timer */
			g_sM1Drive[motorNum].ui16CounterStartCmt--;

            break;

        /* RUN sub-state SPIN is active  */
        case kRunState_Spin:
            /* check whether commutation happened in ADC ISR just before this ISR */
            if (g_sM1Drive[motorNum].bCommutatedSnsless)
            {
                /* calculate next commutation time and period */
                g_sM1Drive[motorNum].ui16TimeOfCmtOld = g_sM1Drive[motorNum].ui16TimeOfCmt;
                g_sM1Drive[motorNum].ui16TimeOfCmt = g_sM1Drive[motorNum].ui16TimeCurrent;
                g_sM1Drive[motorNum].ui16PeriodCmt[g_sM1Drive[motorNum].sCtrlBLDC.i16SectorCmt] = MLIB_Sub_F16(g_sM1Drive[motorNum].ui16TimeOfCmt, g_sM1Drive[motorNum].ui16TimeOfCmtOld);

                /* If commutation period exceeds max allowed time, go into freewheeling period */
                if(g_sM1Drive[motorNum].ui16PeriodCmt[g_sM1Drive[motorNum].sCtrlBLDC.i16SectorCmt] > M1_CMT_T_MAX)
                {
                	if(ui16_NumCmt[motorNum] > 9980)
                	{
                		/* If motor stalls right after beginning closed-loop application, try again */

        				M1_MCDRV_PWM3PH_SET_PWM_OUTPUT(&g_sM1Pwm3ph[motorNum], 7);
                        /* set long free-wheel time period */
                        g_sM1Drive[motorNum].ui16CounterState = g_sM1Drive[motorNum].ui16PeriodFreewheelLong;

                        /* clear required speed */
                        g_sM1Drive[motorNum].sCtrlBLDC.f16SpeedCmd = 0;

                        /* go to FREEWHEEL state */
                        M1_TransRunSpinFreewheel(motorNum);

        				return;
                	}
    				else
    				{
    					/* If commutation period exceeds max allowed time well after closed-loop begins, stop application */
        				M1_TransRunStop(0);
        				done = true;
        				return;
    				}
                }

    			MCS_BLDCCommutation(&g_sM1Drive[motorNum].sCtrlBLDC);
    			M1_MCDRV_PWM3PH_SET_PWM_OUTPUT(&g_sM1Pwm3ph[motorNum], g_sM1Drive[motorNum].sCtrlBLDC.i16SectorCmt);

                /* calculate Toff period */
    			if (g_sM1Drive[motorNum].sCtrlBLDC.f16SpeedMeasured < FRAC16(-0.52))
    			{
    				g_sM1Drive[motorNum].ui16PeriodToff = MLIB_Mul_F16(g_sM1Drive[motorNum].ui16PeriodCmt[g_sM1Drive[motorNum].sCtrlBLDC.i16SectorCmt], FRAC16(0.07));
    			}
    			else if (g_sM1Drive[motorNum].sCtrlBLDC.f16SpeedMeasured < FRAC16(-0.44444))
    			{
    				g_sM1Drive[motorNum].ui16PeriodToff = MLIB_Mul_F16(g_sM1Drive[motorNum].ui16PeriodCmt[g_sM1Drive[motorNum].sCtrlBLDC.i16SectorCmt], FRAC16(0.12));
    			}
    			else
    			{
    				g_sM1Drive[motorNum].ui16PeriodToff = MLIB_Mul_F16(g_sM1Drive[motorNum].ui16PeriodCmt[g_sM1Drive[motorNum].sCtrlBLDC.i16SectorCmt], FRAC16(0.3));
    			}

    			/* number of commutations counter */
            	ui16_NumCmt[motorNum]--;
                return;
            }
            else
            {
            	/* debug; skip rest of function */
				return;
            }


            /* check commutation stability (number of commutation errors) */
            if (g_sM1Drive[motorNum].ui16CounterCmtError > (M1_MAX_CMT_ERRORS * 3))
            {
                /* unstable or very low speed - PWM output disable request */
                M1_MCDRV_PWM3PH_SET_PWM_OUTPUT(&g_sM1Pwm3ph[motorNum], 7);

                /* set long free-wheel time period */
                g_sM1Drive[motorNum].ui16CounterState = g_sM1Drive[motorNum].ui16PeriodFreewheelLong;

                /* clear required speed */
                g_sM1Drive[motorNum].sCtrlBLDC.f16SpeedCmd = 0;

                /* go to FREEWHEEL state */
                M1_TransRunSpinFreewheel(motorNum);
                return;
            }
            else
            {
                /* increment error counter */
                g_sM1Drive[motorNum].ui16CounterCmtError += 3;
            }

            /* calculate next commutation time and period */
            g_sM1Drive[motorNum].ui16TimeOfCmtOld = g_sM1Drive[motorNum].ui16TimeOfCmt;
            g_sM1Drive[motorNum].ui16TimeOfCmt = g_sM1Drive[motorNum].ui16TimeCurrent;
            g_sM1Drive[motorNum].ui16PeriodCmtNext = MLIB_Sub_F16(g_sM1Drive[motorNum].ui16TimeOfCmt, g_sM1Drive[motorNum].ui16TimeOfCmtOld);
            g_sM1Drive[motorNum].ui16PeriodCmt[g_sM1Drive[motorNum].sCtrlBLDC.i16SectorCmt] = g_sM1Drive[motorNum].ui16PeriodCmtNext;

            /* perform commutation */
            MCS_BLDCCommutation(&g_sM1Drive[motorNum].sCtrlBLDC);
            /* request sector change */
            M1_MCDRV_PWM3PH_SET_PWM_OUTPUT(&g_sM1Pwm3ph[motorNum], g_sM1Drive[motorNum].sCtrlBLDC.i16SectorCmt);
            /* clear BEMF integrator */
            g_sM1Drive[motorNum].f32UBemfIntegSum = 0;

            /* calculate Toff period */
            g_sM1Drive[motorNum].ui16PeriodToff = MLIB_Mul_F16(g_sM1Drive[motorNum].ui16PeriodCmtNext, g_sM1Drive[motorNum].ui16PeriodToffInit);

            /* request next time event to double of last commutation period */
            g_sM1Drive[motorNum].ui16PeriodCmtNext = MLIB_Sh1LSat_F16(g_sM1Drive[motorNum].ui16PeriodCmtNext);

            /* calculate next time of forced commutation */
            g_sM1Drive[motorNum].ui16TimeNextEvent = MLIB_Add_F16(g_sM1Drive[motorNum].ui16TimeCurrent, g_sM1Drive[motorNum].ui16PeriodCmtNext);
            /* request next time event (commutation ISR) */
            M1_MCDRV_TMR_CMT_SET(&g_sM1CmtTmr[motorNum], g_sM1Drive[motorNum].ui16TimeNextEvent);
            break;

        default:
            break;
    }
}

/*!
 * @brief Fault detention routine - check various faults
 *
 * @param void  No input parameter
 *
 * @return None
 */
void M1_FaultDetection(int motorNum)
{
    /* Clearing actual faults before detecting them again  */
    /* Clear all faults */
    MC_FAULT_CLEAR_ALL(g_sM1Drive[motorNum].sFaultIdPending);

    /* Fault:   DC-bus over-current */
    if (g_sM1Drive[motorNum].sCtrlBLDC.f16IDcBus > g_sM1Drive[motorNum].sFaultThresholds.f16IDcBusOver)
        MC_FAULT_SET(g_sM1Drive[motorNum].sFaultIdPending, MC_FAULT_I_DCBUS_OVER);

    /* Fault:   DC-bus over-voltage */
    if (g_sM1Drive[motorNum].sCtrlBLDC.f16UDcBus > g_sM1Drive[motorNum].sFaultThresholds.f16UDcBusOver)
        MC_FAULT_SET(g_sM1Drive[motorNum].sFaultIdPending, MC_FAULT_U_DCBUS_OVER);

    /* Fault:   DC-bus under-voltage */
    if (g_sM1Drive[motorNum].sCtrlBLDC.f16UDcBus < g_sM1Drive[motorNum].sFaultThresholds.f16UDcBusUnder)
        MC_FAULT_SET(g_sM1Drive[motorNum].sFaultIdPending, MC_FAULT_U_DCBUS_UNDER);

    /* pass fault to Fault ID */
    g_sM1Drive[motorNum].sFaultId |= g_sM1Drive[motorNum].sFaultIdPending;
}

/*******************************************************************************
 * Debug Functions
 ******************************************************************************/
/*!
 * @brief Checks if state machine is transitioning to a new state.
 *
 * @param bValue  bool value, true - Yes of false - No
 *
 * @return None
 */
void M1_DidStateChange(bool_t bValue)
{
	g_bSwitchedStates = bValue;
}

/*!
 * @brief Manually stops application if done is true
 *
 * @param bValue  bool value, true - Stop of false - Keep running
 *
 * @return None
 */
bool_t M1_StopApplication(void)
{
	return done;
}


/*!
 * @brief Manually stops application if done is true
 *
 * @param bValue  bool value, true - Stop of false - Keep running
 *
 * @return None
 */
void M1_SetStopApplication(bool stop, int motorNum)
{
	if(stop)
	{
		M1_TransInitStop(motorNum);
		done = true;
		s_eM1StateRun[motorNum] = 0;
	}
	return;
}

/*!
 * @brief Print ADC history of BEMF and Bus Voltage Reference
 *
 * @param None
 *
 * @return None
 */

void M1_PrintADCHistory(void)
{
	uint16_t index;

	PRINTF("data = [");

	for(index=0; index < MAX_HISTORY_NUM; index++)
	{
		PRINTF("[%d, %d, %d, %d, %d]", ui16_TimeHistory[index], ui16_SectorHistory[index], ui16_BEMFHistory0[index], ui16_UDCBHistory0[index], ui16_CurrentHistory[index]);

		if(index < (MAX_HISTORY_NUM-1))
		{
			PRINTF(",\r\n");
		}
	}
	PRINTF("];");

	return;
}

/*******************************************************************************
 * API
 ******************************************************************************/

/*!
 * @brief Set application switch value to On or Off mode
 *
 * @param bValue  bool value, true - On of false - Off
 *
 * @return None
 */
void M1_SetAppSwitch(bool_t bValue)
{
    g_bM1SwitchAppOnOff = bValue;
}

/*!
 * @brief Get application switch value
 *
 * @param void  No input parameter
 *
 * @return bool_t Return bool value, true or false
 */
bool_t M1_GetAppSwitch(void)
{
    return (g_bM1SwitchAppOnOff);
}
/*!
 * @brief Get application state
 *
 * @param void  No input parameter
 *
 * @return uint16_t Return current application state
 */
uint16_t M1_GetAppState(int motorNum)
{
    return ((uint16_t)g_sM1Ctrl[motorNum].eState);
}

/*!
 * @brief Set spin speed of the motor in fractional value
 *
 * @param f16SpeedCmd  Speed command - set speed
 *
 * @return None
 */
void M1_SetSpeed(frac16_t f16SpeedCmd, int motorNum)
{
    if (g_bM1SwitchAppOnOff)
    {
        /* Set speed */
        if (MLIB_Abs_F16(f16SpeedCmd) < g_sM1Drive[motorNum].sCtrlBLDC.f16SpeedMinimal)
        {
            g_sM1Drive[motorNum].sCtrlBLDC.f16SpeedCmd = 0;
        }
        else if (MLIB_Abs_F16(f16SpeedCmd) > M1_N_NOM)
        {
            g_sM1Drive[motorNum].sCtrlBLDC.f16SpeedCmd = 0;
        }
        else
        {
            g_sM1Drive[motorNum].sCtrlBLDC.f16SpeedCmd = f16SpeedCmd;
        }
    }
    else
    {
        /* Set zero speed */
        g_sM1Drive[motorNum].sCtrlBLDC.f16SpeedCmd = 0;
    }
}

/*!
 * @brief Get spin speed of the motor in fractional value
 *
 * @param void  No input parameter
 *
 * @return frac16_t Fractional value of the current speed
 */
frac16_t M1_GetSpeed(int motorNum)
{
    /* return speed */
    return g_sM1Drive[motorNum].sCtrlBLDC.f16SpeedCmd;
}

