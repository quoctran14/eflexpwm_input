/*
 * Copyright (c) 2016, Freescale Semiconductor, Inc.
 * Copyright 2016-2017 NXP
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef _M1_STATEMACHINE_H_
#define _M1_STATEMACHINE_H_

#include "m1_bldc_appconfig.h"
#include "state_machine.h"
#include "bldc_control.h"
#include "fsl_debug_console.h"

/* library headers */
#include "gmclib.h"
#include "gflib.h"
#include "gdflib.h"
#include "amclib.h"

/*******************************************************************************
 * Definitions
 ******************************************************************************/
#define MC_FAULT_I_DCBUS_OVER 0  /* OverCurrent fault flag */
#define MC_FAULT_U_DCBUS_UNDER 1 /* Undervoltage fault flag */
#define MC_FAULT_U_DCBUS_OVER 2  /* Overvoltage fault flag */

/* VGS Faults (Address = 0x4) */
#define VGS_HA 1<<10
#define VGS_LA 1<<9
#define VGS_HB 1<<8
#define VGS_LB 1<<7
#define VGS_HC 1<<6
#define VGS_LC 1<<5

/* IC Faults Register (ADDRESS = 0x3) */
#define PVDD_UVLO2_FAULT 1<<10
#define WD_FAULT 1<<9
#define OTSD_FAULT 1<<8
#define VREG_UV_FAULT 1<<6
#define AVDD_UVLO_FAULT 1<<5
#define VCP_LSD_UVLO2_FAULT 1<<4
#define VCPH_UVLO2_FAULT 1<<2
#define VCPH_OVLO_FAULT 1<<1
#define VCPH_OVLO_ABS_FAULT 1

/* OV/VDS Faults (ADDRESS = 0x2) */
#define VDS_HA 1<<10
#define VDS_LA 1<<9
#define VDS_HB 1<<8
#define VDS_LB 1<<7
#define VDS_HC 1<<6
#define VDS_LC 1<<5
#define SNS_C_OCP 1<<2 /* Sense C Overcurrent Fault */
#define SNS_B_OCP 1<<1/* Sense B Overcurrent Fault */
#define SNS_A_OCP 1/* Sense A Overcurrent Fault */

/* Warning and Watchdog Reset Register (ADDRESS=0x01)*/
#define FAULT_TRIGGER 1<<10 /* Fault indication */
#define TEMP_FLAG4 1<<8 /* Temp flag setting for approx 175 degree C */
#define PVDD_UVFL 1<<7 /* PVDD undervoltage flag warning */
#define PVDD_OVFL 1<<6 /* PVDD overvoltage flag warning */
#define VDS_STATUS 1<<5 /* Real time OR of all VDS overcurrent monitors */
#define VCHP_UVFL 1<<4 /* Charge pump undervoltage flag warning */
#define TEMP_FLAG1 1<<3 /* Temperature flags etting for approx 105 degree C */
#define TEMP_FLAG2 1<<2 /* Temperature flags etting for approx 125 degree C */
#define TEMP_FLAG3 1<<1 /* Temperature flags etting for approx 135 degree C */
#define OTW 1 /* Overtemperature warning */


#define CLEAR_FAULTS 1<<1

#define CLR_BIT(reg, nbit) (reg &= ~(nbit))
#define SET_BIT(reg, nbit) (reg |= nbit)

/* Sets the fault bit defined by faultid in the faults variable */
#define MC_FAULT_SET(faults, faultid) (faults |= ((mcdef_fault_t)1 << faultid))

/* Clears the fault bit defined by faultid in the faults variable */
#define MC_FAULT_CLEAR(faults, faultid) (faults &= ~((mcdef_fault_t)1 << faultid))

/* Check the fault bit defined by faultid in the faults variable, returns 1 or 0 */
#define MC_FAULT_CHECK(faults, faultid) ((faults & ((mcdef_fault_t)1 << faultid)) >> faultid)

/* Clears all fault bits in the faults variable */
#define MC_FAULT_CLEAR_ALL(faults) (faults = 0)

/* Check if a fault bit is set in the faults variable, 0 = no fault */

#define MC_FAULT_ANY(faults) (faults > 0)
/* Update a fault bit defined by faultid in the faults variable according to the LSB of value */
#define MC_FAULT_UPDATE(faults, faultid, value)                     \
    {                                                               \
        MC_FAULT_CLEAR(faults, faultid);                            \
        faults |= (((MC_FAULT_T)value & (MC_FAULT_T)1) << faultid); \
    }

/*! @brief device fault typedef */
typedef uint16_t mcdef_fault_t;

/*! @brief States of machine enumeration */
typedef enum _m1_run_substate_t
{
    kRunState_Calib = 0,
    kRunState_Ready = 1,
    kRunState_Align = 2,
    kRunState_Startup = 3,
    kRunState_Spin = 4,
    kRunState_Freewheel = 5,
	kRunState_Debug = 6,
} m1_run_substate_t; /* Run sub-states */

/*! @brief Device fault thresholds */
typedef struct _mcdef_fault_thresholds_t
{
    frac16_t f16IDcBusOver;  /* DC bus over current level */
    frac16_t f16UDcBusOver;  /* DC bus over voltage level */
    frac16_t f16UDcBusUnder; /* DC bus under voltage level */
} mcdef_fault_thresholds_t;

/*! @brief BLDC sensorless with BEMF integration method */
typedef struct _mcdef_bldc_t
{
    mcs_bldc_ctrl_t sCtrlBLDC;                 /* Main BLDC control structure */
    mcdef_fault_t sFaultId;                    /* Application motor faults */
    mcdef_fault_t sFaultIdPending;             /* Fault pending structure */
    mcdef_fault_thresholds_t sFaultThresholds; /* Fault thresholds */
    uint16_t ui16PeriodCmt[6];                 /* commutation periods */
    uint16_t ui16TimePrev;                  /* current time */
    uint16_t ui16TimeCurrent;                  /* current time */
    uint16_t ui16TimeCurrentEvent;             /* time of current event */
    uint16_t ui16TimeNextEvent;                /* time of next event */
    uint16_t ui16TimeOfCmt;                    /* current commutation time */
    uint16_t ui16TimeOfCmtOld;                 /* previous commutation time */
    uint16_t ui16PeriodCmtNext;                /* next commutation period */
    uint16_t ui16PeriodToff;                   /* Toff period */
    uint16_t ui16CounterStartCmt;              /* startup commutations counter */
    uint16_t ui16CounterCmtError;              /* commutation error counter */
    frac32_t f32UBemfIntegSum;                 /* BEMF integrator */
    frac32_t f32UBemfIntegThreshold;           /* BEMF integration threshold */
    bool_t bCommutatedSnsless;                 /* commutated by sensorless algorithm flag */
    uint16_t ui16PeriodFreewheelLong;          /* long free-wheel period */
    uint16_t ui16PeriodFreewheelShort;         /* short free-wheel period */
    uint16_t ui16TimeAlignment;                /* alignment period */
    uint16_t ui16TimeCalibration;              /* Calibration time count number */
    uint16_t ui16TimeFaultRelease;             /* Fault time count number */
    uint16_t ui16CounterState;                 /* Main state counter */
    bool_t bFaultClearMan;                     /* Manual fault clear detection */
    frac16_t f16StartCmtAcceleration;          /* Startup commutation acceleration init value*/
    uint16_t ui16StartCmtNumber;               /* Startup commutation counter init value */
    uint16_t ui16PeriodCmtNextInit;            /* Next commutation period init value */
    uint16_t ui16PeriodToffInit;               /* Toff period init value */
    uint16_t ui16Aux;                          /* auxiliary quantity measured value */
    uint16_t ui16FreqCtrlLoop;                 /* Pass defined loop frequency to FreeMASTER */
    uint32_t ui32FreqCmtTimer;                 /* Pass defined commutation timer frequency to FreeMASTER */
    uint32_t ui16FreqPwm;                      /* Pass defined PWM frequency to FreeMASTER */
} mcdef_bldc_t;

#define NUM_MOTORS 8
/*******************************************************************************
 * Variables
 ******************************************************************************/
extern bool_t g_bM1SwitchAppOnOff;
extern mcdef_bldc_t g_sM1Drive[NUM_MOTORS];
extern sm_app_ctrl_t g_sM1Ctrl[NUM_MOTORS];
#ifdef __cplusplus
extern "C" {
#endif
/*******************************************************************************
 * Debug Functions
 ******************************************************************************/
#define MAX_HISTORY_NUM	3500
#define DUTY_CYCLE_INCR FRAC16(0.088)
/*
 * 100% duty cycle = FRAC16(-1.0)
 * 0% duty cycle = FRAC16(1.0)
 */
#define DUTY_CYCLE_INIT FRAC16(0.1)
volatile bool_t g_bSwitchedStates;
volatile bool_t done;
volatile uint16_t ui16_BEMFHistory0[MAX_HISTORY_NUM];
volatile uint16_t ui16_UDCBHistory0[MAX_HISTORY_NUM];
volatile uint16_t ui16_SectorHistory[MAX_HISTORY_NUM];
volatile uint16_t ui16_TimeHistory[MAX_HISTORY_NUM];
volatile uint16_t ui16_CurrentHistory[MAX_HISTORY_NUM];
volatile uint16_t history_index = 0;
volatile uint16_t history_array = 0;
volatile bool_t g_bRisingEdge = FALSE;
volatile bool_t g_bFallingEdge = FALSE;
volatile bool_t g_bZeroCross[NUM_MOTORS] =  {false, false, false, false, false, false, false, false};
volatile uint16_t zeroCrossConsecCount = 0;
volatile uint16_t ui16_ZeroCrossNum = 0;
volatile uint16_t ui16_ZeroXTime[NUM_MOTORS] = {0,0,0,0,0,0,0,0};
volatile uint16_t ui16ZeroXTime[NUM_MOTORS] = {0,0,0,0,0,0,0,0};
volatile uint16_t ui16PotentialZeroXTime[NUM_MOTORS] = {0,0,0,0,0,0,0,0};
volatile uint16_t ui16_ZeroXCurr[NUM_MOTORS] = {0,0,0,0,0,0,0,0};
volatile uint16_t ui16_ZeroXPrev[NUM_MOTORS] = {0,0,0,0,0,0,0,0};
volatile uint16_t ui16_ZeroXOverflow[NUM_MOTORS] = {0,0,0,0,0,0,0,0};
volatile uint16_t ui16_NumCmt[NUM_MOTORS] = {10000,10000,10000,10000,10000,10000,10000,10000};
volatile uint16_t ui16_ZeroCrossCnt = 0;
volatile uint16_t ui16_ZeroCrossFilter = 0;
volatile frac16_t f16_DutyCycleInit = DUTY_CYCLE_INIT;
volatile frac16_t f16_DutyCycle = 0;
volatile frac16_t f16_DutyCyclePrev = 0;
volatile bool START_PI = false;

volatile bool potentialZeroXPN[NUM_MOTORS] = {false, false, false, false, false, false, false, false};
volatile bool potentialZeroXNP[NUM_MOTORS] = {false, false, false, false, false, false, false, false};

int32_t capture1Val;
int32_t capture2Val;
int32_t pulseWidth;
float_t floatPulseWidth;
frac16_t f16PulseWidth;
frac16_t f16divisor;
/* Get source clock for FTM driver */
#define FTM_SOURCE_CLOCK CLOCK_GetFreq(kCLOCK_FastPeriphClk)
#define LOWER_LIMIT -1200*1.6666/13300
#define UPPER_LIMIT -0.98

/*! @brief States of zero-cross detection enumeration */
typedef enum _m1_zx_state_t
{
    kZXState_Init = 0,
    kZXState_Low = 1,
    kZXState_High = 2
} m1_zx_state_t; /* Run sub-states */

m1_zx_state_t sBEMFPrev[NUM_MOTORS];
m1_zx_state_t sBEMFCurr[NUM_MOTORS];

void M1_DidStateChange(bool_t g_bSwitchedStates);
bool_t M1_StopApplication(void);
void M1_PrintADCHistory(void);

/*******************************************************************************
 * API
 ******************************************************************************/

/*!
 * @brief Set application switch value to On or Off mode
 *
 * @param bValue  bool value, true - On of false - Off
 *
 * @return None
 */
void M1_SetAppSwitch(bool_t bValue);

/*!
 * @brief Get application switch value
 *
 * @param void  No input parameter
 *
 * @return bool_t Return bool value, true or false
 */
bool_t M1_GetAppSwitch(void);

/*!
 * @brief Get application state
 *
 * @param void  No input parameter
 *
 * @return uint16_t Return current application state
 */
uint16_t M1_GetAppState(int motorNum);

/*!
 * @brief Set spin speed of the motor in fractional value
 *
 * @param f16SpeedCmd  Speed command - set speed
 *
 * @return None
 */
void M1_SetSpeed(frac16_t f16SpeedCmd, int motorNum);

/*!
 * @brief Get spin speed of the motor in fractional value
 *
 * @param void  No input parameter
 *
 * @return frac16_t Fractional value of the current speed
 */
frac16_t M1_GetSpeed(int motorNum);

/*!
 * @brief Forced commutation if regular commutation not detected using BEMF method
 *
 * @param void  No input parameter
 *
 * @return No return value
 */
void M1_TimeEvent(int motorNum);

#ifdef __cplusplus
}
#endif

#endif /* STATEMACHINE */

