/*
 * Copyright (c) 2016, Freescale Semiconductor, Inc.
 * Copyright 2016-2017 NXP
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "mcdrv_spi_driver3ph.h"
#include "m1_sm_ref_sol.h"
/*******************************************************************************
 * Definitions
 ******************************************************************************/
//#define PRINT_DRV_MSGS


static bool_t MCDRV_Driver3PhSendCmd(mcdrv_spi_drv3ph_t *this,
                                     uint16_t *pui16TxData,
                                     uint16_t *pui16RxData);

/*******************************************************************************
* Variables
******************************************************************************/

static bool_t s_statusPass;

/*******************************************************************************
 * Code
 ******************************************************************************/

/*!
 * @brief Initialization function of MC33937 SPI driver
 *
 * @param this   Pointer to the current object
 * @param init   Pointer to initialization structure
 *
 * @return boot_t true on success
 */
bool_t MCDRV_Driver3PhInit(mcdrv_spi_drv3ph_t *this, mcdrv_spi_drv3ph_init_t *init)
{
    s_statusPass = TRUE;

    /* configure GPIO pads for MC33937 signals */
    this->sSpiData.pSpiBase = init->pSpiBase;      /* SPI Base Address */
    this->sSpiData.ui32Pcs = (1 << init->ui32Pcs); /* PCS number */

    /* if enabled, set over-current PIN & PORT - INPUT */
    if (init->bFaultOcEnabled)
    {
        this->sSpiData.pGpioOcBase = init->pGpioOcBase;     /* base Address */
        this->sSpiData.ui32GpioOcPin = init->ui32GpioOcPin; /* pin number for over current */
        /* setup PIN Muxing mode - GPIO */
        /* set passive filter*/
        /* enable pull-down */
        init->pPortOcBase->PCR[init->ui32GpioOcPin] |= (PORT_PCR_MUX(GPIO_MUX) | PORT_PCR_PFE_MASK | PORT_PCR_PE_MASK);
    }

    /* interrupt PIN & PORT - INPUT*/
    this->sSpiData.pGpioIntBase = init->pGpioIntBase;     /* base Address */
    this->sSpiData.ui32GpioIntPin = init->ui32GpioIntPin; /* pin number for interrupt detection */
    /* setup PIN Muxing mode - GPIO */
    /* set passive filter*/
    /* enable pull-down */
    init->pPortIntBase->PCR[init->ui32GpioIntPin] |= (PORT_PCR_MUX(GPIO_MUX) | PORT_PCR_PFE_MASK | PORT_PCR_PE_MASK);

    /* enable PIN & PORT - OUTPUT */
    this->sSpiData.pGpioEnBase = init->pGpioEnBase;     /* base Address */
    this->sSpiData.ui32GpioEnPin = init->ui32GpioEnPin; /* pin number for driver enabled */
    /* setup PIN Muxing mode - GPIO */
    init->pPortEnBase->PCR[init->ui32GpioEnPin] |= (PORT_PCR_MUX(GPIO_MUX));
    /* set GPIO as output */
    init->pGpioEnBase->PDDR |= (1 << init->ui32GpioEnPin);

    /* reset PIN & PORT - OUTPUT */
    this->sSpiData.pGpioResetBase = init->pGpioResetBase;     /* base Address */
    this->sSpiData.ui32GpioResetPin = init->ui32GpioResetPin; /* pin number for driver reset */
    /* setup PIN Muxing mode - GPIO	*/
    init->pPortResetBase->PCR[init->ui32GpioResetPin] |= (PORT_PCR_MUX(GPIO_MUX));
    /* set GPIO as output */
    init->pGpioResetBase->PDDR |= (1 << init->ui32GpioResetPin);

    return (s_statusPass);
}

/*!
 * @brief Function send SPI command to MC33937
 *
 * @param this          Pointer to the current object
 * @param pui8TxData    Pointer to data which be send via SPI
 * @param pui8RxData    Pointer to data which be read via SPI
 *
 * @return boot_t true on success
 */
static bool_t MCDRV_Driver3PhSendCmd(mcdrv_spi_drv3ph_t *this, uint16_t *pui16TxData, uint16_t *pui16RxData)
{
    uint16_t ui16SafetyCounter = 0; /* eliminate code freezing in while loop */
    dspi_command_data_config_t commandData;

    s_statusPass = TRUE;

    /* clear TCF bit */
    SPI0->SR |= (SPI_SR_TCF_MASK);

    commandData.whichPcs = kDSPI_Pcs0;
    commandData.whichCtar = kDSPI_Ctar0;
    commandData.clearTransferCount = false;
    commandData.isEndOfQueue = false;
    commandData.isPcsContinuous = false;

    /* push data */
    DSPI_StartTransfer(SPI0);
    DSPI_MasterWriteData(SPI0, &commandData, (*pui16TxData));

    /* wait till TCF sets */
    while (((SPI0->SR & (SPI_SR_TCF_MASK)) == 0) && (ui16SafetyCounter < 1000000))
    {
        ui16SafetyCounter++;
    }

    /* load value to variable from register */
    *pui16RxData = (uint16_t)DSPI_ReadData(SPI0);

    return (s_statusPass);
}

/*!
 * @brief Function read MC33937 over current pin
 *
 * @param this Pointer to the current object
 *
 * @return boot_t true on success
 */
bool_t MCDRV_Driver3PhReadOc(mcdrv_spi_drv3ph_t *this)
{
    s_statusPass = TRUE;

    /* read over-current pin */
    s_statusPass = ((this->sSpiData.pGpioOcBase->PDIR) & (1 << this->sSpiData.ui32GpioOcPin));

    return (s_statusPass);
}

/*!
 * @brief Function read MC33937 interrupt pin
 *
 * @param this Pointer to the current object
 *
 * @return boot_t true on success
 */
bool_t MCDRV_Driver3PhReadInt(mcdrv_spi_drv3ph_t *this)
{
    s_statusPass = TRUE;

    /* read interrupt pin */
    s_statusPass = ((this->sSpiData.pGpioIntBase->PDIR) & (1 << this->sSpiData.ui32GpioIntPin));

    return (s_statusPass);
}

/*!
 * @brief Function clear MC33937 reset pin
 *
 * @param this Pointer to the current object
 *
 * @return boot_t true on success
 */
bool_t MCDRV_Driver3PhClearRst(mcdrv_spi_drv3ph_t *this)
{
    s_statusPass = TRUE;

    /* clear pin output for 3PPA driver reset */
    (this->sSpiData.pGpioResetBase->PCOR) |= (1 << this->sSpiData.ui32GpioResetPin);

    return (s_statusPass);
}

/*!
 * @brief Function set MC33937 reset pin
 *
 * @param this Pointer to the current object
 *
 * @return boot_t true on success
 */
bool_t MCDRV_Driver3PhSetRst(mcdrv_spi_drv3ph_t *this)
{
    s_statusPass = TRUE;

    /* set pin output for 3PPA driver reset */
    (this->sSpiData.pGpioResetBase->PSOR) |= (1 << this->sSpiData.ui32GpioResetPin);

    return (s_statusPass);
}

/*!
 * @brief Function clear MC33937 enable pin
 *
 * @param this Pointer to the current object
 *
 * @return boot_t true on success
 */
bool_t MCDRV_Driver3PhClearEn(mcdrv_spi_drv3ph_t *this)
{
    s_statusPass = TRUE;

    (this->sSpiData.pGpioEnBase->PCOR) |= (1 << this->sSpiData.ui32GpioEnPin);

    return (s_statusPass);
}

/*!
 * @brief Function set MC33937 enable pin
 *
 * @param this Pointer to the current object
 *
 * @return boot_t true on success
 */
bool_t MCDRV_Driver3PhSetEn(mcdrv_spi_drv3ph_t *this)
{
    s_statusPass = TRUE;

    (this->sSpiData.pGpioEnBase->PSOR) |= (1 << this->sSpiData.ui32GpioEnPin);

    return (s_statusPass);
}

/*!
 * @brief Function set MC33937 deadtime
 *
 * @param this Pointer to the current object
 *
 * @return boot_t true on success
 */
bool_t MCDRV_Driver3PhSetDeadtime(mcdrv_spi_drv3ph_t *this)
{
    uint8_t ui8TxData, ui8RxData;

    s_statusPass = TRUE;

    /* zero deadtime calibration */
    /* send ZERODEADTIME command for dead time configuration with zero
       calibration */
    ui8TxData = 0x80;
//    s_statusPass &= MCDRV_Driver3PhSendCmd(this, &ui8TxData, &ui8RxData);

    return (s_statusPass);
}

/*!
 * @brief Function clear MC33937 flag register
 *
 * @param this Pointer to the current object
 *
 * @return boot_t true on success
 */
bool_t MCDRV_Driver3PhClearFlags(mcdrv_spi_drv3ph_t *this)
{
    uint8_t ui8TxData, ui8RxData;

    s_statusPass = TRUE;

    /* CLINT0_COMMAND = 0x6F */
    ui8TxData = 0x6F;
//    s_statusPass &= MCDRV_Driver3PhSendCmd(this, &ui8TxData, &ui8RxData);

    /* CLINT1_COMMAND = 0x7F */
    ui8TxData = 0x7F;
//    s_statusPass &= MCDRV_Driver3PhSendCmd(this, &ui8TxData, &ui8RxData);

    return (s_statusPass);
}

/*!
 * @brief Function read MC33937 status register 0
 *
 * @param this Pointer to the current object
 *
 * @return boot_t true on success
 */
bool_t MCDRV_Driver3PhGetSr0(mcdrv_spi_drv3ph_t *this)
{
    uint8_t ui8TxData;

    s_statusPass = TRUE;

    /* status Register 0 reading = 0x00 */
    ui8TxData = 0x00;
//    s_statusPass &= MCDRV_Driver3PhSendCmd(this, &ui8TxData, &this->sSr0.R);

    /* status Register 0 write to read SR0 result */
    ui8TxData = 0x00;
//    s_statusPass &= MCDRV_Driver3PhSendCmd(this, &ui8TxData, &this->sSr0.R);

    return (s_statusPass);
}

/*!
 * @brief Function read MC33937 status register 1
 *
 * @param this Pointer to the current object
 *
 * @return boot_t true on success
 */
bool_t MCDRV_Driver3PhGetSr1(mcdrv_spi_drv3ph_t *this)
{
    uint8_t ui8TxData;

    s_statusPass = TRUE;

    /* status Register 1 reading = 0x01 */
    ui8TxData = 0x01;
//    s_statusPass &= MCDRV_Driver3PhSendCmd(this, &ui8TxData, &this->sSr0.R);

    /* status Register 0 write to read SR1 result */
    ui8TxData = 0x00;
//    s_statusPass &= MCDRV_Driver3PhSendCmd(this, &ui8TxData, &this->sSr1.R);

    return (s_statusPass);
}

/*!
 * @brief Function read MC33937 status register 2
 *
 * @param this          Pointer to the current object
 *
 * @return boot_t true on success
 */
bool_t MCDRV_Driver3PhGetSr2(mcdrv_spi_drv3ph_t *this)
{
    uint8_t ui8TxData;

    s_statusPass = TRUE;

    /* status Register 2 reading = 0x02 */
    ui8TxData = 0x02;
//    s_statusPass &= MCDRV_Driver3PhSendCmd(this, &ui8TxData, &this->sSr0.R);

    /* status Register 0 write to read SR2 result */
    ui8TxData = 0x00;
//    s_statusPass &= MCDRV_Driver3PhSendCmd(this, &ui8TxData, &this->sSr2.R);

    return (s_statusPass);
}

/*!
 * @brief Function read MC33937 status register 3
 *
 * @param this Pointer to the current object
 *
 * @return boot_t true on success
 */
bool_t MCDRV_Driver3PhGetSr3(mcdrv_spi_drv3ph_t *this)
{
    uint8_t ui8TxData;

    s_statusPass = TRUE;

    /* status Register 3 reading = 0x03 */
    ui8TxData = 0x03;
//    s_statusPass &= MCDRV_Driver3PhSendCmd(this, &ui8TxData, &this->sSr0.R);

    /* status Register 0 write to read SR3 result */
    ui8TxData = 0x00;
//    s_statusPass &= MCDRV_Driver3PhSendCmd(this, &ui8TxData, &this->sSr3);

    return (s_statusPass);
}

/*!
 * @brief Function configure MC33937 pre-driver
 *
 * @param this Pointer to the current object
 *
 * @return boot_t true on success
 */
bool_t MCDRV_Driver3PhConfig(mcdrv_spi_drv3ph_t *this)
{
    uint16_t ui16TxData, ui16RxData;
    register uint32_t ui32loop_cnt;
    bool ls_statusPass = TRUE;
    s_statusPass = FALSE;
    int attempts = 0;

    do
    {
    	ls_statusPass = TRUE;
    /* EN1 and EN2 are still low */
    //s_statusPass &= MCDRV_Driver3PhClearEn(this);

    /* set RST high */
    //s_statusPass &= MCDRV_Driver3PhSetRst(this);

    /* required start-up delay between the RST going up and initialization
       routine at 75 MHz the delay will be 50 ms , at lower CPU clocks will be
       more than 2 ms */
    for (ui32loop_cnt = 0; ui32loop_cnt < 1500000; ui32loop_cnt++)
    {
        __asm("nop");
    }
//#define SPI_DRV_GATE_DISABLE_OVERCURRENT 0xCE20
#define SPI_DRV_GATE_DISABLE_OVERCURRENT 0x4E20
#define READ_FAULT_INDICATION 0x8800
//#define PRINT_DRV_MSGS
    // Configure SPI Gate Overcurrent

    // Read if fault indicated
//    ui16TxData = READ_FAULT_INDICATION;
//    MCDRV_Driver3PhSendCmd(this, &ui16TxData, &ui16RxData);
//    //s_statusPass = ((ui16TxData&0x01FF) == (ui16RxData&0x01FF));
//	#ifdef PRINT_DRV_MSGS
//		printf("\r\nSetting FAULT INDICATION, TX: %x, RX: %x\r\n", (0x3FF&ui16TxData), (0x3FF&ui16RxData));
//	#endif
    // Read IC Fault
//#define IC_FAULT_READ 0xB800
//    ui16TxData = IC_FAULT_READ;
//    MCDRV_Driver3PhSendCmd(this, &ui16TxData, &ui16RxData);
//    //s_statusPass = ((ui16TxData&0x01FF) == (ui16RxData&0x01FF));
//	#ifdef PRINT_DRV_MSGS
//		printf("\r\nSetting READ IC FAULT, TX: %x, RX: %x\r\n", (0x3FF&ui16TxData), (0x3FF&ui16RxData));
//	#endif
		ui16TxData = SPI_DRV_GATE_DISABLE_OVERCURRENT;
//		SET_BIT(ui16TxData, CLEAR_FAULTS);
		MCDRV_Driver3PhSendCmd(this, &ui16TxData, &ui16RxData);
//		ui16TxData = IC_FAULT_READ;
//		MCDRV_Driver3PhSendCmd(this, &ui16TxData, &ui16RxData);
#ifdef PRINT_DRV_MSGS
	printf("\r\nCLEARED FAULT: Setting ERROR, TX: %x, RX: %x\r\n", (0x3FF&ui16TxData), (0x3FF&ui16RxData));
#endif


//#define PRINT_DRV_MSGS
    // Configure SPI Gate Overcurrent
#define HS_GATE_DRIVE_CONTROL 0x2977
    ui16TxData = HS_GATE_DRIVE_CONTROL;
    ls_statusPass &= MCDRV_Driver3PhSendCmd(this, &ui16TxData, &ui16RxData);
    ls_statusPass &= ((ui16TxData&0x01FF) == (ui16RxData&0x01FF));
	#ifdef PRINT_DRV_MSGS
		printf("\r\nSetting HS_GATE_DRIVE_CONTROL, TX: %x, RX: %x\r\n", (0x1FF&ui16TxData), (0x1FF&ui16RxData));
	#endif

    // Configure DRV VDS setting
#define SET_VDS 0x60C8
    ui16TxData = SET_VDS;
    ls_statusPass &= MCDRV_Driver3PhSendCmd(this, &ui16TxData, &ui16RxData);
    ls_statusPass &= ((ui16TxData&0xF) == (ui16RxData&0xF));
	#ifdef PRINT_DRV_MSGS
		printf("\r\nSetting SET_VDS, TX: %x, RX: %x\r\n", (0xF&ui16TxData), (0xF&ui16RxData));
	#endif

    // Configure SPI Gate SPI_DRV_GATE_DISABLE_OVERCURRENT
//    ui16TxData = SPI_DRV_GATE_DISABLE_OVERCURRENT;
//    s_statusPass &= MCDRV_Driver3PhSendCmd(this, &ui16TxData, &ui16RxData);
//    s_statusPass = ((ui16TxData&0x03FF) == (ui16RxData&0x03FF));
//	#ifdef PRINT_DRV_MSGS
//		printf("\r\nSetting SPI_DRV_GATE_DISABLE_OVERCURRENT, TX: %x, RX: %x\r\n", (0x3FF&ui16TxData), (0x3FF&ui16RxData));
//	#endif

    // Configure SPI Gate LS_GATE_DRIVE_CONTROL
#define LS_GATE_DRIVE_CONTROL 0x3177
    ui16TxData = LS_GATE_DRIVE_CONTROL;
    ls_statusPass &= MCDRV_Driver3PhSendCmd(this, &ui16TxData, &ui16RxData);
    ls_statusPass &= ((ui16TxData&0x01FF) == (ui16RxData&0x01FF));
	#ifdef PRINT_DRV_MSGS
		printf("\r\nSetting LS_GATE_DRIVE_CONTROL, TX: %x, RX: %x\r\n", (0x1FF&ui16TxData), (0x1FF&ui16RxData));
	#endif

    // Configure SPI SPI_DRV_GATE_CONTROL
#define SPI_DRV_GATE_CONTROL 0x3806
    ui16TxData = SPI_DRV_GATE_CONTROL;
    ls_statusPass &= MCDRV_Driver3PhSendCmd(this, &ui16TxData, &ui16RxData);
    ls_statusPass &= ((ui16TxData&0x01FF) == (ui16RxData&0x01FF));
	#ifdef PRINT_DRV_MSGS
		printf("\r\nSetting SPI_DRV_GATE_CONTROL, TX: %x, RX: %x\r\n", (0xFF&ui16TxData), (0xFF&ui16RxData));
	#endif

//#define CURRENT_SHUNT_CONTROL 0x5015
//    ui16TxData = CURRENT_SHUNT_CONTROL;
//    ls_statusPass &= MCDRV_Driver3PhSendCmd(this, &ui16TxData, &ui16RxData);
//    ls_statusPass &= ((ui16TxData&0x07FF) == (ui16RxData&0x07FF));
//	#ifdef PRINT_DRV_MSGS
//		printf("\r\nSetting CURRENT SHUNT CONTROL, TX: %x, RX: %x\r\n", (0xFF&ui16TxData), (0xFF&ui16RxData));
//	#endif

	MCDRV_DriverReadFaults(this);
//    /* clear all faults */
//    s_statusPass &= MCDRV_Driver3PhClearFlags(this);
//
//    /* initialize MASK register 0 */
//    ui8TxData = 0x20 | (uint8_t)(this->sInterruptEnable.R);
//    s_statusPass &= MCDRV_Driver3PhSendCmd(this, &ui8TxData, &ui8RxData);
//
//    /* initialize MASK register 1 */
//    ui8TxData = 0x30 | (uint8_t)(this->sInterruptEnable.R >> 4);
//    s_statusPass &= MCDRV_Driver3PhSendCmd(this, &ui8TxData, &ui8RxData);
//
//    /* initialize MODE_COMMAND register  */
//    ui8TxData = 0x40 | (uint8_t)(this->sMode.R);
//    s_statusPass &= MCDRV_Driver3PhSendCmd(this, &ui8TxData, &ui8RxData);
//
//    /* set dead time, ONLY ZERO DT AVAILABLE - TBD */
//    s_statusPass &= MCDRV_Driver3PhSetDeadtime(this);
//
//    /* clear all faults */
//    s_statusPass &= MCDRV_Driver3PhClearFlags(this);
	#ifdef PRINT_DRV_MSGS
		printf("PASSED RESULT: %u\r\n", ls_statusPass);
	#endif
    } while ((!ls_statusPass)&& (++attempts<3));
    return (ls_statusPass);
}

void MCDRV_DriverReadFaults(mcdrv_spi_drv3ph_t *this)
{
    uint16_t ui16TxData, ui16RxData;
    bool_t spi_status_pass = false;
//	#define PRINT_ERR_MSGS
		// Configure SPI Gate Overcurrent
	#define WARNWATCHDOGRESET 0x8800 //0b1000100000000000
	#define OV_VDS_FAULT 0x9000 // 0b1001000000000000
	#define IC_FAULT 0x9800 // 0b1001100000000000
	#define VGS_FAULTS 0xA000 //0b1010000000000000

    ui16TxData = WARNWATCHDOGRESET;
    s_statusPass &= MCDRV_Driver3PhSendCmd(this, &ui16TxData, &ui16RxData);
	#ifdef PRINT_ERR_MSGS
		printf("\r\READ WARNWATCHDOGRESET, TX: %x, RX: %x\r\n", (0x1FF&ui16TxData), (0x1FF&ui16RxData));
	#endif


    ui16TxData = OV_VDS_FAULT;
    s_statusPass &= MCDRV_Driver3PhSendCmd(this, &ui16TxData, &ui16RxData);
	#ifdef PRINT_ERR_MSGS
		printf("\r\READ OV_VDS_FAULT, TX: %x, RX: %x\r\n", (0x1FF&ui16TxData), (0x1FF&ui16RxData));
	#endif


	ui16TxData = IC_FAULT;
	s_statusPass &= MCDRV_Driver3PhSendCmd(this, &ui16TxData, &ui16RxData);
	#ifdef PRINT_ERR_MSGS
		printf("\r\READ IC_FAULT, TX: %x, RX: %x\r\n", (0x1FF&ui16TxData), (0x1FF&ui16RxData));
	#endif


    ui16TxData = VGS_FAULTS;
    s_statusPass &= MCDRV_Driver3PhSendCmd(this, &ui16TxData, &ui16RxData);
	#ifdef PRINT_ERR_MSGS
		printf("\r\READ VGS_FAULTS, TX: %x, RX: %x\r\n", (0x1FF&ui16TxData), (0x1FF&ui16RxData));
	#endif

}
