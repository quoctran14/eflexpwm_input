/*
 * Copyright (c) 2016, Freescale Semiconductor, Inc.
 * Copyright 2016-2017 NXP
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "mcdrv_adc_hsadc.h"

/*******************************************************************************
 * Definitions
 ******************************************************************************/

/*******************************************************************************
 * Prototypes
 ******************************************************************************/

/*******************************************************************************
 * Variables
 ******************************************************************************/

static bool_t s_statusPass;

/*******************************************************************************
 * Code
 ******************************************************************************/

/*!
 * @brief Initializes ADC driver to measure 3 currents, DC-bus voltage and
 *        BEMF voltage for BLDC sensorless algorithm
 *
 * @param this   Pointer to the current object
 * @param init   Pointer to initialization structure
 *
 * @return boot_t true on success
 */
bool_t MCDRV_HSAdcInit(mcdrv_hsadc_t *this, mcdrv_hsadc_init_t *init)
{
    s_statusPass = TRUE;

    this->motorNum = init->motorNum;

    /* pointer to array with the channel numbers */
    this->pui16AdcArray = init->ui16AdcArray;

    /* default DC-bus current offset */
    this->ui16OffsetDcCurr = 0x3fff;

    /* check if there are one pair of assigned channels */
    if ((init->ui16AdcArray[MCDRV_ADC0_BEMFA] == MCDRV_CHAN_OFF) &&
        (init->ui16AdcArray[MCDRV_ADC1_BEMFA] == MCDRV_CHAN_OFF))
    {
        /* check if channel for phase A BEMF is assigned */
        s_statusPass = FALSE;
    }
    else if ((init->ui16AdcArray[MCDRV_ADC0_BEMFB] == MCDRV_CHAN_OFF) &&
             (init->ui16AdcArray[MCDRV_ADC1_BEMFB] == MCDRV_CHAN_OFF))
    {
        /* check if channel for phase B BEMF is assigned */
        s_statusPass = FALSE;
    }
    else if ((init->ui16AdcArray[MCDRV_ADC0_BEMFC] == MCDRV_CHAN_OFF) &&
             (init->ui16AdcArray[MCDRV_ADC1_BEMFC] == MCDRV_CHAN_OFF))
    {
        /* check if channel for phase C BEMF is assigned */
        s_statusPass = FALSE;
    }
    else if ((init->ui16AdcArray[MCDRV_ADC0_UDCB] == MCDRV_CHAN_OFF) &&
             (init->ui16AdcArray[MCDRV_ADC1_UDCB] == MCDRV_CHAN_OFF))
    {
        /* check if channel for DC-bus voltage is assigned */
        s_statusPass = FALSE;
    }
    else if ((init->ui16AdcArray[MCDRV_ADC0_IDCBA] == MCDRV_CHAN_OFF) &&
             (init->ui16AdcArray[MCDRV_ADC1_IDCBA] == MCDRV_CHAN_OFF))
    {
        /* check if channel for DC-bus current is assigned */
        s_statusPass = FALSE;
    }
    else
    {
        /* ADC module result register assignment */
        this->ui16RsltNumBemf = init->ui16RsltNumArray[MCDRV_BEMFA_RSLTNUM];
        this->ui16RsltNumBemfC = init->ui16RsltNumArray[MCDRV_BEMFC_RSLTNUM];
        this->ui16RsltNumAux = init->ui16RsltNumArray[MCDRV_AUX_RSLTNUM];
        this->ui16RsltNumIdcbA = init->ui16RsltNumArray[MCDRV_IDCBA_RSLTNUM];
        this->ui16RsltNumIdcbB = init->ui16RsltNumArray[MCDRV_IDCBB_RSLTNUM];
        this->ui16RsltNumIdcbC = init->ui16RsltNumArray[MCDRV_IDCBC_RSLTNUM];
        this->ui16RsltNumUdcb = init->ui16RsltNumArray[MCDRV_UDCB_RSLTNUM];
        this->pui32Adc0Base = init->pui32Adc0Base;
        this->pui32Adc1Base = init->pui32Adc1Base;

        /*BEMF A*/
        this->bldcAdc0SectorCfg[2] = init->ui16AdcArray[MCDRV_ADC0_BEMFA];
        this->bldcAdc0SectorCfg[5] = init->ui16AdcArray[MCDRV_ADC0_BEMFA];


        /*BEMF B*/

        this->bldcAdc0SectorCfg[1] = init->ui16AdcArray[MCDRV_ADC0_BEMFB];
        this->bldcAdc0SectorCfg[4] = init->ui16AdcArray[MCDRV_ADC0_BEMFB];


        /*BEMF C*/

        this->bldcAdc0SectorCfg[0] = init->ui16AdcArray[MCDRV_ADC1_BEMFC];
        this->bldcAdc0SectorCfg[3] = init->ui16AdcArray[MCDRV_ADC1_BEMFC];

        /* Sample Channel configurations specific to motor number*/
        switch(this->motorNum)
        {
			case 0:
				/*BEMF A*/
		        this->pui32BemfAAdcBase = init->pui32Adc0Base;

				/*BEMF B*/
		        this->pui32BemfBAdcBase = init->pui32Adc0Base;

		        /*BEMF C*/
		        this->pui32BemfCAdcBase = init->pui32Adc1Base;
		        this->pui32BemfCAdcBase->CLIST3 = ((this->pui32BemfCAdcBase->CLIST3 & ~(HSADC_CLIST3_SAMPLE8_MASK)) |
		                                           HSADC_CLIST3_SAMPLE8(init->ui16AdcArray[MCDRV_ADC1_BEMFC] + 8));

		        /* DC-bus voltage measurement */
		        this->pui32UdcbAdcBase = init->pui32Adc0Base;
		        this->pui32UdcbAdcBase->CLIST1 = ((this->pui32UdcbAdcBase->CLIST1 & ~(HSADC_CLIST1_SAMPLE1_MASK)) |
		                                          HSADC_CLIST1_SAMPLE1(init->ui16AdcArray[MCDRV_ADC0_UDCB]));
		        this->pui32UdcbAdcBase->MUX67_SEL = HSADC_MUX67_SEL_CH6_SELA(0);


		        /*IDCB */
		        this->pui32IdcbAAdcBase = init->pui32Adc0Base;
		        this->pui32IdcbAAdcBase->CLIST3 = ((this->pui32IdcbAAdcBase->CLIST3 & ~(HSADC_CLIST3_SAMPLE8_MASK)) |
		                                          HSADC_CLIST3_SAMPLE8(init->ui16AdcArray[MCDRV_ADC0_IDCBA] + 8));
		        this->pui32IdcbAAdcBase->MUX67_SEL = HSADC_MUX67_SEL_CH7_SELA(0);

		        this->pui32IdcbBAdcBase = init->pui32Adc1Base;
		        this->pui32IdcbBAdcBase->CLIST3 = ((this->pui32IdcbBAdcBase->CLIST3 & ~(HSADC_CLIST3_SAMPLE9_MASK)) |
		                                          HSADC_CLIST3_SAMPLE9(init->ui16AdcArray[MCDRV_ADC1_IDCBB] + 8));


		        this->pui32IdcbCAdcBase = init->pui32Adc1Base;
		        this->pui32IdcbCAdcBase->CLIST3 = ((this->pui32IdcbCAdcBase->CLIST3 & ~(HSADC_CLIST3_SAMPLE10_MASK)) |
		                                          HSADC_CLIST3_SAMPLE10(init->ui16AdcArray[MCDRV_ADC1_IDCBC] + 8));
		        this->pui32IdcbCAdcBase->MUX67_SEL = HSADC_MUX67_SEL_CH7_SELA(0);


//				/* auxiliary measurement */
//				this->pui32AuxAdcBase = init->pui32Adc0Base;
//				this->pui32AuxAdcBase->CLIST1 = ((this->pui32AuxAdcBase->CLIST1 & ~(HSADC_CLIST1_SAMPLE3_MASK)) |
//												 HSADC_CLIST1_SAMPLE3(init->ui16AdcArray[MCDRV_ADC0_AUX]));
				break;
			case 1:
				/*BEMF A*/
		        this->pui32BemfAAdcBase = init->pui32Adc1Base;

				/*BEMF B*/
		        this->pui32BemfBAdcBase = init->pui32Adc1Base;

		        /*BEMF C*/
		        this->pui32BemfCAdcBase = init->pui32Adc1Base;
		        this->pui32BemfCAdcBase->CLIST3 = ((this->pui32BemfCAdcBase->CLIST3 & ~(HSADC_CLIST3_SAMPLE9_MASK)) |
		                                           HSADC_CLIST3_SAMPLE9(init->ui16AdcArray[MCDRV_ADC1_BEMFC] + 8));

		        /* DC-bus voltage measurement */
		        this->pui32UdcbAdcBase = init->pui32Adc1Base;
		        this->pui32UdcbAdcBase->CLIST3 = ((this->pui32UdcbAdcBase->CLIST3 & ~(HSADC_CLIST3_SAMPLE10_MASK)) |
		        									HSADC_CLIST3_SAMPLE10(init->ui16AdcArray[MCDRV_ADC1_UDCB] + 8));

		        /*IDCB */
		        this->pui32IdcbAAdcBase = init->pui32Adc0Base;
		        this->pui32IdcbAAdcBase->CLIST2 = ((this->pui32IdcbAAdcBase->CLIST2 & ~(HSADC_CLIST2_SAMPLE7_MASK)) |
		                                          HSADC_CLIST2_SAMPLE7(init->ui16AdcArray[MCDRV_ADC0_IDCBA]));

		        this->pui32IdcbBAdcBase = init->pui32Adc1Base;
		        this->pui32IdcbBAdcBase->CLIST1 = ((this->pui32IdcbBAdcBase->CLIST1 & ~(HSADC_CLIST2_SAMPLE4_MASK)) |
		                                          HSADC_CLIST2_SAMPLE4(init->ui16AdcArray[MCDRV_ADC1_IDCBB]));


		        this->pui32IdcbCAdcBase = init->pui32Adc1Base;
		        this->pui32IdcbCAdcBase->CLIST2 = ((this->pui32IdcbCAdcBase->CLIST2 & ~(HSADC_CLIST2_SAMPLE7_MASK)) |
		                                          HSADC_CLIST2_SAMPLE7(init->ui16AdcArray[MCDRV_ADC1_IDCBC]));

//				/* auxiliary measurement */
//				this->pui32AuxAdcBase = init->pui32Adc0Base;
//				this->pui32AuxAdcBase->CLIST1 = ((this->pui32AuxAdcBase->CLIST1 & ~(HSADC_CLIST1_SAMPLE3_MASK)) |
//												 HSADC_CLIST1_SAMPLE3(init->ui16AdcArray[MCDRV_ADC0_AUX]));
				break;
			case 2:
				break;
			case 3:
				break;
			default:
				break;
        }
    }

    return (s_statusPass);
}

/*!
 * @brief Function set new channel assignment for next BEMF voltage sensing
 *
 * @param this   Pointer to the current object
 *
 * @return boot_t true on success
 */
bool_t MCDRV_AssignBemfChannel(mcdrv_hsadc_t *this)
{
    s_statusPass = TRUE;

    switch (this->ui16Sector)
    {
        /* BEMF phase C sensing */
        case 0:
        case 3:
        	switch(this->motorNum)
        	{
        		case 0:
                    this->pui32BemfCAdcBase->CLIST3 = ((this->pui32BemfCAdcBase->CLIST3 & ~(HSADC_CLIST3_SAMPLE8_MASK)) |
                                                       HSADC_CLIST3_SAMPLE8(this->pui16AdcArray[MCDRV_ADC1_BEMFC] + 8));
                    break;
        		case 1:
    		        this->pui32BemfCAdcBase->CLIST3 = ((this->pui32BemfCAdcBase->CLIST3 & ~(HSADC_CLIST3_SAMPLE9_MASK)) |
    		                                           HSADC_CLIST3_SAMPLE9(this->pui16AdcArray[MCDRV_ADC1_BEMFC] + 8));
        			break;
        		case 2:
        			break;
        		case 3:
        			break;
        		default:
        			break;
        	}

            break;

        /* BEMF phase B sensing */
        case 1:
        case 4:
        	switch(this->motorNum)
        	{
        		case 0:
                    this->pui32BemfBAdcBase->CLIST1 = ((this->pui32BemfBAdcBase->CLIST1 & ~(HSADC_CLIST1_SAMPLE2_MASK)) |
                                                       HSADC_CLIST1_SAMPLE2(this->pui16AdcArray[MCDRV_ADC0_BEMFB]));
                    break;
        		case 1:
                    this->pui32BemfBAdcBase->CLIST1 = ((this->pui32BemfBAdcBase->CLIST1 & ~(HSADC_CLIST1_SAMPLE0_MASK)) |
                                                       HSADC_CLIST1_SAMPLE0(this->pui16AdcArray[MCDRV_ADC1_BEMFB]));
        			break;
        		case 2:
        			break;
        		case 3:
        			break;
        		default:
        			break;
        	}

            break;

        /* BEMF phase A sensing */
        case 2:
        case 5:
        	switch(this->motorNum)
        	{
        		case 0:
                    this->pui32BemfAAdcBase->CLIST1 = ((this->pui32BemfAAdcBase->CLIST1 & ~(HSADC_CLIST1_SAMPLE2_MASK)) |
                                                       HSADC_CLIST1_SAMPLE2(this->pui16AdcArray[MCDRV_ADC0_BEMFA]));
                    break;
        		case 1:
                    this->pui32BemfAAdcBase->CLIST1 = ((this->pui32BemfAAdcBase->CLIST1 & ~(HSADC_CLIST1_SAMPLE0_MASK)) |
                                                       HSADC_CLIST1_SAMPLE0(this->pui16AdcArray[MCDRV_ADC1_BEMFA]));
        			break;
        		case 2:
        			break;
        		case 3:
        			break;
        		default:
        			break;
        	}

            break;

        default:
            break;
    }

    return (s_statusPass);
}

/*!
 * @brief Function reads ADC result register containing actual BEMF voltage
 *
 * Result register value is shifted three times to the right and stored
 * to BEMF voltage pointer
 *
 * @param this   Pointer to the current object
 *
 * @return boot_t true on success
 */
bool_t MCDRV_BemfVoltageGet(mcdrv_hsadc_t *this)
{
    s_statusPass = TRUE;

    switch (this->ui16Sector)
    {
        /* BEMF phase C sensing */
        case 0:
        case 3:
            *this->pf16BemfVoltage = (frac16_t)(this->pui32BemfCAdcBase->RSLT[this->ui16RsltNumBemfC]);
            break;

        /* BEMF phase B sensing */
        case 1:
        case 4:
            *this->pf16BemfVoltage = (frac16_t)(this->pui32BemfBAdcBase->RSLT[this->ui16RsltNumBemf]);
            break;

        /* BEMF phase A sensing */
        case 2:
        case 5:
            *this->pf16BemfVoltage = (frac16_t)(this->pui32BemfAAdcBase->RSLT[this->ui16RsltNumBemf]);
            break;

        default:
            break;
    }

    return (s_statusPass);
}

/*!
 * @brief Function reads ADC result register containing actual DC-bus voltage sample
 *
 * Result register value is shifted three times to the right and stored
 * to DC-bus voltage pointer
 *
 * @param this   Pointer to the current object
 *
 * @return boot_t true on success
 */
bool_t MCDRV_VoltDcBusGet(mcdrv_hsadc_t *this)
{
    s_statusPass = TRUE;
    /* read DC-bus voltage sample from defined ADCx result register */
    *this->pf16UDcBus = (frac16_t)(this->pui32UdcbAdcBase->RSLT[this->ui16RsltNumUdcb]);
    return (s_statusPass);
}

bool_t MCDRV_AssignCurrChannel(mcdrv_hsadc_t *this)
{
    s_statusPass = TRUE;

    switch (this->ui16Sector)
    {
        /* Phase A current sinks */
        case 0:
        case 1:
        	switch(this->motorNum)
        	{
        		case 0:
        		    this->pui32IdcbAAdcBase->CLIST2 = ((this->pui32IdcbAAdcBase->CLIST2 & ~(HSADC_CLIST2_SAMPLE7_MASK)) |
        		                                      HSADC_CLIST2_SAMPLE7(this->pui16AdcArray[MCDRV_ADC0_IDCBA]));
        		    this->pui32IdcbAAdcBase->MUX67_SEL = HSADC_MUX67_SEL_CH7_SELA(0);
        		    break;
        		case 1:
        			break;
        		case 2:
        			break;
        		case 3:
        			break;
        		default:
        			break;
        	}

            break;

        /* Phase B sinks */
        case 2:
        case 3:
        	switch(this->motorNum)
        	{
        		case 0:
        		    this->pui32IdcbBAdcBase->CLIST3 = ((this->pui32IdcbBAdcBase->CLIST3 & ~(HSADC_CLIST3_SAMPLE9_MASK)) |
        		                                      HSADC_CLIST3_SAMPLE9(this->pui16AdcArray[MCDRV_ADC1_IDCBB] + 8));

        		    break;
        		case 1:
        			break;
        		case 2:
        			break;
        		case 3:
        			break;
        		default:
        			break;
        	}

            break;

        /* Phase C current sinks */
        case 4:
        case 5:
        	switch(this->motorNum)
        	{
        		case 0:
        		    this->pui32IdcbCAdcBase->CLIST2 = ((this->pui32IdcbCAdcBase->CLIST2 & ~(HSADC_CLIST2_SAMPLE7_MASK)) |
        		                                      HSADC_CLIST2_SAMPLE7(this->pui16AdcArray[MCDRV_ADC1_IDCBC]));
        		    this->pui32IdcbCAdcBase->MUX67_SEL = HSADC_MUX67_SEL_CH7_SELA(0);
        		    break;
        		case 1:
        			break;
        		case 2:
        			break;
        		case 3:
        			break;
        		default:
        			break;
        	}

            break;

        default:
            break;
    }

    return (s_statusPass);
}

/*!
 * @brief Function reads ADC result register containing actual DC-bus current sample
 *
 * Result register value is shifted three times to the right and stored
 * to DC-bus current pointer
 *
 * @param this   Pointer to the current object
 *
 * @return boot_t true on success
 */
bool_t MCDRV_CurrDcBusGet(mcdrv_hsadc_t *this)
{
    s_statusPass = TRUE;

    switch (this->ui16Sector)
    {
        /* Phase A current sinks */
        case 0:
        case 1:
//            *this->pf16IDcBus = (frac16_t)((int16_t)(this->pui32IdcbAAdcBase->RSLT[this->ui16RsltNumIdcbA]) - this->ui16OffsetDcCurr);
            *this->pf16IDcBus = (frac16_t)(this->pui32IdcbAAdcBase->RSLT[this->ui16RsltNumIdcbA]);

            break;

        /* Phase B current sinks  */
        case 2:
        case 3:
//            *this->pf16IDcBus = (frac16_t)((int16_t)(this->pui32IdcbBAdcBase->RSLT[this->ui16RsltNumIdcbB]) - this->ui16OffsetDcCurr);
            *this->pf16IDcBus = (frac16_t)(this->pui32IdcbBAdcBase->RSLT[this->ui16RsltNumIdcbB]);
            break;

        /* Phase C current sinks  */
        case 4:
        case 5:
//            *this->pf16IDcBus = (frac16_t)((int16_t)(this->pui32IdcbCAdcBase->RSLT[this->ui16RsltNumIdcbC]) - this->ui16OffsetDcCurr);
            *this->pf16IDcBus = (frac16_t)(this->pui32IdcbCAdcBase->RSLT[this->ui16RsltNumIdcbC]);
            break;

        default:
            break;
    }

    return (s_statusPass);
}

/*!
 * @brief Function reads ADC result register containing actual auxiliary sample
 *
 * Result register value is shifted 3 times right and stored to
 * auxiliary pointer
 *
 * @param this   Pointer to the current object
 *
 * @return boot_t true on success
 */
bool_t MCDRV_AuxValGet(mcdrv_hsadc_t *this)
{
    s_statusPass = TRUE;

    /* read auxiliary channel sample from defined ADCx result register */
    *this->pui16AuxChan = (frac16_t)(this->pui32AuxAdcBase->RSLT[this->ui16RsltNumAux]);

    return (s_statusPass);
}

/*!
 * @brief Function initializes phase current channel offset measurement
 *
 * @param this   Pointer to the current object
 *
 * @return boot_t true on success
 */
bool_t MCDRV_CurrOffsetCalibInit(mcdrv_hsadc_t *this)
{
    s_statusPass = TRUE;

    /* clear offset values */
    this->ui16OffsetDcCurr = 0x3fff;
    this->ui16CalibDcCurr = 0;

    /* initialize offset filters */
    this->ui16FiltDcCurr.u16Sh = this->ui16OffsetFiltWindow;

    GDFLIB_FilterMAInit_F16((frac16_t)0, &this->ui16FiltDcCurr);

    return (s_statusPass);
}

/*!
 * @brief Function reads current samples and filter them
 *
 * @param this   Pointer to the current object
 *
 * @return boot_t true on success
 */
bool_t MCDRV_CurrOffsetCalib(mcdrv_hsadc_t *this)
{
    s_statusPass = TRUE;

    /* sensing of DC Bus Current offset */


//    this->ui16CalibDcACurr = (frac16_t) this->pui32IdcbAAdcBase->RSLT[this->ui16RsltNumIdcbA];
//    this->ui16CalibDcBCurr = (frac16_t) this->pui32IdcbBAdcBase->RSLT[this->ui16RsltNumIdcbB];
//    this->ui16CalibDcCCurr = (frac16_t) this->pui32IdcbCAdcBase->RSLT[this->ui16RsltNumIdcbC];

    this->ui16CalibDcCurr =  GDFLIB_FilterMA_F16((frac16_t) this->pui32IdcbAAdcBase->RSLT[this->ui16RsltNumIdcbA], &this->ui16FiltDcCurr);

    return (s_statusPass);
}

/*!
 * @brief Function passes measured offset values to main structure
 *
 * @param this   Pointer to the current object
 *
 * @return boot_t true on success
 */
bool_t MCDRV_CurrOffsetCalibSet(mcdrv_hsadc_t *this)
{
    s_statusPass = TRUE;

    /* pass Calib data for DC Bus current offset */
    this->ui16OffsetDcCurr = (this->ui16CalibDcCurr);

    return (s_statusPass);
}

/*!
 * @brief Function switch between DC bus current or voltage sensing
 *
 * @param this   Pointer to the current object
 *
 * @return boot_t true on success
 */
bool_t MCDRV_AssignDCBusChannel(mcdrv_hsadc_t *this)
{
    s_statusPass = TRUE;

    return (s_statusPass);
}

