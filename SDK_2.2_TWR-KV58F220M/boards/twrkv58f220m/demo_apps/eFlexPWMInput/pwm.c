/*
 * Copyright (c) 2015, Freescale Semiconductor, Inc.
 * Copyright 2016-2017 NXP
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "fsl_debug_console.h"
#include "board.h"
#include "fsl_pwm.h"

#include "pin_mux.h"
#include "fsl_xbara.h"
/*******************************************************************************
 * Definitions
 ******************************************************************************/

#define PWM_SRC_CLK_FREQ CLOCK_GetFreq(kCLOCK_FastPeriphClk)
/*!
 * @brief Main function
 */
int main(void)
{
    /* Structure of initialize PWM */
    pwm_config_t pwmConfig;
    static uint16_t delay;
    uint32_t pwmVal = 4;
    uint16_t i;

    /* Board pin, clock, debug console init */
    BOARD_InitPins();
    BOARD_BootClockRUN();
    BOARD_InitDebugConsole();

    /* Set the PWM Fault inputs to a low value */
    XBARA_Init(XBARA);
    XBARA_SetSignalsConnection(XBARA, kXBARA_InputVdd, kXBARA_OutputPwm0Fault0);
    XBARA_SetSignalsConnection(XBARA, kXBARA_InputVdd, kXBARA_OutputPwm0Fault1);
    XBARA_SetSignalsConnection(XBARA, kXBARA_InputVdd, kXBARA_OutputPwm0Fault2);
    XBARA_SetSignalsConnection(XBARA, kXBARA_InputVdd, kXBARA_OutputPwm0Fault3);

    PRINTF("FlexPWM driver example\n");

    PWM_GetDefaultConfig(&pwmConfig);

    /* Use full cycle reload */
    pwmConfig.reloadLogic = kPWM_ReloadPwmFullCycle;
    /* PWM A & PWM B form a complementary PWM pair */
    pwmConfig.pairOperation = kPWM_Independent;
    pwmConfig.enableDebugMode = true;

    /* Initialize PWM 1 Module 3 */
    if (PWM_Init(PWM1, kPWM_Module_3, &pwmConfig) == kStatus_Fail)
    {
        PRINTF("PWM initialization failed\n");
        return 1;
    }

//    /* Initialize PWM 1 Module 3 */
   if (PWM_Init(PWM0, kPWM_Module_3, &pwmConfig) == kStatus_Fail)
    {
        PRINTF("PWM initialization failed\n");
        return 1;
    }

   /* Setup PWM Capture signals */
   pwm_input_capture_param_t pwmCaptureSignal;
   pwmCaptureSignal.captureInputSel=false;
   pwmCaptureSignal.edgeCompareValue = 100;
   pwmCaptureSignal.edge0 = kPWM_RisingEdge;
   pwmCaptureSignal.edge1 = kPWM_FallingEdge;
   pwmCaptureSignal.enableOneShotCapture = false;
   PWM_SetupInputCapture(PWM0, kPWM_Module_3, kPWM_PwmA, &pwmCaptureSignal);
   PWM_SetupInputCapture(PWM0, kPWM_Module_3, kPWM_PwmB, &pwmCaptureSignal);
   PWM_SetupInputCapture(PWM1, kPWM_Module_3, kPWM_PwmA, &pwmCaptureSignal);
   PWM_SetupInputCapture(PWM1, kPWM_Module_3, kPWM_PwmB, &pwmCaptureSignal);

    PWM_StartTimer(PWM0, kPWM_Control_Module_3);
    PWM_StartTimer(PWM1, kPWM_Control_Module_3);

    uint16_t reg;
    uint16_t edge0PWM0CounterA = 0;
    uint16_t edge1PWM0CounterA = 0;
    uint16_t edge0PWM0CounterB = 0;
    uint16_t edge1PWM0CounterB = 0;

    uint16_t edge0PWM1CounterA = 0;
    uint16_t edge1PWM1CounterA = 0;
    uint16_t edge0PWM1CounterB = 0;
    uint16_t edge1PWM1CounterB = 0;

    int32_t diffEdgePWM0CounterA = 0;
    int32_t diffEdgePWM0CounterB = 0;
    int32_t diffEdgePWM1CounterA = 0;
    int32_t diffEdgePWM1CounterB = 0;

    while (1U)
    {
//        for (i = 0U; i < delay; i++)
//        {
//            __ASM volatile("nop");
//        }
////        pwmVal = pwmVal + 4;
//
//        /* Reset the duty cycle percentage */
//        if (pwmVal > 100)
//        {
//            pwmVal = 4;
//        }
//
//        /* Update duty cycles for all 3 PWM signals */
//        PWM_UpdatePwmDutycycle(BOARD_PWM_BASEADDR, kPWM_Module_0, kPWM_PwmA, kPWM_SignedCenterAligned, pwmVal);
//        PWM_UpdatePwmDutycycle(BOARD_PWM_BASEADDR, kPWM_Module_1, kPWM_PwmA, kPWM_SignedCenterAligned, (pwmVal >> 1));
//        PWM_UpdatePwmDutycycle(BOARD_PWM_BASEADDR, kPWM_Module_2, kPWM_PwmA, kPWM_SignedCenterAligned, (pwmVal >> 2));
//
//        /* Set the load okay bit for all submodules to load registers from their buffer */
//        PWM_SetPwmLdok(BOARD_PWM_BASEADDR, kPWM_Control_Module_0 | kPWM_Control_Module_1 | kPWM_Control_Module_2, true);
//

         edge0PWM0CounterA = 0;
         edge1PWM0CounterA = 0;
         edge0PWM0CounterB = 0;
         edge1PWM0CounterB = 0;

         edge0PWM1CounterA = 0;
         edge1PWM1CounterA = 0;
         edge0PWM1CounterB = 0;
         edge1PWM1CounterB = 0;


        edge0PWM0CounterA = PWM0->SM[kPWM_Module_3].CVAL2;
        edge1PWM0CounterA = PWM0->SM[kPWM_Module_3].CVAL3;
        diffEdgePWM0CounterA = (edge1PWM0CounterA>edge0PWM0CounterA) ? (edge1PWM0CounterA-edge0PWM0CounterA) : edge0PWM0CounterA-edge1PWM0CounterA;

        edge0PWM0CounterB = PWM0->SM[kPWM_Module_3].CVAL4;
        edge1PWM0CounterB = PWM0->SM[kPWM_Module_3].CVAL5;
        diffEdgePWM0CounterB = (edge1PWM0CounterB>edge0PWM0CounterB) ? (edge1PWM0CounterB-edge0PWM0CounterB) : edge0PWM0CounterB-edge1PWM0CounterB;

        edge0PWM1CounterA = PWM1->SM[kPWM_Module_3].CVAL2;
        edge1PWM1CounterA = PWM1->SM[kPWM_Module_3].CVAL3;
        diffEdgePWM1CounterA = (edge1PWM1CounterA>edge0PWM1CounterA) ? (edge1PWM1CounterA-edge0PWM1CounterA) : edge0PWM1CounterA-edge1PWM1CounterA;

        edge0PWM1CounterB = PWM1->SM[kPWM_Module_3].CVAL4;
        edge1PWM1CounterB = PWM1->SM[kPWM_Module_3].CVAL5;
        diffEdgePWM1CounterB = (edge1PWM1CounterB>edge0PWM1CounterB) ? (edge1PWM1CounterB-edge0PWM1CounterB) : edge0PWM1CounterB-edge1PWM1CounterB;

        printf("PWM0 SM3 TYPEA RISING COUNTER VALUE is %u \r\n", edge0PWM0CounterA);
        printf("PWM0 SM3 TYPEA FALLING COUNTER VALUE %u \r\n", edge1PWM0CounterA);
        printf("PWM0 SM3 TYPEA DUTY CYCLE: %u \r\n", diffEdgePWM0CounterA);

        printf("PWM0 SM3 TYPEB RISING COUNTER VALUE is %u \r\n", edge0PWM0CounterB);
        printf("PWM0 SM3 TYPEB FALLING COUNTER VALUE %u \r\n", edge1PWM0CounterB);
        printf("PWM0 SM3 TYPEB DUTY CYCLE: %u\r\n", diffEdgePWM0CounterB);

        printf("PWM1 SM3 TYPEA RISING COUNTER VALUE is %u \r\n", edge0PWM1CounterA);
        printf("PWM1 SM3 TYPEA FALLING COUNTER VALUE %u \r\n", edge1PWM1CounterA);
        printf("PWM1 SM3 TYPEA DUTY CYCLE: %u\r\n", diffEdgePWM1CounterA);

        printf("PWM1 SM3 TYPEB RISING COUNTER VALUE is %u \r\n", edge0PWM1CounterB);
        printf("PWM1 SM3 TYPEB FALLING COUNTER VALUE %u \r\n", edge1PWM1CounterB);
        printf("PWM1 SM3 TYPEB DUTY CYCLE: %u\r\n", diffEdgePWM1CounterB);
    }
}
